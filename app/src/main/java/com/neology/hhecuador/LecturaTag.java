package com.neology.hhecuador;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.hhecuador.dialogs.DialogLectura;
import com.neology.hhecuador.model.BaseResponse;
import com.neology.hhecuador.model.DataVehicleByPlateNumber;
import com.neology.hhecuador.model.VehicleTag;
import com.neology.hhecuador.utils.Constantes;
import com.neology.hhecuador.utils.RestClient;
import com.neology.hhecuador.utils.ServerContext;

public class LecturaTag extends AppCompatActivity implements RF_Listener {

    public static final String TAG = LecturaTag.class.getSimpleName();

    private RF_Control RF;
    private TextView folio;
    private TextView vin;
    private TextView placa;
    ImageView reiniciar;
    ImageView camposID;
    private RelativeLayout dataTagLecturaLayout;
    LinearLayout datosLecturaFinalLayout;
    String folioDisponible = "";

    Button reiniciarGrabadoBtn;


    DataVehicleByPlateNumber dataVehicleByPlateNumber;
    ProgressDialog progressDialog;

    DialogFragment dialogFragment;

    BaseResponse baseResponse;
    SharedPreferences sharedPreferences;
    VehicleTag vehicleTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectura_tag);
        Intent intent = getIntent();
        sharedPreferences = getApplicationContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        dataVehicleByPlateNumber = intent.getParcelableExtra("data");
        folioDisponible = intent.getStringExtra("folioDisponible");
        vehicleTag = intent.getParcelableExtra("vehicleTag");
        initElements();
    }

    private void initElements() {
        RF = new RF_Control();
        RF.Initialize(this);
        progressDialog = new ProgressDialog(this);
        dataTagLecturaLayout = (RelativeLayout) findViewById(R.id.cuerpoLecturaID);
        datosLecturaFinalLayout = (LinearLayout) findViewById(R.id.cuerpoDatosLecturaID);
        placa = (TextView) findViewById(R.id.placaID);
        vin = (TextView) findViewById(R.id.vinID);
        folio = (TextView) findViewById(R.id.folioID);
        reiniciar = (ImageView) findViewById(R.id.grabarNuevoID);
        reiniciar.setOnClickListener(reiniciarListener);
        camposID = (ImageView) findViewById(R.id.camposID);

        ImageView imageView = (ImageView) findViewById(R.id.lecturaImgID);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap preview_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.info_lectura, options);
        imageView.setImageBitmap(preview_bitmap);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(this.getComponentName().getClassName(), "Key-" + keyCode + " - KeyEvent-" + event);
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
//            progressDialog = ProgressDialog.show(this,
//                    getResources().getString(R.string.leyendoEtiqueta),
//                    "Espere por favor...");
            showDialogLectura();
            new readTag_Thread().start();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return false;
        } else {
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        RF.onPauseProcessNFC();
    }

    @Override
    protected void onResume() {
        super.onResume();
        RF.onResumeProcessNFC();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        RF.onNewIntentProcessNFC(intent);
    }

    @Override
    public void Event_RF_Control(String data) {
        resultGatillo(data);
    }

    @Override
    public void Event_RF_Log(String data) {
        resultGatillo(data);
    }

    class readTag_Thread extends Thread {

        @Override
        public void run() {
            super.run();
            RF.leerTagUHF(folioDisponible);
        }
    }

    private void resultGatillo(final String data) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("LECTURA", data);
//        0|3|4E45435500000CB8E1045A6F|E2003412013FC0FFE1045A6F|3256|||0|0|0|0.00|00000|0.00|
                String[] resultStr = data.split("\\|");
//                if (resultStr[0].equals("0")) {
//                    progressDialog.dismiss();
//                    msjLectura.setVisibility(View.GONE);
//                    dataTagLecturaLayout.setVisibility(View.VISIBLE);
//                    folio.setText(resultStr[4]);
//                } else {
//                    progressDialog.dismiss();
//                }
                switch (resultStr[0]) {
                    case "0":
//                        progressDialog.dismiss();
                        placa.setText(resultStr[5]);
                        placa.setVisibility(View.GONE);
                        vin.setText(resultStr[6]);
                        vin.setVisibility(View.GONE);
                        folio.setText(resultStr[4]);
                        folio.setVisibility(View.GONE);
                        new sendPostAsync().execute(vehicleTag.getPlate(), vehicleTag.getTidUhf(), vehicleTag.getTidNfc());
                        break;
                    case "1":
                        showAlert_Error(R.string.NO_TAG_CAMPO_LECTURA_1);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.NO_TAG_CAMPO_LECTURA_1), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "2":
                        showAlert_Error(R.string.MAS_1_TAG_CAMPO_LECTURA_2);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MAS_1_TAG_CAMPO_LECTURA_2), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "3":
                        showAlert_Error(R.string.ERROR_LECTURA_3);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_3), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "4":
                        showAlert_Error(R.string.PROBLEMAS_CONEXION_LECTOR_4);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PROBLEMAS_CONEXION_LECTOR_4), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "5":
                        showAlert_Error(R.string.ERROR_CONFIG_RFID_5);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_CONFIG_RFID_5), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "6":
                        showAlert_Error(R.string.ERROR_TIMEOUT_6);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_6), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "7":
                        showAlert_Error(R.string.ERROR_RFID_NO_INIC_7);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_RFID_NO_INIC_7), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "8":
                        showAlert_Error(R.string.INIC_RFID_8);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_RFID_8), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "9":
                        showAlert_Error(R.string.RFID_DESCONECTADO_9);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DESCONECTADO_9), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "10":
                        showAlert_Error(R.string.ERROR_FORMATO_10);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_10), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "11":
                        showAlert_Error(R.string.MEMORIA_EXCEDIDA_11);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_EXCEDIDA_11), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "12":
                        showAlert_Error(R.string.MEMORIA_BLOQUEADA_12);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_BLOQUEADA_12), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "13":
                        showAlert_Error(R.string.FUERA_RANGO_13);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FUERA_RANGO_13), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "14":
                        showAlert_Error(R.string.ERROR_FORMATO_TELEPEAJE_14);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_TELEPEAJE_14), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "15":
                        showAlert_Error(R.string.ERROR_ESCRITURA_LOCALIDAD_15);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_LOCALIDAD_15), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "16":
                        showAlert_Error(R.string.LONGITUD_FUERA_RANGO_16);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LONGITUD_FUERA_RANGO_16), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "17":
                        showAlert_Error(R.string.ERROR_FORMATO_ECU_17);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_ECU_17), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "18":
                        showAlert_Error(R.string.ERROR_TELEPEAJE_18);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TELEPEAJE_18), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "19":
                        showAlert_Error(R.string.ERROR_PREFIJO_19);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_PREFIJO_19), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "20":
                        showAlert_Error(R.string.ERROR_TAG_NO_PERSONALIZADO_20);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_PERSONALIZADO_20), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "21":
                        showAlert_Error(R.string.ERROR_TAG_NO_LEIDO_21);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_LEIDO_21), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "22":
                        showAlert_Error(R.string.ERROR_TIMEOUT_CANCELA_OPER_22);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_CANCELA_OPER_22), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "23":
                        showAlert_Error(R.string.ERROR_TAG_NO_CORRESPONDE_CON_TAG_LEIDO_23);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_CORRESPONDE_CON_TAG_LEIDO_23), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "24":
                        showAlert_Error(R.string.INIC_PROCESO_24);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_PROCESO_24), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "25":
                        showAlert_Error(R.string.LECTOR_RF_OCUPADO_25);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LECTOR_RF_OCUPADO_25), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "30":
                        showAlert_Error(R.string.FOLIO_NO_VALIDO_30);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_NO_VALIDO_30), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "31":
                        showAlert_Error(R.string.FOLIO_FUERA_RANGO_31);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_FUERA_RANGO_31), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "40":
                        showAlert_Error(R.string.INIC_LECTURA_EPC_40);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_EPC_40), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "41":
                        showAlert_Error(R.string.ERROR_LECTURA_EPC_41);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_EPC_41), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "42":
                        showAlert_Error(R.string.INIC_ESCRITURA_EPC_42);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_EPC_42), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "43":
                        showAlert_Error(R.string.ERROR_ESCRITURA_EPC_43);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_EPC_43), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "44":
                        showAlert_Error(R.string.INIC_LECTURA_TID_44);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_TID_44), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "45":
                        showAlert_Error(R.string.ERROR_LECTURA_TID_45);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_TID_45), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "46":
                        showAlert_Error(R.string.INIC_ESCRITURA_TID_46);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_TID_46), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "47":
                        showAlert_Error(R.string.ERROR_ESCRITURA_TID_47);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_TID_47), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "48":
                        showAlert_Error(R.string.INIC_LECTURA_USER_48);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_48), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "49":
                        showAlert_Error(R.string.ERROR_LECTURA_USER_49);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_49), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "50":
                        showAlert_Error(R.string.INIC_ESCRITURA_USER_50);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_50), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "51":
                        showAlert_Error(R.string.ERROR_ESCRITURA_USER_51);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_51), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "61":
                        showAlert_Error(R.string.INIC_LECTURA_USER_61);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_61), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "62":
                        showAlert_Error(R.string.ERROR_LECTURA_USER_62);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_62), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "63":
                        showAlert_Error(R.string.INIC_ESCRITURA_USER_63);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_63), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "64":
                        showAlert_Error(R.string.ERROR_ESCRITURA_USER_64);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_64), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "98":
                        showAlert_Error(R.string.RFID_OCUPADO_98);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_OCUPADO_98), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                    case "99":
                        showAlert_Error(R.string.RFID_DISPONIBLE_99);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DISPONIBLE_99), Toast.LENGTH_SHORT).show();
//                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        break;
                }
            }
        });
    }

    private void restartRF() {
        if (RF != null) {
            RF.Destroy();
            RF = null;
        }
        RF = new RF_Control();
        RF.Initialize(this);
    }

    private View.OnClickListener reiniciarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), PlacaActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    };

    private void showAlert_Error(int codeError) {
        new AlertDialog.Builder(this)
                .setTitle("INFORMACIÓN")
                .setMessage(codeError)
                .setPositiveButton(R.string.persoDialogOK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showDialogLectura() {
        dialogFragment = DialogLectura.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    class sendPostAsync extends AsyncTask<String, Void, BaseResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected BaseResponse doInBackground(String... params) {
            ServerContext context = new ServerContext("raidentrance", "Jokowis123", "http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null));
            baseResponse = (new RestClient(context)).associateVehicleTag(params[0], params[1], params[2]);
            return baseResponse;
        }

        @Override
        protected void onPostExecute(BaseResponse s) {
            super.onPostExecute(s);
            dialogFragment.dismiss();
            if (s.getCode() == 200) {
                placa.setVisibility(View.VISIBLE);
                vin.setVisibility(View.VISIBLE);
                folio.setVisibility(View.VISIBLE);
                dataTagLecturaLayout.setVisibility(View.GONE);
                datosLecturaFinalLayout.setVisibility(View.VISIBLE);
                camposID.setVisibility(View.VISIBLE);

            } else {
                Log.d(TAG, s.getMessage());
                AlertDialog.Builder builder = new AlertDialog.Builder(LecturaTag.this)
                        .setTitle("Información")
                        .setMessage(s.getMessage())
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(getApplicationContext(), PersoUHFActivity.class);
                                intent.putExtra("data", dataVehicleByPlateNumber);
                                intent.putExtra("folioDisponible", folioDisponible);
                                intent.putExtra("vehicleTag", vehicleTag);
                                startActivity(intent);
                                finish();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }
}
