package com.neology.hhecuador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.neology.hhecuador.dialogs.DialogValidacion;
import com.neology.hhecuador.dialogs.Dialog_PrePersoUHF;
import com.neology.hhecuador.model.DataVehicleByPlateNumber;

public class DatosPlacaActivity extends AppCompatActivity {

    public static final String TAG = DatosPlacaActivity.class.getSimpleName();
    private ImageView    persoUHFBtn;
    private TextView plateNumber;
    private TextView vin;
    private TextView model;
    private TextView folio;

    DataVehicleByPlateNumber dataVehicleByPlateNumber;
    String folioDisponible = "";
    int positionDisponible = 0;
    DialogFragment dialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_placa);
        Intent intent = getIntent();
        dataVehicleByPlateNumber = intent.getParcelableExtra("data");
        folioDisponible = intent.getStringExtra("folioDisponible");
        positionDisponible = intent.getIntExtra("positionDisponible", positionDisponible);
        initElements();
    }

    private void initElements() {
        plateNumber = (TextView) findViewById(R.id.plateNumberTxtID);
        plateNumber.setText(dataVehicleByPlateNumber.getPlateNumber());
        vin = (TextView) findViewById(R.id.vinTxtID);
        vin.setText(dataVehicleByPlateNumber.getVin());
        model = (TextView) findViewById(R.id.modelTxtID);
        model.setText(folioDisponible);

        persoUHFBtn = (ImageView) findViewById(R.id.persoUHFBtnID);
        persoUHFBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showAlertOK_Perso(dataVehicleByPlateNumber);
                showDialog_Validacion();
            }
        });
    }

    private void showAlertOK_Perso(final DataVehicleByPlateNumber dataVehicleByPlateNumber) {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.validDataDialogTitle))
                .setMessage(getResources().getString(R.string.validDataDialogMsg))
                .setPositiveButton(R.string.validDataDialogOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ok();
                    }
                })
                .setNegativeButton(R.string.validDataDialogCancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void ok () {
        Intent intent = new Intent(getApplicationContext(), PersoUHFActivity.class);
        intent.putExtra("data", dataVehicleByPlateNumber);
        intent.putExtra("folioDisponible", folioDisponible);
        intent.putExtra("positionDisponible", positionDisponible);
        startActivity(intent);
        finish();
    }

    public void closeDialog() {
        dialogFragment.dismiss();
    }
    public void showDialog_Validacion() {
//        dialogFragment = DialogValidacion.newInstance();
//        dialogFragment.show(getSupportFragmentManager(), "dialog");
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        dialogFragment = DialogValidacion.newInstance();
        dialogFragment.show(ft, "dialog");
    }

}
