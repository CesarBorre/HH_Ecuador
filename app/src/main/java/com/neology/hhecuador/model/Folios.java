package com.neology.hhecuador.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 18/02/16.
 */
public class Folios implements Parcelable{

    private String folio;
    private boolean isChecked;
    private boolean isCheckedDotado;

    public Folios (String folio, boolean isChecked, boolean isCheckedDotado) {
        this.folio = folio;
        this.isChecked = isChecked;
        this.isCheckedDotado = isCheckedDotado;
    }

    protected Folios(Parcel in) {
        folio = in.readString();
    }

    public static final Creator<Folios> CREATOR = new Creator<Folios>() {
        @Override
        public Folios createFromParcel(Parcel in) {
            return new Folios(in);
        }

        @Override
        public Folios[] newArray(int size) {
            return new Folios[size];
        }
    };

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(folio);
    }

    public boolean isCheckedDotado() {
        return isCheckedDotado;
    }

    public void setIsCheckedDotado(boolean isCheckedDotado) {
        this.isCheckedDotado = isCheckedDotado;
    }
}
