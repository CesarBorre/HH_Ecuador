package com.neology.hhecuador.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 16/02/16.
 */
public class DataVehicleByPlateNumber implements Parcelable {

//    {"model":2015,"status":"Vehículo con Tag Asociado","fuelType":"Gasolina","vin":"vin","vehicleType":"Auto","litersPerMonth":25,"subsidization":true,"licensePlate":"ZZZ299"}


    private String model;
    private String status;
    private String fuelType;
    private String vin;
    private String vehicleType;
    private String litersPerMonth;
    private String subsidization;
    private String plateNumber;
    private String folio;

    public DataVehicleByPlateNumber(String model, String status, String fuelType, String vin, String vehicleType,
                                    String litersPerMonth, String subsidization, String plateNumber, String folio) {
        this.model = model;
        this.status = status;
        this.fuelType = fuelType;
        this.vin = vin;
        this.vehicleType = vehicleType;
        this.litersPerMonth = litersPerMonth;
        this.subsidization = subsidization;
        this.plateNumber = plateNumber;
        this.folio = folio;
    }


    protected DataVehicleByPlateNumber(Parcel in) {
        model = in.readString();
        status = in.readString();
        fuelType = in.readString();
        vin = in.readString();
        vehicleType = in.readString();
        litersPerMonth = in.readString();
        subsidization = in.readString();
        plateNumber = in.readString();
        folio = in.readString();
    }

    public static final Creator<DataVehicleByPlateNumber> CREATOR = new Creator<DataVehicleByPlateNumber>() {
        @Override
        public DataVehicleByPlateNumber createFromParcel(Parcel in) {
            return new DataVehicleByPlateNumber(in);
        }

        @Override
        public DataVehicleByPlateNumber[] newArray(int size) {
            return new DataVehicleByPlateNumber[size];
        }
    };

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getLitersPerMonth() {
        return litersPerMonth;
    }

    public void setLitersPerMonth(String litersPerMonth) {
        this.litersPerMonth = litersPerMonth;
    }

    public String getSubsidization() {
        return subsidization;
    }

    public void setSubsidization(String subsidization) {
        this.subsidization = subsidization;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(model);
        dest.writeString(status);
        dest.writeString(fuelType);
        dest.writeString(vin);
        dest.writeString(vehicleType);
        dest.writeString(litersPerMonth);
        dest.writeString(subsidization);
        dest.writeString(plateNumber);
        dest.writeString(folio);
    }
}
