package com.neology.hhecuador.model;

/**
 * Created by root on 1/03/16.
 */
public class TID_TAG {
    private String numberPlate;
    private String tidUHF;
    private String tidNFC;

    public TID_TAG () {
    }


    public String getNumberPlate() {
        return numberPlate;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public String getTidUHF() {
        return tidUHF;
    }

    public void setTidUHF(String tidUHF) {
        this.tidUHF = tidUHF;
    }

    public String getTidNFC() {
        return tidNFC;
    }

    public void setTidNFC(String tidNFC) {
        this.tidNFC = tidNFC;
    }
}
