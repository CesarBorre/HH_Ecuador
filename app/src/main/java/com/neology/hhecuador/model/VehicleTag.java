package com.neology.hhecuador.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;import java.lang.Override;import java.lang.String;

public class VehicleTag implements Parcelable{
    private String plate;
    private String tidUhf;
    private String tidNfc;
    @JsonIgnore
    private String folio;

    public VehicleTag() {
    }

    public VehicleTag(String plate, String tidUhf, String tidNfc) {
        this.plate = plate;
        this.tidUhf = tidUhf;
        this.tidNfc = tidNfc;
    }

    protected VehicleTag(Parcel in) {
        plate = in.readString();
        tidUhf = in.readString();
        tidNfc = in.readString();
        folio = in.readString();
    }

    public static final Creator<VehicleTag> CREATOR = new Creator<VehicleTag>() {
        @Override
        public VehicleTag createFromParcel(Parcel in) {
            return new VehicleTag(in);
        }

        @Override
        public VehicleTag[] newArray(int size) {
            return new VehicleTag[size];
        }
    };

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getTidUhf() {
        return tidUhf;
    }

    public void setTidUhf(String tidUhf) {
        this.tidUhf = tidUhf;
    }

    public String getTidNfc() {
        return tidNfc;
    }

    public void setTidNfc(String tidNfc) {
        this.tidNfc = tidNfc;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public String toString() {
        return "VehicleTag{" +
                "folio='" + folio + '\'' +
                ", plate='" + plate + '\'' +
                ", tidUhf='" + tidUhf + '\'' +
                ", tidNfc='" + tidNfc + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(plate);
        dest.writeString(tidUhf);
        dest.writeString(tidNfc);
        dest.writeString(folio);
    }
}
