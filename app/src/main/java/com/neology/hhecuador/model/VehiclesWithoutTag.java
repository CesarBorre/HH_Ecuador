package com.neology.hhecuador.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by root on 1/03/16.
 */
public class VehiclesWithoutTag implements Parcelable{
    private String model;
    private String licensePlate;
    private String vin;
    private String litersPerMonth;
    private String subsidization;
    private String fuelType;
    private String status;
    private String vehicleType;

    public VehiclesWithoutTag (
            String model,
            String status,
            String fuelType,
            String vin,
            String vehicleType,
            String litersPerMonth,
            String subsidization,
            String licensePlate) {
        this.model = model;
        this.licensePlate = licensePlate;
        this.vin = vin;
        this.litersPerMonth = litersPerMonth;
        this.subsidization = subsidization;
        this.fuelType = fuelType;
        this.status = status;
        this.vehicleType = vehicleType;
    }


    protected VehiclesWithoutTag(Parcel in) {
        model = in.readString();
        licensePlate = in.readString();
        vin = in.readString();
        litersPerMonth = in.readString();
        subsidization = in.readString();
        fuelType = in.readString();
        status = in.readString();
        vehicleType = in.readString();
    }

    public static final Creator<VehiclesWithoutTag> CREATOR = new Creator<VehiclesWithoutTag>() {
        @Override
        public VehiclesWithoutTag createFromParcel(Parcel in) {
            return new VehiclesWithoutTag(in);
        }

        @Override
        public VehiclesWithoutTag[] newArray(int size) {
            return new VehiclesWithoutTag[size];
        }
    };

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLitersPerMonth() {
        return litersPerMonth;
    }

    public void setLitersPerMonth(String litersPerMonth) {
        this.litersPerMonth = litersPerMonth;
    }

    public String getSubsidization() {
        return subsidization;
    }

    public void setSubsidization(String subsidization) {
        this.subsidization = subsidization;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(model);
        dest.writeString(licensePlate);
        dest.writeString(vin);
        dest.writeString(litersPerMonth);
        dest.writeString(subsidization);
        dest.writeString(fuelType);
        dest.writeString(status);
        dest.writeString(vehicleType);
    }
}
