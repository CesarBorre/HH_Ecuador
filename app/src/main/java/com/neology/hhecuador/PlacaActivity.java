package com.neology.hhecuador;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.hhecuador.dialogs.DialogSettings;
import com.neology.hhecuador.dialogs.DialogVehicleWithoutTAG;
import com.neology.hhecuador.dialogs.Dialog_Folios;
import com.neology.hhecuador.dialogs.Dialog_PrePersoUHF;
import com.neology.hhecuador.model.DataVehicleByPlateNumber;
import com.neology.hhecuador.model.Folios;
import com.neology.hhecuador.model.VehiclesWithoutTag;
import com.neology.hhecuador.utils.Constantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlacaActivity extends AppCompatActivity implements RF_Listener {

    public static final String TAG = PlacaActivity.class.getSimpleName();
    private ImageView searchBtn;
    private EditText numberPlate;

    DataVehicleByPlateNumber dataVehicleByPlateNumber;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ArrayList<Folios> foliosArrayList;

    ProgressDialog progressDialog;
    DialogFragment newFragment;
    DialogFragment newFragmentFolios;
    DialogFragment newFragmentVehicleWithout;

    int position = 0;
    int positionDisponible = 0;
    String folioDisponible = "";

    private RF_Control RF;

    ArrayList<VehiclesWithoutTag> vehiclesWithoutTagArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placa);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Intent intent = getIntent();
//        dataVehicleByPlateNumber = intent.getParcelableExtra("data");
        initElements();

        loadFoliosShared();
        loadArrayFromShaerd();
    }

    private void initElements() {
        RF = new RF_Control();
        RF.Initialize(this);

        progressDialog = new ProgressDialog(this);

        numberPlate = (EditText) findViewById(R.id.numberPlateID);
//        numberPlate.setText("ZZZ299");

        searchBtn = (ImageView) findViewById(R.id.searchPlateId);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarFolioDisponible();
                if (!folioDisponible.equals("")) {
                    if (numberPlate.getText().length() > 0) {
                        searchPlate(numberPlate.getText().toString());
                    } else {
                        Toast.makeText(getApplicationContext(), "Ingresar Placa", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showAlert_Folio_PrePerso();
                }
            }
        });
    }

    private void cargarFolioDisponible() {
        if (foliosArrayList != null) {
            for (int i = 0; i < foliosArrayList.size(); i++) {
                Log.d(null, foliosArrayList.get(i).isChecked() + "");
                if (foliosArrayList.get(i).isChecked() && !foliosArrayList.get(i).isCheckedDotado()) {
                    folioDisponible = foliosArrayList.get(i).getFolio();
                    Log.d(null, "Folio DIsponible " + folioDisponible);
                    positionDisponible = i;
                    break;
                }
            }
        }
    }

    private void searchPlate(String plateNumber) {
        callRestPlateNumber(plateNumber);
    }

    private void callRestPlateNumber(String plateNumber) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null) + "/rest/vehicles/vehicle/plate/" + plateNumber,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        readJson_VehicleByPlateNumber(response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        if (error.toString().equals("com.android.volley.ServerError")) {
                            Toast.makeText(getApplicationContext(), "Vehículo no encontrado", Toast.LENGTH_SHORT).show();
                        } else if (error.toString().contains("com.android.volley.NoConnectionError")) {
                            Toast.makeText(getApplicationContext(), "Error al conectar con servidor", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Auth();
            }

        };
        VolleyApp.getmInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void readJson_VehicleByPlateNumber(String response) {
        JsonVehiclebyPlateNumber jsonVehiclebyPlateNumber = new JsonVehiclebyPlateNumber();
        jsonVehiclebyPlateNumber.execute(response);
    }

    private HashMap Auth() {
        HashMap<String, String> params = new HashMap<String, String>();
        String creds = String.format("%s:%s", "raidentrance", "Jokowis123");
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        params.put("Authorization", auth);
        return params;
    }

    @Override
    public void Event_RF_Control(String data) {
        recordResult(data);
    }

    @Override
    public void Event_RF_Log(String data) {
        recordResult(data);
    }

    class JsonVehiclebyPlateNumber extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            try {
                JSONObject jsonObject = new JSONObject(params[0]);
                if (jsonObject.getString("licensePlate") != null) {
//                    {"model":2015,"status":"Vehículo con Tag Asociado","fuelType":"Gasolina","c":"vin","vehicleType":"Auto","litersPerMonth":25,"subsidization":true,"licensePlate":"ZZZ299"}
                    Log.d(TAG, String.valueOf(jsonObject.getInt("model")));
                    Log.d(TAG, jsonObject.getString("status"));
                    Log.d(TAG, jsonObject.getString("fuelType"));
                    Log.d(TAG, jsonObject.getString("vin"));
                    Log.d(TAG, jsonObject.getString("vehicleType"));
                    Log.d(TAG, String.valueOf(jsonObject.getDouble("litersPerMonth")));
                    Log.d(TAG, String.valueOf(jsonObject.getBoolean("subsidization")));
                    Log.d(TAG, jsonObject.getString("licensePlate"));


                    dataVehicleByPlateNumber = new DataVehicleByPlateNumber(
                            String.valueOf(jsonObject.getInt("model")),
                            jsonObject.getString("status"),
                            jsonObject.getString("fuelType"),
                            jsonObject.getString("vin"),
                            jsonObject.getString("vehicleType"),
                            String.valueOf(jsonObject.getDouble("litersPerMonth")),
                            String.valueOf(jsonObject.getBoolean("subsidization")),
                            jsonObject.getString("licensePlate"), null);
                    result = "OK";
                } else {
                    result = "Vehículo no encontrado";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return e.toString();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            super.onPostExecute(aBoolean);
            if (!aBoolean.equals("Vehículo no encontrado")) {
                cargarFolioDisponible();
                Intent intent = new Intent(getApplicationContext(), DatosPlacaActivity.class);
                intent.putExtra("data", dataVehicleByPlateNumber);
                intent.putExtra("folioDisponible", folioDisponible);
                intent.putExtra("positionDisponible", positionDisponible);
                numberPlate.setText("");
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), aBoolean, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadFoliosShared() {
        editor = sharedPreferences.edit();

        if (!sharedPreferences.contains(Constantes.KEY_FOLIO1)) {
            editor.putString(Constantes.KEY_FOLIO1, Constantes.SHARED_PREF_FOLIO1);
            editor.putString(Constantes.KEY_FOLIO2, Constantes.SHARED_PREF_FOLIO2);
            editor.putString(Constantes.KEY_FOLIO3, Constantes.SHARED_PREF_FOLIO3);
            editor.putString(Constantes.KEY_FOLIO4, Constantes.SHARED_PREF_FOLIO4);
            editor.putString(Constantes.KEY_FOLIO5, Constantes.SHARED_PREF_FOLIO5);
            editor.putString(Constantes.KEY_FOLIO6, Constantes.SHARED_PREF_FOLIO6);
            editor.putString(Constantes.KEY_FOLIO7, Constantes.SHARED_PREF_FOLIO7);
            editor.putString(Constantes.KEY_FOLIO8, Constantes.SHARED_PREF_FOLIO8);
            editor.putString(Constantes.KEY_FOLIO9, Constantes.SHARED_PREF_FOLIO9);
            editor.putString(Constantes.KEY_FOLIO10, Constantes.SHARED_PREF_FOLIO10);

            editor.putString(Constantes.KEY_CHECK1, Constantes.SHARED_PREF_CHECK1);
            editor.putString(Constantes.KEY_CHECK2, Constantes.SHARED_PREF_CHECK2);
            editor.putString(Constantes.KEY_CHECK3, Constantes.SHARED_PREF_CHECK3);
            editor.putString(Constantes.KEY_CHECK4, Constantes.SHARED_PREF_CHECK4);
            editor.putString(Constantes.KEY_CHECK5, Constantes.SHARED_PREF_CHECK5);
            editor.putString(Constantes.KEY_CHECK6, Constantes.SHARED_PREF_CHECK6);
            editor.putString(Constantes.KEY_CHECK7, Constantes.SHARED_PREF_CHECK7);
            editor.putString(Constantes.KEY_CHECK8, Constantes.SHARED_PREF_CHECK8);
            editor.putString(Constantes.KEY_CHECK9, Constantes.SHARED_PREF_CHECK9);
            editor.putString(Constantes.KEY_CHECK10, Constantes.SHARED_PREF_CHECK10);

            editor.putString(Constantes.KEY_CHECKDOTADO1, Constantes.SHARED_PREF_CHECKDOTADO1);
            editor.putString(Constantes.KEY_CHECKDOTADO2, Constantes.SHARED_PREF_CHECKDOTADO2);
            editor.putString(Constantes.KEY_CHECKDOTADO3, Constantes.SHARED_PREF_CHECKDOTADO3);
            editor.putString(Constantes.KEY_CHECKDOTADO4, Constantes.SHARED_PREF_CHECKDOTADO4);
            editor.putString(Constantes.KEY_CHECKDOTADO5, Constantes.SHARED_PREF_CHECKDOTADO5);
            editor.putString(Constantes.KEY_CHECKDOTADO6, Constantes.SHARED_PREF_CHECKDOTADO6);
            editor.putString(Constantes.KEY_CHECKDOTADO7, Constantes.SHARED_PREF_CHECKDOTADO7);
            editor.putString(Constantes.KEY_CHECKDOTADO8, Constantes.SHARED_PREF_CHECKDOTADO8);
            editor.putString(Constantes.KEY_CHECKDOTADO9, Constantes.SHARED_PREF_CHECKDOTADO9);
            editor.putString(Constantes.KEY_CHECKDOTADO10, Constantes.SHARED_PREF_CHECKDOTADO10);

            editor.putString(Constantes.kEY_IP_PORT, Constantes.SHARED_PREF_IP_PORT);
            editor.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
//            showDialog_PrePerso();
            loadArrayFromShaerd();
            showDialog_Folios();
            return true;
        } else if (id == R.id.action_settings_ip) {
            showDialogSettings();
            return true;
        } else if (id == R.id.searchPlateWithoutTAG) {
            searchVehicleWithoutTag();
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadArrayFromShaerd() {
        foliosArrayList = new ArrayList<Folios>();
        Folios folios = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO1, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK1, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO1, "false")));
        foliosArrayList.add(folios);
        Folios folios1 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO2, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK2, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO2, "false")));
        foliosArrayList.add(folios1);
        Folios folios2 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO3, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK3, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO3, "false")));
        foliosArrayList.add(folios2);
        Folios folios3 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO4, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK4, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO4, "false")));
        foliosArrayList.add(folios3);
        Folios folios4 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO5, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK5, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO5, "false")));
        foliosArrayList.add(folios4);
        Folios folios5 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO6, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK6, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO6, "false")));
        foliosArrayList.add(folios5);
        Folios folios6 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO7, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK7, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO7, "false")));
        foliosArrayList.add(folios6);
        Folios folios7 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO8, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK8, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO8, "false")));
        foliosArrayList.add(folios7);
        Folios folios8 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO9, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK9, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO9, "false")));
        foliosArrayList.add(folios8);
        Folios folios9 = new Folios(
                sharedPreferences.getString(Constantes.KEY_FOLIO10, null),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECK10, "false")),
                Boolean.valueOf(sharedPreferences.getString(Constantes.KEY_CHECKDOTADO10, "false")));
        foliosArrayList.add(folios9);
    }

    private void showDialog_Folios() {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        newFragmentFolios = Dialog_Folios.newInstance(foliosArrayList);
        newFragmentFolios.show(ft, "dialog");
    }

    private void recordResult(final String data) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String[] resultStr = data.split("\\|");
                Log.i(PlacaActivity.this.getComponentName().getClassName(), "--------- RESULT CODE FROM API: " + data);
                switch (resultStr[0]) {
                    case "0":
                        Log.d(TAG, "Folio DIsponible----------------------> " + folioDisponible);
                        modifyShared(position);
                        loadArrayFromShaerd();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.OPERACION_EXITOSA_0), Toast.LENGTH_SHORT).show();
                        if (newFragmentFolios.isVisible()) {
                            newFragmentFolios.dismiss();
                        }
                        progressDialog.dismiss();
                        break;
                    case "1":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.NO_TAG_CAMPO_LECTURA_1), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "2":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MAS_1_TAG_CAMPO_LECTURA_2), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "3":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_3), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "4":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PROBLEMAS_CONEXION_LECTOR_4), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "5":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_CONFIG_RFID_5), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "6":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_6), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "7":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_RFID_NO_INIC_7), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "8":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_RFID_8), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "9":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DESCONECTADO_9), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "10":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_10), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "11":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_EXCEDIDA_11), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "12":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_BLOQUEADA_12), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "13":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FUERA_RANGO_13), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "14":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_TELEPEAJE_14), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "15":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_LOCALIDAD_15), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "16":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LONGITUD_FUERA_RANGO_16), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "17":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_ECU_17), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "18":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TELEPEAJE_18), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "19":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_PREFIJO_19), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "20":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_PERSONALIZADO_20), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "21":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_LEIDO_21), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "22":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_CANCELA_OPER_22), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "23":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_CORRESPONDE_CON_TAG_LEIDO_23), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "24":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_PROCESO_24), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "25":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LECTOR_RF_OCUPADO_25), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "30":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_NO_VALIDO_30), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "31":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_FUERA_RANGO_31), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "40":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_EPC_40), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "41":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_EPC_41), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "42":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_EPC_42), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "43":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_EPC_43), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "44":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_TID_44), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "45":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_TID_45), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "46":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_TID_46), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "47":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_TID_47), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "48":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_48), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "49":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_49), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "50":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_50), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "51":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_51), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "61":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_61), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "62":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_62), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "63":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_63), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "64":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_64), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "98":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_OCUPADO_98), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                    case "99":
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DISPONIBLE_99), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        break;
                }
            }
        });
    }

    private void modifyShared(int position) {
        sharedPreferences = getApplicationContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        switch (position) {
            case 0:
                editor.putString(Constantes.KEY_CHECK1, "true");
                editor.commit();
                break;
            case 1:
                editor.putString(Constantes.KEY_CHECK2, "true");
                editor.commit();
                break;
            case 2:
                editor.putString(Constantes.KEY_CHECK3, "true");
                editor.commit();
                break;
            case 3:
                editor.putString(Constantes.KEY_CHECK4, "true");
                editor.commit();
                break;
            case 4:
                editor.putString(Constantes.KEY_CHECK5, "true");
                editor.commit();
                break;
            case 5:
                editor.putString(Constantes.KEY_CHECK6, "true");
                editor.commit();
                break;
            case 6:
                editor.putString(Constantes.KEY_CHECK7, "true");
                editor.commit();
                break;
            case 7:
                editor.putString(Constantes.KEY_CHECK8, "true");
                editor.commit();
                break;
            case 8:
                editor.putString(Constantes.KEY_CHECK9, "true");
                editor.commit();
                break;
            case 9:
                editor.putString(Constantes.KEY_CHECK10, "true");
                editor.commit();
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        RF.onPauseProcessNFC();
    }

    @Override
    protected void onResume() {
        super.onResume();
        RF.onResumeProcessNFC();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        RF.onNewIntentProcessNFC(intent);
    }

    class PrePersoUHF_Thread extends Thread {
        String folio;

        public PrePersoUHF_Thread(String folio) {
            this.folio = folio;
        }

        @Override
        public void run() {
            super.run();
            RF.prePersoTagUHF(folio);
        }
    }

    public void doPositiveClick(String folio, int position) {
        if (folio.length() > 0) {
            progressDialog = ProgressDialog.show(this, "PrePersonalizando Chip", "Espere por favor...");
//            dataVehicleByPlateNumber = new DataVehicleByPlateNumber(
//                    folio,
//                    null,
//                    null,
//                    null);
            this.position = position;
            new PrePersoUHF_Thread(folio).start();
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.dialogFolio), Toast.LENGTH_SHORT).show();
        }
    }

    public void showDialog_PrePerso(String folio, int position) {
        newFragment = Dialog_PrePersoUHF.newInstance(folio, position);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void reiniciarFolios() {
        if (newFragmentFolios.isVisible()) {
            newFragmentFolios.dismiss();
        }
        editor = sharedPreferences.edit();
        editor.putString(Constantes.KEY_CHECK1, "false");
        editor.putString(Constantes.KEY_CHECK2, "false");
        editor.putString(Constantes.KEY_CHECK3, "false");
        editor.putString(Constantes.KEY_CHECK4, "false");
        editor.putString(Constantes.KEY_CHECK5, "false");
        editor.putString(Constantes.KEY_CHECK6, "false");
        editor.putString(Constantes.KEY_CHECK7, "false");
        editor.putString(Constantes.KEY_CHECK8, "false");
        editor.putString(Constantes.KEY_CHECK9, "false");
        editor.putString(Constantes.KEY_CHECK10, "false");

        editor.putString(Constantes.KEY_CHECKDOTADO1, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO2, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO3, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO4, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO5, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO6, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO7, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO8, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO9, "false");
        editor.putString(Constantes.KEY_CHECKDOTADO10, "false");
        editor.commit();
    }

    private void showAlert_Folio_PrePerso() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.alertTitlePreguntaPrePerso))
                .setMessage(getResources().getString(R.string.preguntaPrePerso))
                .setPositiveButton(R.string.persoDialogOK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        loadArrayFromShaerd();
                        showDialog_Folios();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.dialogPrePersoCancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void showDialogSettings() {
        DialogFragment dialogFragment = DialogSettings.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void searchVehicleWithoutTag() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                "http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null) + "/rest/vehicles/vehiclesWithoutTag",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i(TAG + "Vehicle Without TAG ", response.toString());
                        readJson_VehicleWithoutTag(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Auth();
            }

        };
        VolleyApp.getmInstance().addToRequestQueue(jsonArrayRequest);
    }

    private void readJson_VehicleWithoutTag(JSONArray response) {
        JSONVehicleWithoutTAG jsonVehicleWithoutTAG = new JSONVehicleWithoutTAG();
        jsonVehicleWithoutTAG.execute(response);
    }

    class JSONVehicleWithoutTAG extends AsyncTask<JSONArray, Void, Boolean> {
        boolean result = false;

        @Override
        protected Boolean doInBackground(JSONArray... params) {
            vehiclesWithoutTagArrayList = new ArrayList<>();
            for (int i = 0; i < params[0].length(); i++) {

                try {
                    JSONObject json_data = (JSONObject) params[0].get(i);
                    VehiclesWithoutTag vehiclesWithoutTag = new VehiclesWithoutTag(
                            String.valueOf(json_data.getInt("model")),
                            json_data.getString("status"),
                            json_data.getString("fuelType"),
                            json_data.getString("vin"),
                            json_data.getString("vehicleType"),
                            String.valueOf(json_data.getInt("litersPerMonth")),
                            String.valueOf(json_data.getBoolean("subsidization")),
                            json_data.getString("licensePlate"));
                    vehiclesWithoutTagArrayList.add(vehiclesWithoutTag);
                    result = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    result = false;
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                showDialogVehicleWhitoutTag();
            }
        }
    }

    private void showDialogVehicleWhitoutTag() {
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");

        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        newFragmentVehicleWithout = DialogVehicleWithoutTAG.newInstance(vehiclesWithoutTagArrayList);
        newFragmentVehicleWithout.show(ft, "dialog");
    }

    public void cerrarDialogVehicle() {
        newFragmentVehicleWithout.dismiss();
    }
}
