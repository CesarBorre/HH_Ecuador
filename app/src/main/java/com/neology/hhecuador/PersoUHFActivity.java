package com.neology.hhecuador;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.atid.lib.media.SoundPlayer;
import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.hhecuador.dialogs.DialogPerso;
import com.neology.hhecuador.dialogs.DialogPersoNFC;
import com.neology.hhecuador.dialogs.DialogPersoOk;
import com.neology.hhecuador.dialogs.DialogPersoUHF;
import com.neology.hhecuador.model.DataVehicleByPlateNumber;
import com.neology.hhecuador.model.TID_TAG;
import com.neology.hhecuador.model.VehicleTag;
import com.neology.hhecuador.utils.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PersoUHFActivity extends AppCompatActivity implements RF_Listener {

    private RF_Control RF;
    ProgressDialog progressDialogUHF, progressDialogNFC;

    private SoundPlayer sound_success;
    private SoundPlayer sound_error;
    private SoundPlayer sound_seeking;

    DataVehicleByPlateNumber dataVehicleByPlateNumber;

    String folioDisponible = "";
    int positionDisponible = 0;

    String tidUHF = "";
    String tidNFC = "";
    String requestBody = "";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    DialogPerso dialogFragment;
    DialogPersoUHF dialogFragmentUhf;
    DialogPersoNFC dialogFragmentnfc;
    DialogPersoOk dialogFragmentOk;

    TID_TAG tid_tag;

    VehicleTag vehicleTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        showDialog_Perso();
        setContentView(R.layout.activity_perso_uhf);
        Intent intent = getIntent();
        dataVehicleByPlateNumber = intent.getParcelableExtra("data");
        folioDisponible = intent.getStringExtra("folioDisponible");
        positionDisponible = intent.getIntExtra("positionDisponible", positionDisponible);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        initElements();
    }

    private void initElements() {
        RF = new RF_Control();
        RF.Initialize(this);
        progressDialogUHF = new ProgressDialog(this);
        progressDialogNFC = new ProgressDialog(this);
        sound_seeking = new SoundPlayer(this, R.raw.success);
        sound_success = new SoundPlayer(this, R.raw.ok);
        sound_error = new SoundPlayer(this, R.raw.error);

        ImageView imageView = (ImageView) findViewById(R.id.popPresentarID);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap preview_bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.popup_info, options);
        imageView.setImageBitmap(preview_bitmap);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(this.getComponentName().getClassName(), "Key-" + keyCode + " - KeyEvent-" + event);
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
            showDialog_PersoUHF();
            new persoUHF_Thread().start();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return false;
        } else {
            return false;
        }
    }

    @Override
    public void Event_RF_Control(String data) {
        resultGatillo(data);
    }

    @Override
    public void Event_RF_Log(String data) {
        resultGatillo(data);
    }

    private void resultGatillo(final String data) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String[] resultStr = data.split("\\|");
                Log.i(PersoUHFActivity.this.getComponentName().getClassName(), "--------- RESULT CODE FROM API: " + data);
                switch (resultStr[0]) {
                    case "0":
//                        Perso UHF
//                        Operación exitosa|*Personalización UHF|4E454355000186A1E1045A73|E20034120132C0FFE1045A73
//                        tidUHF = resultStr[3];
//                        NFC
//                        0|2|4E454355000186A1E1045A73|E20034120132C0FFE1045A73|04B7C2DA1B2380

                        Toast.makeText(getApplicationContext(), "Operación Exitosa", Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        switch (resultStr[1]) {
                            //Presenta NFC
                            case "7":
                                showDialog_Personfc();
                                modifyShared(positionDisponible);
//                                showProgressNFC();

//                                showAlertOK_Perso();
                                break;
                            //Termina NFC correctamente
                            case "2":
                                ;
                                tidUHF = resultStr[3];
                                tidNFC = resultStr[4];
//                                tid_tag = ((tid_tag != null) ? tid_tag : new TID_TAG(dataVehicleByPlateNumber.getPlateNumber(), tidUHF, tidNFC));
//                                tid_tag.setTidUHF(resultStr[3]); // Set UHF ID
//                                tid_tag.setTidNFC(resultStr[4]); // Set UHF ID
//                                tid_tag.setNumberPlate(dataVehicleByPlateNumber.getPlateNumber());
                                dialogFragmentnfc.dismiss();
//                                progressDialogNFC.dismiss();
//                                showAlertOK_Perso();
//                                asociarTagVehiculo();
//                                postNewComment(getApplicationContext(), dataVehicleByPlateNumber.getPlateNumber(), tidUHF, tidNFC);
//                                post( dataVehicleByPlateNumber.getPlateNumber(), tidUHF, tidNFC);
//                                new SendDataToServer().execute(dataVehicleByPlateNumber.getPlateNumber(), tidUHF, tidNFC);
                                vehicleTag = new VehicleTag(dataVehicleByPlateNumber.getPlateNumber(), tidUHF, tidNFC);
                                showDialog_PersoOk();
                            case "1":
//                                showProgressNFC();
//                                showAlertOK_Perso();
                                showDialog_PersoOk();
                                break;
                        }
                        break;
                    case "1":
                        showAlert_Error(R.string.NO_TAG_CAMPO_LECTURA_1);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.NO_TAG_CAMPO_LECTURA_1), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "2":
                        showAlert_Error(R.string.MAS_1_TAG_CAMPO_LECTURA_2);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MAS_1_TAG_CAMPO_LECTURA_2), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "3":
                        showAlert_Error(R.string.ERROR_LECTURA_3);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_3), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "4":
                        showAlert_Error(R.string.PROBLEMAS_CONEXION_LECTOR_4);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PROBLEMAS_CONEXION_LECTOR_4), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "5":
                        showAlert_Error(R.string.ERROR_CONFIG_RFID_5);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_CONFIG_RFID_5), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "6":
                        showAlert_Error(R.string.ERROR_TIMEOUT_6);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_6), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "7":
                        showAlert_Error(R.string.ERROR_RFID_NO_INIC_7);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_RFID_NO_INIC_7), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "8":
                        showAlert_Error(R.string.INIC_RFID_8);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_RFID_8), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "9":
                        showAlert_Error(R.string.RFID_DESCONECTADO_9);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DESCONECTADO_9), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "10":
                        showAlert_Error(R.string.ERROR_FORMATO_10);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_10), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "11":
                        showAlert_Error(R.string.MEMORIA_EXCEDIDA_11);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_EXCEDIDA_11), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "12":
                        showAlert_Error(R.string.MEMORIA_BLOQUEADA_12);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.MEMORIA_BLOQUEADA_12), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "13":
                        showAlert_Error(R.string.FUERA_RANGO_13);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FUERA_RANGO_13), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "14":
                        showAlert_Error(R.string.ERROR_FORMATO_TELEPEAJE_14);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_TELEPEAJE_14), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "15":
                        showAlert_Error(R.string.ERROR_ESCRITURA_LOCALIDAD_15);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_LOCALIDAD_15), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "16":
                        showAlert_Error(R.string.LONGITUD_FUERA_RANGO_16);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LONGITUD_FUERA_RANGO_16), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "17":
                        showAlert_Error(R.string.ERROR_FORMATO_ECU_17);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_FORMATO_ECU_17), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "18":
                        showAlert_Error(R.string.ERROR_TELEPEAJE_18);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TELEPEAJE_18), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "19":
                        showAlert_Error(R.string.ERROR_PREFIJO_19);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_PREFIJO_19), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "20":
                        showAlert_Error(R.string.ERROR_TAG_NO_PERSONALIZADO_20);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_PERSONALIZADO_20), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "21":
                        showAlert_Error(R.string.ERROR_TAG_NO_LEIDO_21);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_LEIDO_21), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "22":
                        showAlert_Error(R.string.ERROR_TIMEOUT_CANCELA_OPER_22);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TIMEOUT_CANCELA_OPER_22), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "23":
                        showAlert_Error(R.string.ERROR_TAG_NO_CORRESPONDE_CON_TAG_LEIDO_23);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_TAG_NO_CORRESPONDE_CON_TAG_LEIDO_23), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "24":
                        showAlert_Error(R.string.INIC_PROCESO_24);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_PROCESO_24), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "25":
                        showAlert_Error(R.string.LECTOR_RF_OCUPADO_25);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.LECTOR_RF_OCUPADO_25), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "30":
                        showAlert_Error(R.string.FOLIO_NO_VALIDO_30);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_NO_VALIDO_30), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "31":
                        showAlert_Error(R.string.FOLIO_FUERA_RANGO_31);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.FOLIO_FUERA_RANGO_31), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "40":
                        showAlert_Error(R.string.INIC_LECTURA_EPC_40);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_EPC_40), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "41":
                        showAlert_Error(R.string.ERROR_LECTURA_EPC_41);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_EPC_41), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "42":
                        showAlert_Error(R.string.INIC_ESCRITURA_EPC_42);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_EPC_42), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "43":
                        showAlert_Error(R.string.ERROR_ESCRITURA_EPC_43);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_EPC_43), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "44":
                        showAlert_Error(R.string.INIC_LECTURA_TID_44);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_TID_44), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "45":
                        showAlert_Error(R.string.ERROR_LECTURA_TID_45);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_TID_45), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "46":
                        showAlert_Error(R.string.INIC_ESCRITURA_TID_46);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_TID_46), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "47":
                        showAlert_Error(R.string.ERROR_ESCRITURA_TID_47);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_TID_47), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "48":
                        showAlert_Error(R.string.INIC_LECTURA_USER_48);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_48), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "49":
                        showAlert_Error(R.string.ERROR_LECTURA_USER_49);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_49), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "50":
                        showAlert_Error(R.string.INIC_ESCRITURA_USER_50);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_50), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "51":
                        showAlert_Error(R.string.ERROR_ESCRITURA_USER_51);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_51), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "61":
                        showAlert_Error(R.string.INIC_LECTURA_USER_61);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_LECTURA_USER_61), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "62":
                        showAlert_Error(R.string.ERROR_LECTURA_USER_62);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_LECTURA_USER_62), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "63":
                        showAlert_Error(R.string.INIC_ESCRITURA_USER_63);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.INIC_ESCRITURA_USER_63), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "64":
                        showAlert_Error(R.string.ERROR_ESCRITURA_USER_64);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.ERROR_ESCRITURA_USER_64), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "98":
                        showAlert_Error(R.string.RFID_OCUPADO_98);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_OCUPADO_98), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                    case "99":
                        showAlert_Error(R.string.RFID_DISPONIBLE_99);
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.RFID_DISPONIBLE_99), Toast.LENGTH_SHORT).show();
//                        progressDialogUHF.dismiss();
                        dialogFragmentUhf.dismiss();
                        break;
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        RF.onPauseProcessNFC();
    }

    @Override
    protected void onResume() {
        super.onResume();
        RF.onResumeProcessNFC();
        RF = new RF_Control();
        RF.Initialize(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        RF.onNewIntentProcessNFC(intent);
    }

    class persoUHF_Thread extends Thread {

        @Override
        public void run() {
            super.run();
//            1 Diesel
//            2 Gasolna
//            1 Auto
//            2 Pick-up
//            3 Camion
//            4 Tracto-camion
//            5 Doble Remolque
//            6 Utility
            String catCombus = "";
            if (dataVehicleByPlateNumber.getFuelType().equals("Gasolina")) {
                catCombus = "2";
            } else if (dataVehicleByPlateNumber.getFuelType().equals("Diesel")) {
                catCombus = "1";
            }
            String catVeh = "";
            if (dataVehicleByPlateNumber.getVehicleType().equals("Auto")) {
                catVeh = "1";
            } else if (dataVehicleByPlateNumber.getVehicleType().equals("Pick-up")) {
                catVeh = "2";
            } else if (dataVehicleByPlateNumber.getVehicleType().equals("Camion")) {
                catVeh = "3";
            } else if (dataVehicleByPlateNumber.getVehicleType().equals("Tracto-camion")) {
                catVeh = "4";
            } else if (dataVehicleByPlateNumber.getVehicleType().equals("Doble Remolque")) {
                catVeh = "5";
            } else if (dataVehicleByPlateNumber.getVehicleType().equals("Utility")) {
                catVeh = "6";
            }

            String sub = "";
            if (dataVehicleByPlateNumber.getSubsidization().equals("true")) {
                sub = "1";
            } else if (dataVehicleByPlateNumber.getSubsidization().equals("false")) {
                sub = "0";
            }
//            RF.prePersoTagUHF(folioDisponible);
            RF.persoTagUHF(
                    folioDisponible, //String folio
                    dataVehicleByPlateNumber.getPlateNumber(), //String Placa
                    dataVehicleByPlateNumber.getVin(), //String sVin
                    catVeh, //String TipoV--int uTypeVehicle = Integer.parseInt(TipoV);
                    catCombus, //String TypeCombus--int uTypeCombus = Integer.parseInt(TypeCombus);
                    sub, //String Subsidio--int uTypeSubsidio = Integer.parseInt(Subsidio);
                    dataVehicleByPlateNumber.getLitersPerMonth(), //String Galones
                    DateFormat.format("ddMMyyyy", new Date()).toString(), //String FechaRec
                    "0.00"); //String GalonesConsu
        }
    }

    private void showProgressNFC() {
        progressDialogNFC = ProgressDialog.show(this,
                getResources().getString(R.string.persoDialogTitleNFC),
                "PRESENTAR TAG NFC");
    }

    public void persoOk() {
//        new AlertDialog.Builder(this)
//                .setTitle(getResources().getString(R.string.persoDialogTitleUHF))
//                .setMessage(getResources().getString(R.string.persoLecturaMsg))
//                .setPositiveButton(R.string.persoDialogOK, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intent = new Intent(getApplicationContext(), LecturaTag.class);
//                        intent.putExtra("data", dataVehicleByPlateNumber);
//                        intent.putExtra("folioDisponible", folioDisponible);
//                        startActivity(intent);
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
        Intent intent = new Intent(getApplicationContext(), LecturaTag.class);
        intent.putExtra("data", dataVehicleByPlateNumber);
        intent.putExtra("folioDisponible", folioDisponible);
        intent.putExtra("vehicleTag", vehicleTag);
        startActivity(intent);
        finish();
    }

    private void showAlert_Error(int codeError) {
        new AlertDialog.Builder(this)
                .setTitle("INFORMACIÓN")
                .setMessage(codeError)
                .setPositiveButton(R.string.persoDialogOK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void modifyShared(int position) {
        editor = sharedPreferences.edit();
        switch (position) {
            case 0:
                editor.putString(Constantes.KEY_CHECKDOTADO1, "true");
                editor.commit();
                break;
            case 1:
                editor.putString(Constantes.KEY_CHECKDOTADO2, "true");
                editor.commit();
                break;
            case 2:
                editor.putString(Constantes.KEY_CHECKDOTADO3, "true");
                editor.commit();
                break;
            case 3:
                editor.putString(Constantes.KEY_CHECKDOTADO4, "true");
                editor.commit();
                break;
            case 4:
                editor.putString(Constantes.KEY_CHECKDOTADO5, "true");
                editor.commit();
                break;
            case 5:
                editor.putString(Constantes.KEY_CHECKDOTADO6, "true");
                editor.commit();
                break;
            case 6:
                editor.putString(Constantes.KEY_CHECKDOTADO7, "true");
                editor.commit();
                break;
            case 7:
                editor.putString(Constantes.KEY_CHECKDOTADO8, "true");
                editor.commit();
                break;
            case 8:
                editor.putString(Constantes.KEY_CHECKDOTADO9, "true");
                editor.commit();
                break;
            case 9:
                editor.putString(Constantes.KEY_CHECKDOTADO10, "true");
                editor.commit();
                break;
        }
    }

    public void showDialog_Perso() {
        dialogFragment = DialogPerso.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void showDialog_PersoUHF() {
        dialogFragmentUhf = DialogPersoUHF.newInstance();
        dialogFragmentUhf.show(getSupportFragmentManager(), "dialog");
    }

    public void showDialog_Personfc() {
        dialogFragmentnfc = DialogPersoNFC.newInstance();
        dialogFragmentnfc.show(getSupportFragmentManager(), "dialog");
    }

    public void showDialog_PersoOk() {
        dialogFragmentOk = DialogPersoOk.newInstance();
        dialogFragmentOk.show(getSupportFragmentManager(), "dialog");
    }

    public static HashMap Auth() {
        HashMap<String, String> params = new HashMap<String, String>();
        String creds = String.format("%s:%s", "raidentrance", "Jokowis123");
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
//        params.put("Content-Type", "application/json; charset=utf-8");
        params.put("Authorization", auth);
        return params;
    }

    public void postNewComment(Context context, final String plate, final String UHF, final String NFC) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("plate", plate);
            jsonObject.put("tidUhf", UHF);
            jsonObject.put("tidNfc", NFC);
            requestBody = jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST,
                "http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null) + "/rest/vehicles/vehicle/relateTag", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(null, response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(null, error.toString());
            }
        }) {

            //        JsonObjectRequest myRequest = new JsonObjectRequest(
//                Request.Method.POST,
//                "http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null) + "/rest/vehicles/vehicle/relateTag",
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(null, response.toString());
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d(null, error.toString());
//                    }
//                }) {
//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return Auth();
            }
        };
        VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
    }

    class SendDataToServer extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("plate", params[0]);
                jsonObject.put("tidUhf", params[1]);
                jsonObject.put("tidNfc", params[2]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {

                // 1. URL
                URL url = new URL("http://" + sharedPreferences.getString(Constantes.kEY_IP_PORT, null) + "/rest/vehicles/vehicle/relateTag");

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true); // Set Http method to POST
                urlConnection.setChunkedStreamingMode(0); // Use default chunk size

                // Write serialized JSON data to output stream.
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                writer.write(jsonObject.toString());

                // Close streams and disconnect.
                writer.close();
                out.close();
                urlConnection.disconnect();

                Log.d(null, "RESPONSE " + urlConnection.getResponseCode());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
        }

    }

}




