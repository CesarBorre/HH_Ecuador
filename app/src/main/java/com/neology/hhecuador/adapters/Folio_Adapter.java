package com.neology.hhecuador.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.neology.hhecuador.MainActivity;
import com.neology.hhecuador.PlacaActivity;
import com.neology.hhecuador.R;
import com.neology.hhecuador.model.Folios;
import com.neology.hhecuador.utils.Constantes;

import java.util.ArrayList;

/**
 * Created by root on 18/02/16.
 */
public class Folio_Adapter extends RecyclerView.Adapter<Folio_Adapter.Folio_Adapter_ViewHolder> {

    ArrayList<Folios> foliosArrayList;
    Activity a;

    SharedPreferences sharedPreferences;

    public Folio_Adapter (ArrayList<Folios> foliosArrayList, Activity a) {
        this.foliosArrayList = foliosArrayList;
        this.a = a;
    }

    @Override
    public Folio_Adapter_ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        sharedPreferences = a.getApplicationContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_dialog_folio, viewGroup, false);
        return new Folio_Adapter_ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Folio_Adapter_ViewHolder holder, final int position) {
        final Folios folios = foliosArrayList.get(position);
        holder.folio.setText(folios.getFolio());
        holder.checkedFolio.setChecked(folios.isChecked());
        holder.checkedFolioDotado.setChecked(folios.isCheckedDotado());
        holder.folioCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!folios.isChecked() && !folios.isCheckedDotado()) {
                    ((PlacaActivity) a).showDialog_PrePerso(holder.folio.getText().toString(), position);
                } else {
                    Toast.makeText(a.getApplicationContext(),
                            "El folio "+folios.getFolio().toString()+" ha sido dotado",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return foliosArrayList.size();
    }

    public static class Folio_Adapter_ViewHolder extends RecyclerView.ViewHolder {
        public TextView folio;
        public CheckBox checkedFolio;
        public CheckBox checkedFolioDotado;
        public CardView folioCard;

        public Folio_Adapter_ViewHolder(View itemView) {
            super(itemView);
            folio = (TextView) itemView.findViewById(R.id.folioAssignID);
            checkedFolio = (CheckBox) itemView.findViewById(R.id.checkFolioID);
            checkedFolio.setEnabled(false);
            checkedFolioDotado = (CheckBox) itemView.findViewById(R.id.checkFolioDotadoID);
            checkedFolioDotado.setEnabled(false);
            folioCard = (CardView) itemView.findViewById(R.id.folio_card_viewID);
        }
    }
}
