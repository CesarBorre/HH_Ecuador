package com.neology.hhecuador.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.neology.hhecuador.R;
import com.neology.hhecuador.model.VehiclesWithoutTag;

import java.util.ArrayList;

/**
 * Created by root on 1/03/16.
 */
public class VehiclesWithoutTAG_Adapter extends RecyclerView.Adapter<VehiclesWithoutTAG_Adapter.VehiclesWithoutTAG_Adapter_ViewHolder>{
    ArrayList<VehiclesWithoutTag> vehiclesWithoutTagArrayList;
    Activity a;

    public VehiclesWithoutTAG_Adapter(ArrayList<VehiclesWithoutTag> vehiclesWithoutTagArrayList, Activity a) {
        this.vehiclesWithoutTagArrayList = vehiclesWithoutTagArrayList;
        this.a = a;
    }

    @Override
    public VehiclesWithoutTAG_Adapter_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dialog_vehicle_whitouttag, parent, false);
        return new VehiclesWithoutTAG_Adapter_ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(VehiclesWithoutTAG_Adapter_ViewHolder holder, int position) {
        VehiclesWithoutTag vehiclesWithoutTag = vehiclesWithoutTagArrayList.get(position);
        holder.placa.setText(vehiclesWithoutTag.getLicensePlate());
        holder.vin.setText(vehiclesWithoutTag.getVin());
        holder.modelo.setText(vehiclesWithoutTag.getModel());
        holder.status.setText(vehiclesWithoutTag.getStatus());
    }

    @Override
    public int getItemCount() {
        return vehiclesWithoutTagArrayList.size();
    }

    public static class VehiclesWithoutTAG_Adapter_ViewHolder extends RecyclerView.ViewHolder {
        public TextView placa;
        public TextView vin;
        public TextView modelo;
        public TextView status;

        public VehiclesWithoutTAG_Adapter_ViewHolder(View itemView) {
            super(itemView);
            placa = (TextView)itemView.findViewById(R.id.placaDialogID);
            vin = (TextView)itemView.findViewById(R.id.vinDialogID);
            modelo = (TextView)itemView.findViewById(R.id.modeloDialogID);
            status = (TextView)itemView.findViewById(R.id.statusDialogID);
        }
    }
}
