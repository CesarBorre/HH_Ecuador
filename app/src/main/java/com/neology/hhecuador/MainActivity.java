package com.neology.hhecuador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.hhecuador.dialogs.Dialog_Folios;
import com.neology.hhecuador.dialogs.Dialog_PrePersoUHF;
import com.neology.hhecuador.model.DataVehicleByPlateNumber;
import com.neology.hhecuador.model.Folios;
import com.neology.hhecuador.utils.Constantes;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private Button grabarCHipBtn;

    ProgressDialog progressDialog;
    AlertDialog dialog;
    DialogFragment newFragment;

    DataVehicleByPlateNumber dataVehicleByPlateNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadWebView();
        initElements();
    }

    private void initElements() {

        grabarCHipBtn = (Button) findViewById(R.id.grabarChipID);
        grabarCHipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showDialog_PrePerso();
//                dataVehicleByPlateNumber = new DataVehicleByPlateNumber(
//                        "3041675",
//                        null,
//                        null,
//                        null);
                Intent intent = new Intent(getApplicationContext(), PlacaActivity.class);
                intent.putExtra("data", dataVehicleByPlateNumber);
                startActivity(intent);
            }
        });
    }

    private void loadWebView() {
        WebView wv = (WebView) findViewById(R.id.webView);
//        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/tap.html");
    }







    private void showAlertPersoOK() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.alertMsgPrePerso)
                .setTitle(R.string.alertTitlePrePerso)
                .setPositiveButton(R.string.alertOkPrePerso,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
        dialog = builder.create();
    }




}
