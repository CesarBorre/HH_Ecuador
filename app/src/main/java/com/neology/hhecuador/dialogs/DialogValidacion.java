package com.neology.hhecuador.dialogs;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neology.hhecuador.DatosPlacaActivity;
import com.neology.hhecuador.R;

/**
 * Created by root on 27/02/16.
 */
public class DialogValidacion extends DialogFragment {
    ImageView continuar;
    ImageView cancelar;

    RelativeLayout fondLayout;

    public static DialogValidacion newInstance() {
        DialogValidacion dialogValidacion = new DialogValidacion();
        return dialogValidacion;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.dialog_validacion, container, false);
        continuar = (ImageView)v.findViewById(R.id.continuarID);
        continuar.setOnClickListener(continuarListener);
        cancelar = (ImageView)v.findViewById(R.id.cancelarID);
        cancelar.setOnClickListener(cancelarListener);
        fondLayout = (RelativeLayout)v.findViewById(R.id.fondoID);
        fondLayout.setBackgroundResource(R.drawable.back_gris);
        fondLayout.getBackground().setAlpha(182);
        return v;
    }

    private View.OnClickListener continuarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((DatosPlacaActivity)getActivity()).ok();
        }
    };

    private View.OnClickListener cancelarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((DatosPlacaActivity)getActivity()).closeDialog();
        }
    };
}
