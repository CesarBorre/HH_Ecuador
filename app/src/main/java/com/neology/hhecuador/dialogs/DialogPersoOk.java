package com.neology.hhecuador.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neology.hhecuador.PersoUHFActivity;
import com.neology.hhecuador.R;

/**
 * Created by root on 27/02/16.
 */
public class    DialogPersoOk extends DialogFragment {

    RelativeLayout fondLayout;
    ImageView aceptar;

    public static DialogPersoOk newInstance() {
        DialogPersoOk dialogPerso = new DialogPersoOk();
        return dialogPerso;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.dialog_perso_ok, container, false);
        fondLayout = (RelativeLayout)v.findViewById(R.id.fondoPersoOkID);
        fondLayout.setBackgroundResource(R.drawable.back_gris);
        fondLayout.getBackground().setAlpha(182);
        aceptar = (ImageView)v.findViewById(R.id.aceptarID);
        aceptar.setOnClickListener(aceptarListener);
        return v;
    }

    private View.OnClickListener aceptarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((PersoUHFActivity)getActivity()).persoOk();
        }
    };
}
