package com.neology.hhecuador.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.neology.hhecuador.R;

/**
 * Created by root on 27/02/16.
 */
public class DialogLectura extends DialogFragment {

    RelativeLayout fondLayout;

    public static DialogLectura newInstance() {
        DialogLectura dialogPerso = new DialogLectura();
        return dialogPerso;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.dialog_lectra, container, false);
        fondLayout = (RelativeLayout)v.findViewById(R.id.fondoLecturaID);
        fondLayout.setBackgroundResource(R.drawable.back_gris);
        fondLayout.getBackground().setAlpha(182);
        return v;
    }
}
