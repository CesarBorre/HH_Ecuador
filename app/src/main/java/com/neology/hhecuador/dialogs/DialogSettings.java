package com.neology.hhecuador.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neology.hhecuador.PlacaActivity;
import com.neology.hhecuador.R;
import com.neology.hhecuador.utils.Constantes;

/**
 * Created by root on 27/02/16.
 */
public class DialogSettings extends DialogFragment {
    EditText ip;
    EditText usr;
    EditText pwd;

    RelativeLayout fondLayout;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static DialogSettings newInstance() {
        DialogSettings dialogPerso = new DialogSettings();
        return dialogPerso;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getContext().getSharedPreferences(Constantes.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_settings, null);
        ip = (EditText)v.findViewById(R.id.portID);
        usr = (EditText)v.findViewById(R.id.usrID);
        pwd = (EditText)v.findViewById(R.id.pwdID);


        return new AlertDialog.Builder(getContext())
                .setTitle("CONFGURACIÓN")
                .setView(v)
                .setPositiveButton(R.string.alertOkPrePerso,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                changeIP();
                            }
                        })
                .setNegativeButton(R.string.dialogPrePersoCancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }

    private void changeIP() {
        if(usr.getText().length()>0&&pwd.getText().length()>0&&ip.getText().length()>0) {
            if(usr.getText().toString().equals("NEOLOGY")&&pwd.getText().toString().equals("NeologyNFC")){
                editor = sharedPreferences.edit();
                editor.putString(Constantes.kEY_IP_PORT, ip.getText().toString());
                editor.commit();
                Toast.makeText(getContext(), "Configuración exitosa", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Datos inválidos", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Ingresar datos completos", Toast.LENGTH_SHORT).show();
        }
    }
}
