package com.neology.hhecuador.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neology.hhecuador.DatosPlacaActivity;
import com.neology.hhecuador.R;

/**
 * Created by root on 27/02/16.
 */
public class DialogPerso extends DialogFragment {

    RelativeLayout fondLayout;

    public static DialogPerso newInstance() {
        DialogPerso dialogPerso = new DialogPerso();
        return dialogPerso;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.dialog_perso, container, false);
        fondLayout = (RelativeLayout)v.findViewById(R.id.fondoPersoID);
        fondLayout.setBackgroundResource(R.drawable.back_gris);
        fondLayout.getBackground().setAlpha(182);
        return v;
    }
}
