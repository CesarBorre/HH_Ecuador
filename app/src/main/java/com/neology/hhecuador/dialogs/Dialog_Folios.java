package com.neology.hhecuador.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.neology.hhecuador.PlacaActivity;
import com.neology.hhecuador.R;
import com.neology.hhecuador.adapters.Folio_Adapter;
import com.neology.hhecuador.model.Folios;

import java.util.ArrayList;

/**
 * Created by root on 18/02/16.
 */
public class Dialog_Folios extends DialogFragment {

    private RecyclerView recyclerView;
    ArrayList<Folios> foliosArrayList;
    Button reiniciarFolios;

    public static Dialog_Folios newInstance(ArrayList<Folios> foliosArrayList) {
        Dialog_Folios dialog_folios = new Dialog_Folios();
        Bundle bundle= new Bundle();
        bundle.putParcelableArrayList("list", foliosArrayList);
        dialog_folios.setArguments(bundle);
        return dialog_folios;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        foliosArrayList = getArguments().getParcelableArrayList("list");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_folio, container, false);
        reiniciarFolios = (Button)v.findViewById(R.id.reiniciarFoliosBtn);
        reiniciarFolios.setOnClickListener(reiniciarBtnListener);
        createRecycler(v);
        return v;
    }

    private void createRecycler(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerFoliosID);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        Folio_Adapter movementsAdapter = new Folio_Adapter(foliosArrayList, getActivity());
        recyclerView.setAdapter(movementsAdapter);
    }

    private View.OnClickListener reiniciarBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((PlacaActivity)getActivity()).reiniciarFolios();
        }
    };
}
