package com.neology.hhecuador.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.neology.hhecuador.PlacaActivity;
import com.neology.hhecuador.R;
import com.neology.hhecuador.adapters.VehiclesWithoutTAG_Adapter;
import com.neology.hhecuador.model.VehiclesWithoutTag;

import java.util.ArrayList;

/**
 * Created by root on 27/02/16.
 */
public class DialogVehicleWithoutTAG extends DialogFragment {
    private RecyclerView recyclerView;
    Button cerrar;
    ArrayList<VehiclesWithoutTag> vehiclesWithoutTagArrayList;

    public static DialogVehicleWithoutTAG newInstance(ArrayList<VehiclesWithoutTag> vehiclesWithoutTagArrayList) {
        DialogVehicleWithoutTAG dialogVehicleWithoutTAG = new DialogVehicleWithoutTAG();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("vehiclesWithoutTagArrayList", vehiclesWithoutTagArrayList);
        dialogVehicleWithoutTAG.setArguments(bundle);
        return dialogVehicleWithoutTAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        vehiclesWithoutTagArrayList = getArguments().getParcelableArrayList("vehiclesWithoutTagArrayList");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.dialog_vehiclewithout, container, false);
        cerrar = (Button)v.findViewById(R.id.cerrarDialogID);
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PlacaActivity)getActivity()).cerrarDialogVehicle();
            }
        });
        createRecycler(v);
        return v;
    }

    private void createRecycler(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerVechicleWithoutID);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        VehiclesWithoutTAG_Adapter vehiclesWithoutTAG_adapter = new VehiclesWithoutTAG_Adapter(vehiclesWithoutTagArrayList, getActivity());
        recyclerView.setAdapter(vehiclesWithoutTAG_adapter);
    }
}
