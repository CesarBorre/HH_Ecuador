package com.neology.hhecuador.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.neology.hhecuador.MainActivity;
import com.neology.hhecuador.PlacaActivity;
import com.neology.hhecuador.R;

/**
 * Created by root on 17/02/16.
 */
public class Dialog_PrePersoUHF extends DialogFragment {

    String folio;
    int position = 0;

    public static Dialog_PrePersoUHF newInstance(String folio, int position) {
        Dialog_PrePersoUHF dialog_prePersoUHF = new Dialog_PrePersoUHF();
        Bundle bundle = new Bundle();
        bundle.putString("folio", folio);
        bundle.putInt("position", position);
        dialog_prePersoUHF.setArguments(bundle);
        return dialog_prePersoUHF;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        folio = getArguments().getString("folio");
        position =  getArguments().getInt("position");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_preperso_uhf, null);

        final EditText folioTxt = (EditText) v.findViewById(R.id.folioPreTxtID);
        folioTxt.setEnabled(false);
        folioTxt.setText(folio);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Pre-Personalización TAG UHF")
                .setView(v)
                .setPositiveButton(R.string.dialogPrePersoOk,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((PlacaActivity) getActivity()).doPositiveClick(folioTxt.getText().toString(), position);
                            }
                        })
                .setNegativeButton(R.string.dialogPrePersoCancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
    }
}
