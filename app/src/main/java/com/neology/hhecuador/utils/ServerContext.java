package com.neology.hhecuador.utils;

/**
 * Created by Neo01 on 28/05/2015.
 */
public class ServerContext {
    private String username;
    private String password;
    private String serverUrl;


    public ServerContext()
    {
        // TODO Auto-generated constructor stub
    }

    public ServerContext(String username, String password, String serverUrl)
    {
        super();
        this.username = username;
        this.password = password;
        this.serverUrl = serverUrl;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getServerUrl()
    {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl)
    {
        this.serverUrl = serverUrl;
    }

}
