package com.neology.hhecuador.utils;

/**
 * Created by root on 18/02/16.
 */
public class Constantes {

    public static final String SHARED_PREF_NAME = "HH_GAS_ECU";

    public static final String KEY_FOLIO1 = "FOLIO1";
    public static final String KEY_FOLIO2 = "FOLIO2";
    public static final String KEY_FOLIO3 = "FOLIO3";
    public static final String KEY_FOLIO4 = "FOLIO4";
    public static final String KEY_FOLIO5 = "FOLIO5";
    public static final String KEY_FOLIO6 = "FOLIO6";
    public static final String KEY_FOLIO7 = "FOLIO7";
    public static final String KEY_FOLIO8 = "FOLIO8";
    public static final String KEY_FOLIO9 = "FOLIO9";
    public static final String KEY_FOLIO10 = "FOLIO10";

    public static final String SHARED_PREF_FOLIO1 = "100001";
    public static final String SHARED_PREF_FOLIO2 = "100002";
    public static final String SHARED_PREF_FOLIO3 = "100003";
    public static final String SHARED_PREF_FOLIO4 = "100004";
    public static final String SHARED_PREF_FOLIO5 = "100005";
    public static final String SHARED_PREF_FOLIO6 = "100006";
    public static final String SHARED_PREF_FOLIO7 = "100007";
    public static final String SHARED_PREF_FOLIO8 = "100008";
    public static final String SHARED_PREF_FOLIO9 = "100009";
    public static final String SHARED_PREF_FOLIO10 = "100010";

    public static final String KEY_CHECK1 = "CHECK1";
    public static final String KEY_CHECK2 = "CHECK2";
    public static final String KEY_CHECK3 = "CHECK3";
    public static final String KEY_CHECK4 = "CHECK4";
    public static final String KEY_CHECK5 = "CHECK5";
    public static final String KEY_CHECK6 = "CHECK6";
    public static final String KEY_CHECK7 = "CHECK7";
    public static final String KEY_CHECK8 = "CHECK8";
    public static final String KEY_CHECK9 = "CHECK9";
    public static final String KEY_CHECK10 = "CHECK10";

    public static final String SHARED_PREF_CHECK1 = "false";
    public static final String SHARED_PREF_CHECK2 = "false";
    public static final String SHARED_PREF_CHECK3 = "false";
    public static final String SHARED_PREF_CHECK4 = "false";
    public static final String SHARED_PREF_CHECK5 = "false";
    public static final String SHARED_PREF_CHECK6 = "false";
    public static final String SHARED_PREF_CHECK7 = "false";
    public static final String SHARED_PREF_CHECK8 = "false";
    public static final String SHARED_PREF_CHECK9 = "false";
    public static final String SHARED_PREF_CHECK10 = "false";

    public static final String KEY_CHECKDOTADO1 = "CHECKDOTADO1";
    public static final String KEY_CHECKDOTADO2 = "CHECKDOTADO2";
    public static final String KEY_CHECKDOTADO3 = "CHECKDOTADO3";
    public static final String KEY_CHECKDOTADO4 = "CHECKDOTADO4";
    public static final String KEY_CHECKDOTADO5 = "CHECKDOTADO5";
    public static final String KEY_CHECKDOTADO6 = "CHECKDOTADO6";
    public static final String KEY_CHECKDOTADO7 = "CHECKDOTADO7";
    public static final String KEY_CHECKDOTADO8 = "CHECKDOTADO8";
    public static final String KEY_CHECKDOTADO9 = "CHECKDOTADO9";
    public static final String KEY_CHECKDOTADO10 = "CHECKDOTADO10";

    public static final String SHARED_PREF_CHECKDOTADO1 = "false";
    public static final String SHARED_PREF_CHECKDOTADO2 = "false";
    public static final String SHARED_PREF_CHECKDOTADO3 = "false";
    public static final String SHARED_PREF_CHECKDOTADO4 = "false";
    public static final String SHARED_PREF_CHECKDOTADO5 = "false";
    public static final String SHARED_PREF_CHECKDOTADO6 = "false";
    public static final String SHARED_PREF_CHECKDOTADO7 = "false";
    public static final String SHARED_PREF_CHECKDOTADO8 = "false";
    public static final String SHARED_PREF_CHECKDOTADO9 = "false";
    public static final String SHARED_PREF_CHECKDOTADO10 = "false";

    public static final String kEY_IP_PORT = "URL";
    public static final String SHARED_PREF_IP_PORT = "52.35.30.146:8080";
}
