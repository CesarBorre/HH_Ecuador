package com.neology.hhecuador.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neology.hhecuador.model.BaseResponse;
import com.neology.hhecuador.model.EndpointsUrls;
import com.neology.hhecuador.model.VehicleTag;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


public class RestClient {

    public static final int kVehicleNotFoundCode = 2001;
    public static final int kTagUHFAlreadyAssigned = 2020;
    public static final int kTagNFCAlreadyAssigned = 2021;

    private ServerContext context;
    private HttpHeaders requestHeaders;
    private RestTemplate client = new RestTemplate();

    public RestClient(ServerContext context) {
        this.context = context;
        requestHeaders = new HttpHeaders();
        HttpAuthentication authHeader = new HttpBasicAuthentication(context.getUsername(), context.getPassword());
        requestHeaders.setAuthorization(authHeader);
        client.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ClientHttpRequestFactory httpFactory = client.getRequestFactory();

        if (httpFactory != null) {
            if (httpFactory instanceof SimpleClientHttpRequestFactory) {
                ((SimpleClientHttpRequestFactory) httpFactory).setConnectTimeout(10 * 1000);
                ((SimpleClientHttpRequestFactory) httpFactory).setReadTimeout(30 * 1000);
            } else if (httpFactory instanceof HttpComponentsClientHttpRequestFactory) {
                ((HttpComponentsClientHttpRequestFactory) httpFactory).setConnectTimeout(10 * 1000);
                ((HttpComponentsClientHttpRequestFactory) httpFactory).setReadTimeout(30 * 1000);
            }
        }
    }

    public BaseResponse associateVehicleTag(String plate, String tidUhf, String tidNfc) {

        HttpStatus _status;
        BaseResponse result = null;
        VehicleTag vt = new VehicleTag(plate, tidUhf, tidNfc);

        try {

            String baseUrl = EndpointsUrls.associateVehicleTag();
            String assembledPath = assembleEndpoint(baseUrl);

            HttpEntity<?> requestEntity = new HttpEntity<Object>(vt, requestHeaders);
            ResponseEntity response = client.exchange(assembledPath, HttpMethod.POST, requestEntity, null);
            _status = response.getStatusCode();

            if (_status != null && _status.equals(HttpStatus.OK)) {
                result = new BaseResponse();
                result.setStatus(_status);
                return result;
            }

        } catch (HttpClientErrorException ex) {
            try {
                result = new ObjectMapper().readValue(ex.getResponseBodyAsString(), BaseResponse.class);
                result.setStatus(ex.getStatusCode());
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private String assembleEndpoint(String path) {

        return context
                .getServerUrl()
                .concat(path);
    }

}
