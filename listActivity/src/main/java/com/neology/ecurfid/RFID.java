package com.neology.ecurfid;

import android.annotation.SuppressLint;
import android.util.Log;
import com.atid.lib.dev.ATRfidManager;
import com.atid.lib.dev.ATRfidReader;
import com.atid.lib.dev.event.RfidReaderEventListener;
import com.atid.lib.dev.rfid.param.SimpleSelectionMask;
import com.atid.lib.dev.rfid.type.BankType;
import com.atid.lib.dev.rfid.type.MaskActionType;
import com.atid.lib.dev.rfid.type.ReaderState;
import com.atid.lib.dev.rfid.type.ResultCode;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.math.BigInteger;

public class RFID implements RfidReaderEventListener {
	// region Field Variables and Constants
	private final static String TAG = "ECU_UHF";
	
	private final static int STATUS_NOTINITIALIZED = -1;
	private final static int STATUS_IDLE = 0;

	// prePersoTag(
	private final static int PP_READ_EPC = 1;
	private final static int PP_READ_TID = 2;
	private final static int PP_WRITE_EPC = 3;

	// persoTag(
	private final static int P_READ_EPC = 6;
	private final static int P_READ_TID = 7;
	private final static int P_WRITE_CONSUMO = 8;
	private final static int P_WRITE_USER = 9;

	// leerTag(
	private final static int L_EPC = 11;
	private final static int L_TID = 12;
	private final static int L_USER = 13;
	private final static String L_FOLIO ="";

	// grabarConsumo(
	private final static int G_READ_EPC = 16;
	private final static int G_READ_TID = 17;
	private final static int G_READ_USER = 18;
	private final static int G_WRITE_USER = 19;

	// test_leeEPC(
	private final static int T_LEPC_READ_EPC = 21;

	// test_leeTID(
	private final static int T_LTID_READ_EPC = 26;
	private final static int T_LTID_READ_TID = 27;

	// Resultados
	private final static int STATUS_ECU_00_Success = 0;
	private final static int STATUS_ECU_01_NoTagInTheField = 1;
	private final static int STATUS_ECU_02_MoreThan1Tag = 2;
	private final static int STATUS_ECU_03_CannotReadBlock = 3;
	private final static int STATUS_ECU_04_ReaderConnectionFailed = 4;
	private final static int STATUS_ECU_05_PowerConfigurationError = 5;
	private final static int STATUS_ECU_06_TimeOutConfigurationError = 6;
	private final static int STATUS_ECU_07_RFIDNotIniatialized = 7;
	private final static int STATUS_ECU_08_InitializingRFID = 8;
	private final static int STATUS_ECU_09_RFIDDisconnected = 9;

	private final static int STATUS_ECU_10_WrongDataFormat = 10;
	private final static int STATUS_ECU_11_MemoryOverrun = 11;
	private final static int STATUS_ECU_12_BlockedMemory = 12;
	private final static int STATUS_ECU_13_OffsetOutOfRange = 13;
	private final static int STATUS_ECU_14_WrongTollingFormat = 14;
	private final static int STATUS_ECU_15_WriteError = 15;
	private final static int STATUS_ECU_16_LenghtOutOfRange = 16;
	private final static int STATUS_ECU_17_WrongECUFolioFormat = 17;
	private final static int STATUS_ECU_18_TollingWriteError = 18;
	private final static int STATUS_ECU_19_WrongPrefix = 19;
	private final static int STATUS_ECU_20_NotPersonalizedTag = 20;
	private final static int STATUS_ECU_21_TagNotReadPreviously = 21;
	private final static int STATUS_ECU_22_TimeoutOperation = 22;
	private final static int STATUS_ECU_23_TagNotCorrespondingToFolioGiven = 23;
	private final static int STATUS_ECU_24_InicializatingProccess = 24;
	private final static int STATUS_ECU_25_BusyReader = 25;

	private final static int STATUS_ECU_30_NoValidFolio = 30;
	private final static int STATUS_ECU_31_OutOfRangeFolio = 31;

	private final static int STATUS_ECU_40_ReadEPC = 40;
	private final static int STATUS_ECU_41_ErrorReadEPC = 41;
	private final static int STATUS_ECU_42_WriteEPC = 42;
	private final static int STATUS_ECU_43_ErrorWriteEPC = 43;

	private final static int STATUS_ECU_44_ReadTID = 44;
	private final static int STATUS_ECU_45_ErrorReadTID = 45;

	private final static int STATUS_ECU_48_ReadUSER = 48;
	private final static int STATUS_ECU_49_ErrorReadUSER = 49;
	private final static int STATUS_ECU_50_WriteUSER = 50;
	private final static int STATUS_ECU_51_ErrorWriteUSER = 51;

	private final static int STATUS_ECU_98_RFIDOcupado = 98;
	private final static int STATUS_ECU_99_RFIDDisponible = 99;

	private final static int PROCESS_ECU_1_PrePerso = 1;
	private final static int PROCESS_ECU_2_Perso = 2;
	private final static int PROCESS_ECU_3_LeerTag = 3;
	private final static int PROCESS_ECU_4_GrabarConsumo = 4;
	
	private static int CURRENT_FUNTION = 0;

	private final static int RESULT_ECU__1_Unknown = -1;
	// prePersoTag(
	private String _PP_EPC = "";
	private String _PP_TID = "";
	private String _PP_Folio = "";
	// persoTag(
	private String _P_EPC = "";
	private String _P_TID = "";
	private String _P_Folio = "";
	private String _P_Dato = "";
	private String _P_Dato2 = "";

	// leerTag(
	private String _L_EPC = "";
	private String _L_TID = "";
	private String _L_USER = "";
	private String _L_Folio = "";
	private int _L_USER_Count = 0;

	// grabarConsumo(
	private String _G_EPC = "";
	private String _G_TID = "";
	private String _G_USER = "";
	private String _G_Folio = "";
	private String _G_Consumo = "";
	private String _G_Dato = "";

	private static String _last_EPC = "";

	private Timer _timer_Timeout = null;
	private TimerTask _timerTask_Timeout = null;
	private Timer _timer_StartTask = null;
	private TimerTask _timerTask_StartTask = null;

	private int _timeoutCurrent = 0;
	private int _timeout = 2500;
	private int _power = 15;

	private ATRfidReader _reader = null;
	private boolean _readerConfigured = false;
	private boolean _readerOccupied = false;
	private boolean _restartingReader = false;
	private static boolean _initializedBefore = false;

	private boolean _writting = false;
	private int _retryNumber = 0;
	private boolean _waitingForStop = false;

	private int _status = STATUS_NOTINITIALIZED;

	private boolean _tagNotFound = false;
	private SimpleSelectionMask _mask;

	private String getErrorText(int iError) {
		switch (iError ) {
			case 0: return "*Operación exitosa";
			case 1: return "*No hay tag en el campo de lectura;";
			case 2: return "*Más de 1 tag en el campo de lectura";
			case 3: return "*Error de lectura: No se pudo leer la localidad";
			case 4: return "*Problemas de conexión con el lector. Verifique los parámetros de conexión";
			case 5: return "*Error en la configuración de RFID Power";
			case 6: return "*Error en la configuración del Timeout.";
			case 7: return "*Error RFID no Inicializado.";
			case 8: return "*Inicializando RFID";
			case 9: return "*RFID Desconectado";
			case 10: return "*Formato de datos erróneo";
			case 11: return "*Capacidad de memoria excedida";
			case 12: return "*Memoria bloqueada";
			case 13: return "*Offset fuera de rango [1-23]";
			case 14: return "Formato de telepeaje incorrecto";
			case 15: return "*Error de escritura: No se pudo escribir en la localidad";
			case 16: return "*Longitud fuera de rango [1-23]";
			case 17: return "*Formato de folio ECU incorrecto";
			case 18: return "*Error al escribir telepeaje";
			case 19: return "*Prefijo incorrecto";
			case 20: return "*El tag no se encuentra personalizado";
			case 21: return "*El tag no ha sido leído previamente";
			case 22: return "*Se cancela operación por Timeout";
			case 23: return "*El Tag leido no corresponde con el Folio ingresado";
			case 24: return "*Iniciando proceso";
			case 25: return "*Lector RF Ocupado";


			case 30: return "*Folio no valido";
			case 31: return "*Folio fuera de rango";

			case 40: return "*Inicia Lectura del EPC";
			case 41: return "*Error de lectura del EPC";
			case 42: return "*Inicia Escritura del EPC";
			case 43: return "*Error de Escritura del EPC";
			case 44: return "*Inicia Lectura del TID";
			case 45: return "*Error de lectura del TID";
			case 46: return "*Inicia Escritura del TID";
			case 47: return "*Error de Escritura del TID";
			case 48: return "*Inicia Lectura del USER";
			case 49: return "*Error de lectura del USER";
			case 50: return "*Inicia Escritura del USER";
			case 51: return "*Error de Escritura del USER";

			case 61: return "*Inicia Lectura del USER";
			case 62: return "*Error de lectura del USER";
			case 63: return "*Inicia Escritura del USER";
			case 64: return "*Error de Escritura del USER";

			case 98: return "*RFID Ocupado";
			case 99: return "*RFID Disponible";

			default: return "";
		}
	}

	private String getProcessText(int Process) {
		switch (Process) {
			case 0: return "*Inicializado";
			case 1:	return "*Pre-personalización UHF";
			case 2:	return "*Personalización UHF";
			case 3:	return "*Lectura de Tag UHF";
			case 4:	return "*Grabar Consumo UHF";
			default:return "";
		}
	}

	public int getTimeout() {
		return this._timeout;
	}

	public void setTimeout(int timeout) {
		if (timeout < 1300 || timeout > 10000) {
			sendRFIDEvent(STATUS_ECU_06_TimeOutConfigurationError, 0, null);
			this._timeout = 2500;
		} else {
			this._timeout = timeout;
		}
	}

	public int getPower() {
		return this._power;
	}

	public void setPower(int power) {
		if (power < 0 || power > 20) {
			sendRFIDEvent(STATUS_ECU_05_PowerConfigurationError, 0, null);
			this._power = 0;
		} else {
			this._power = power;
		}
	}

	protected Vector<RFID_Listener> _listeners;

	private boolean resultAvailable;

	private void add_RFID_Listener(RFID_Listener listener) {
		if (_listeners == null)
			_listeners = new Vector<RFID_Listener>();
		_listeners.addElement(listener);
	}

	private void remove_RFID_Listeners() {
		if (_listeners != null)
			_listeners.clear();
	}

	public boolean Initialize(/*Activity toAdd, */RFID_Listener rfListener) {
		KillTimer_ReadTimeout();
		if (_readerOccupied || _readerConfigured || _reader != null) {
			try {
				remove_RFID_Listeners();
				_reader.destroy();
				ATRfidManager.onDestroy();

				_readerConfigured = false;
				_readerOccupied = false;
				_reader = null;

				Thread.sleep(100);
			} catch (InterruptedException e) {

			} catch (Exception e) {

			} finally {
				_reader = null;
				_readerOccupied = false;
				_readerConfigured = false;
			}
		}

		if ((this._reader = ATRfidManager.getInstance()) == null) {
			_status = STATUS_NOTINITIALIZED;
			_readerConfigured = false;

			Log.e(TAG, "Error al obtener instancia del lector RFID. Reinicie el equipo.");
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed,0,null);
		} else {
			// Initializing listeners
			_listeners = new Vector<RFID_Listener>();
			add_RFID_Listener((RFID_Listener) rfListener);
			_reader.setEventListener(this);
			_reader.setPower(this._power);
			// Sending events
			if (!_initializedBefore) {
				sendLogEvent(STATUS_ECU_08_InitializingRFID,0, "Neology - AT911 RFID ECU v0.0.1 20151209");
				_initializedBefore = true;
				String firmwareVersion = _reader.getFirmewareVersion();
				sendLogEvent(STATUS_ECU_08_InitializingRFID,0, "FW RFID v" + firmwareVersion);
				if (!firmwareVersion.equalsIgnoreCase("R15033100")) {
					_status = STATUS_NOTINITIALIZED;
					_readerConfigured = false;
					sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed, 0, null);
					sendLogEvent(STATUS_ECU_04_ReaderConnectionFailed ,0, "Versión de FW RFID incorrecta. Por favor actualice el FW RFID.");
					return false;
				}
			}
			// Initializing mask
			_mask = new SimpleSelectionMask();
			_readerConfigured = true;
			_tagNotFound = false;
			_writting = false;
			_status = STATUS_IDLE;
			sendLogEvent(STATUS_ECU_99_RFIDDisponible,0,null);
		}
		return _readerConfigured;
	}

	public void Destroy() {
		sendRFIDEvent(STATUS_ECU_09_RFIDDisconnected, 0, null);
		KillTimer_ReadTimeout();
		remove_RFID_Listeners();
		setStatus(STATUS_NOTINITIALIZED);
		if (_reader != null) {
			try {
				_reader.disconnect();
			} catch (Exception ex) {
				Log.e(TAG, "Destroy: " + ex.getMessage());
			}
		}
		_reader = null;
		ATRfidManager.onDestroy();
	}

	private void setStatus(int status) {
		this._status = status;
		switch (_status) {
		case STATUS_NOTINITIALIZED:
			sendRFIDEvent(STATUS_ECU_07_RFIDNotIniatialized, 0, null);
			break;
		case STATUS_IDLE:
			_tagNotFound = false;
			_readerOccupied = false;
			break;

		// prePerso(
		case PP_READ_EPC:
			_timeoutCurrent = _timeout;
			break;
		case PP_READ_TID:
			_timeoutCurrent = _timeout;
			break;
		case PP_WRITE_EPC:
			_timeoutCurrent = _timeout;
			break;

		// persoTag(
		case P_READ_EPC:
			_timeoutCurrent = _timeout;
			break;
		case P_READ_TID:
			_timeoutCurrent = _timeout;
			break;
		case P_WRITE_CONSUMO:
			_timeoutCurrent = _timeout;
			break;
		case P_WRITE_USER:
			_timeoutCurrent = _timeout;
			break;

		// leer_Tag
		case L_EPC:
			_timeoutCurrent = _timeout;
			break;
		case L_TID:
			_timeoutCurrent = _timeout;
			break;
		case L_USER:
			_timeoutCurrent = _timeout;
			break;
		case G_READ_EPC:
			_timeoutCurrent = _timeout;
			break;
		case G_READ_TID:
			_timeoutCurrent = _timeout;
			break;
		case G_READ_USER:
			_timeoutCurrent = _timeout;
			break;
		case G_WRITE_USER:
			_timeoutCurrent = _timeout;
			break;

		// test_leeEPC(
		case T_LEPC_READ_EPC:
			_timeoutCurrent = _timeout;
			break;

		// test_leeTID
		case T_LTID_READ_EPC:
			_timeoutCurrent = _timeout;
			break;
		case T_LTID_READ_TID:
			_timeoutCurrent = _timeout;
			break;

		default:
			break;
		}

		// Reset previous timer if it exists
		KillTimer_ReadTimeout();
		if (!(_status == STATUS_IDLE || _status == STATUS_NOTINITIALIZED)) {
			// Start a new timer when timeout is reached
			_timerTask_Timeout = new TimerTask() {
				@Override
				public void run() {
					onOperationTimeout();
				}
			};
			_timer_Timeout = new Timer();
			_timer_Timeout.schedule(_timerTask_Timeout, _timeoutCurrent);
		} else if (_status == STATUS_IDLE) {
			// Put the reader in IDLE
			if (_reader != null) {
				_reader.stop();
			}

			if (!_restartingReader && !WaitForStop()) {
				_status = STATUS_NOTINITIALIZED;
				_readerConfigured = false;
			}

			if (_tagNotFound) {
				_tagNotFound = false;
				RFID._last_EPC = "";
				sendRFIDEvent(STATUS_ECU_01_NoTagInTheField,CURRENT_FUNTION, null);
			}

			if (_readerConfigured)
				sendLogEvent(STATUS_ECU_99_RFIDDisponible,0,null);
		}
	}

	private void KillTimer_ReadTimeout() {
		if (_timerTask_Timeout != null) {
			_timerTask_Timeout.cancel();
		}
		if (_timer_Timeout != null) {
			_timer_Timeout.purge();
			_timer_Timeout.cancel();
		}
		_timerTask_Timeout = null;
		_timer_Timeout = null;
	}

	private void onOperationTimeout() {
		Log.d(TAG, "Operation timeout");
		if (_writting) {
			Log.d(TAG, "W_Operation timeout " + _retryNumber);
			if (_retryNumber >= 3) {
				sendRFIDEvent(STATUS_ECU_22_TimeoutOperation,CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			} else {
				_retryNumber++;
				_waitingForStop = true;
				_reader.stop();
				WaitForStop();
				this._status = STATUS_IDLE;
				_waitingForStop = false;
				_timerTask_StartTask = new TimerTask() {
					@Override
					public void run() {
						escribeTag_retry();
					}
				};
				_timer_StartTask = new Timer();
				_timer_StartTask.schedule(_timerTask_StartTask, 100);
				Log.w(TAG, "R_Operation timeout-TimerStartTask created");
			}
		} else {
			if (this._status == L_EPC) {
				Log.d(TAG, "R_Operation timeout " + _retryNumber);
				if (_retryNumber >= 3) {
					sendRFIDEvent(STATUS_ECU_22_TimeoutOperation, CURRENT_FUNTION, null);
					setStatus(STATUS_IDLE);
				} else {
					_retryNumber++;
					_waitingForStop = true;
					if (_reader.getState().equals(ReaderState.Stop)) {
						onReaderStateChanged(ReaderState.Stop);
					} else {
						_reader.stop();
					}
					WaitForStop();
					this._status = STATUS_IDLE;
					_waitingForStop = false;

					_timerTask_StartTask = new TimerTask() {
						@Override
						public void run() {
							leeTag_retry();
						}
					};
					_timer_StartTask = new Timer();
					_timer_StartTask.schedule(_timerTask_StartTask, 100);
					Log.w(TAG, "R_Operation timeout-TimerStartTask created");
				}
			} else if (this._status == P_WRITE_USER )
			{
				sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}else if (this._status == P_READ_TID)
			{
				sendRFIDEvent(STATUS_ECU_45_ErrorReadTID, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}	else if (this._status == PP_WRITE_EPC)
			{
				sendRFIDEvent(STATUS_ECU_43_ErrorWriteEPC, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}
			else if (this._status == PP_READ_EPC)
			{
				sendRFIDEvent(STATUS_ECU_01_NoTagInTheField, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}
			else if (this._status == L_TID)
			{
				sendRFIDEvent(STATUS_ECU_49_ErrorReadUSER, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}
			else
			{
				sendRFIDEvent(STATUS_ECU_01_NoTagInTheField, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			}

		}
	}

	private boolean WaitForStop() {
		int i = 0;
		ReaderState rs;
		boolean result = true;
		try {
			Thread.sleep(150);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while (_reader != null && (rs = _reader.getState()) != ReaderState.Stop) {
			Log.w(TAG, "Waiting... " + i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
			if (i >= 3) {
				sendRFIDEvent(STATUS_ECU_22_TimeoutOperation,CURRENT_FUNTION,null);
				Log.w(TAG, "Reiniciando lector... " + rs);
				_restartingReader = true;
				result = false;
				break;
			}
		}
		_readerOccupied = !result;
		return result;
	}

	private void sendRFIDEvent(int iError, int iComando, String sText) {
		String data = ""+iError;
		if (iComando != 0)
			data = data + "|" + iComando;
		if (sText != null)
			data = data + "|" + sText;
		Log.i(TAG, data);
		if (_listeners != null) {
			for (RFID_Listener l : _listeners)
				l.Event_RFID(data);
		}
		sendLogEvent(iError, iComando, sText);
	}

	private void sendLogEvent(int iError, int iComando, String sText) {
		String data = iError +"|"+ iComando + " -> " +  getErrorText(iError);
		if (iComando != 0)
			data = data + "|" + getProcessText(iComando);
		if (sText != null)
			data = data + "|" + sText;
		Log.w(TAG, _status + " - LogEvent - " + data);
		if (_listeners != null) {
			for (RFID_Listener l : _listeners)
				l.Event_Log(data);
		}
	}

	/***** Public methods ***************************************************/
	public boolean prePersoTag(String folio) {
		CURRENT_FUNTION = PROCESS_ECU_1_PrePerso;
		sendLogEvent(STATUS_ECU_24_InicializatingProccess, CURRENT_FUNTION, null);
		if (!_readerConfigured) {
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed,CURRENT_FUNTION,null);
			setStatus(STATUS_IDLE);
			return false;
		}
		if (folio.equals("")) {
			sendRFIDEvent(STATUS_ECU_30_NoValidFolio, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		int iFolio;
		try {
			iFolio = Integer.parseInt(folio);
			if (iFolio < 1 || iFolio > 0xFFFFFF) {
				sendRFIDEvent(STATUS_ECU_31_OutOfRangeFolio ,CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
				return false;
			}
		} catch (Exception e) {
			sendRFIDEvent(STATUS_ECU_30_NoValidFolio, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		boolean result = false;
		int iTimeout = this._timeout;
		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado,CURRENT_FUNTION,null);
			if (iTimeout < 1300 || iTimeout > 10000) {
				sendRFIDEvent(STATUS_ECU_06_TimeOutConfigurationError, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			} else {
				_tagNotFound = true;
				_writting = false;
				_retryNumber = 0;
				_timeout = iTimeout;
				_PP_EPC = "";
				_PP_TID = "";
				_PP_Folio = folio;
				if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
					sendLogEvent(STATUS_ECU_40_ReadEPC, CURRENT_FUNTION, null);
					try{
						result = _reader.readEpc6cTag();
					}
					catch (Exception ex)
					{
						sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
						setStatus(STATUS_IDLE);
					}
				}
				if (result) {
					setStatus(PP_READ_EPC);
				} else {
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, null);
					setStatus(STATUS_IDLE);
				}
			}
		}
		return result;
	}

	@SuppressLint("DefaultLocale")
	public boolean persoTag(String folio, String Placa, String sVin, String TipoV, String TypeCombus, String Subsidio,
			String Galones, String FechaRec, String GalonesConsu) {
		CURRENT_FUNTION = PROCESS_ECU_2_Perso;
		sendLogEvent(STATUS_ECU_24_InicializatingProccess, CURRENT_FUNTION, null);
		if (!_readerConfigured) {
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed, PROCESS_ECU_2_Perso, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		if (folio.equals("")) {
			sendRFIDEvent(STATUS_ECU_30_NoValidFolio,CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		try {
			int iFolio = Integer.parseInt(folio);

			String[] SpGalones = Galones.split("\\.");
			int uGalones = Integer.parseInt(SpGalones[0]);

			String DecimGalones = SpGalones[1];
			if (DecimGalones.length() > 2)
				DecimGalones = DecimGalones.substring(0, 2);
			else if (DecimGalones.length() < 2)
				DecimGalones = "0" + DecimGalones;
			int dGalones = Integer.parseInt(DecimGalones);

			int iDia = Integer.parseInt(FechaRec.substring(0, 2));
			int iMes = Integer.parseInt(FechaRec.substring(2, 4));
			int iYear = Integer.parseInt(FechaRec.substring(4, 8));

			String[] SpGalonesConsu = GalonesConsu.split("\\.");
			int uGalonesCon = Integer.parseInt(SpGalonesConsu[0]);

			String DecimGalonesCon = SpGalonesConsu[1];
			if (DecimGalonesCon.length() > 2)
				DecimGalonesCon = DecimGalonesCon.substring(0, 2);
			else if (DecimGalonesCon.length() < 2)
				DecimGalonesCon = "0" + DecimGalonesCon;
			int dGalonesCon = Integer.parseInt(DecimGalonesCon);

			int uTypeCombus = Integer.parseInt(TypeCombus);
            int uTypeSubsidio = Integer.parseInt(Subsidio);
            int uTypeVehicle = Integer.parseInt(TipoV);

			if (iFolio < 1 || iFolio > 0xFFFFFF ||
                    Placa.length() > 7 ||
                    uTypeCombus < 0 || uTypeCombus > 255 ||
                    uTypeSubsidio < 0 || uTypeSubsidio > 255 ||
                    uTypeVehicle < 0 || uTypeVehicle > 255 ||
                    sVin.length() > 17 ||
                    uGalones < 0 || uGalones > 65535 ||
                    FechaRec.length() < 8 ||
                    iDia < 0 || iDia > 31 ||
                    iMes < 0 || iMes > 12 ||
                    iYear < 0 || iYear > 65535 ||
                    uGalonesCon < 0 || uGalonesCon > 65535
                    ) {
				
				sendRFIDEvent(STATUS_ECU_10_WrongDataFormat, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
				return false;
			}

			_P_Dato = String.format("%14s", StringToHex(Placa)).replace(' ','0')
					+ String.format("%2s", Integer.toHexString(uTypeCombus)).replace(' ', '0')
					+ String.format("%4s", Integer.toHexString(uGalones)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(dGalones)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(uTypeSubsidio)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(uTypeVehicle)).replace(' ', '0')
					+ String.format("%34s", StringToHex(sVin)).replace(' ', '0')
					+ String.format("%4s", "").replace(' ','0')
					+ String.format("%2s", Integer.toHexString(iDia)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(iMes)).replace(' ', '0')
					+ String.format("%4s", Integer.toHexString(iYear)).replace(' ', '0')
					+ String.format("%4s", Integer.toHexString(uGalonesCon)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(dGalonesCon)).replace(' ', '0') + "000000";
			
			_P_Dato = _P_Dato.toUpperCase();
			Log.i(TAG, "_P_Dato: " + _P_Dato);

		} catch (Exception e) {
			sendRFIDEvent(STATUS_ECU_10_WrongDataFormat, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}

		boolean result = false;
		int iTimeout = this._timeout;
		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado,CURRENT_FUNTION,null);
			if (iTimeout < 1300 || iTimeout > 10000) {
				sendRFIDEvent(STATUS_ECU_06_TimeOutConfigurationError, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			} else {
				_tagNotFound = true;
				_writting = false;
				_retryNumber = 0;
				_timeout = iTimeout;
				_P_EPC = "";
				_P_TID = "";
				_P_Folio = folio;

				if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
					sendLogEvent(STATUS_ECU_40_ReadEPC , CURRENT_FUNTION ,null);
					try{
						result = _reader.readEpc6cTag();
					}
					catch (Exception ex)
					{
						sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
						setStatus(STATUS_IDLE);
					}
				}
				if (result) {
					setStatus(P_READ_EPC);
				} else {
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, null);
					setStatus(STATUS_IDLE);
				}
			}
		}
		else
		{
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
		}
		return result;
	}

	@SuppressLint("DefaultLocale")
	public boolean grabarConsumo(String folio, String FechaRec, String Galones) {
		CURRENT_FUNTION = PROCESS_ECU_4_GrabarConsumo;
		sendLogEvent(STATUS_ECU_24_InicializatingProccess, CURRENT_FUNTION, null);
		if (!_readerConfigured) {
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}

		if (folio.equals("")) {
			sendRFIDEvent(STATUS_ECU_30_NoValidFolio, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		int iFolio;
		try {

			int iDia = Integer.parseInt(FechaRec.substring(0, 2));
			int iMes = Integer.parseInt(FechaRec.substring(2, 4));
			int iYear = Integer.parseInt(FechaRec.substring(4, 8));

			String[] SpGalones = Galones.split("\\.");
			int uGalones = Integer.parseInt(SpGalones[0]);

			String DecimGalones = SpGalones[1];
			if (DecimGalones.length() > 2)
				DecimGalones = DecimGalones.substring(0, 2);
			else if (DecimGalones.length() < 2)
				DecimGalones = "0" + DecimGalones;
			int dGalones = Integer.parseInt(DecimGalones);

			iFolio = Integer.parseInt(folio);
			if (iFolio < 1 || iFolio > 17777215 || uGalones > 65535 || FechaRec.length() < 8 || iDia > 31 || iMes > 12
					|| iYear > 65535) {
				sendRFIDEvent(STATUS_ECU_10_WrongDataFormat, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
				return false;
			}
			_G_Dato = String.format("%2s", Integer.toHexString(iDia)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(iMes)).replace(' ', '0')
					+ String.format("%4s", Integer.toHexString(iYear)).replace(' ', '0')
					+ String.format("%4s", Integer.toHexString(uGalones)).replace(' ', '0')
					+ String.format("%2s", Integer.toHexString(dGalones)).replace(' ', '0') + "00";
			_G_Dato = _G_Dato.toUpperCase();
		} catch (Exception e) {
			sendRFIDEvent(STATUS_ECU_10_WrongDataFormat, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		boolean result = false;
		int iTimeout = this._timeout;
		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado, CURRENT_FUNTION,null);
			if (iTimeout < 1300 || iTimeout > 10000) {
				sendRFIDEvent(STATUS_ECU_06_TimeOutConfigurationError, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			} else {
				_tagNotFound = true;
				_writting = false;
				_retryNumber = 0;
				_timeout = iTimeout;
				_G_EPC = "";
				_G_TID = "";
				_G_Folio = folio;

				if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
					sendLogEvent(STATUS_ECU_40_ReadEPC,CURRENT_FUNTION,null);
					try{
						result = _reader.readEpc6cTag();
					}
					catch (Exception ex)
					{
						sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
						setStatus(STATUS_IDLE);
					}

				}
				if (result) {
					setStatus(G_READ_EPC);
				} else {
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, null);
					setStatus(STATUS_IDLE);
				}
			}
		}
		else {
			sendRFIDEvent(STATUS_ECU_25_BusyReader, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
		}
		return result;
	}



	public boolean leerTag(String folio) {
		CURRENT_FUNTION = PROCESS_ECU_3_LeerTag;
		sendLogEvent(STATUS_ECU_24_InicializatingProccess, CURRENT_FUNTION, null);
		if (!_readerConfigured) {
			sendRFIDEvent(STATUS_ECU_04_ReaderConnectionFailed, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		int iFolio;
		try {
			iFolio = Integer.parseInt(folio);
			if (iFolio < 1 || iFolio > 17777215) {
				sendRFIDEvent(STATUS_ECU_31_OutOfRangeFolio, CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
				return false;
			}
		} catch (Exception e) {
			sendRFIDEvent(STATUS_ECU_10_WrongDataFormat , CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
			return false;
		}
		boolean result = false;
		int iTimeout = this._timeout;

		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado, CURRENT_FUNTION, null);

			if (iTimeout < 1300 || iTimeout > 10000) {
				sendRFIDEvent(STATUS_ECU_06_TimeOutConfigurationError, CURRENT_FUNTION, null);
				setStatus(STATUS_IDLE);
			} else {
				_tagNotFound = true;
				_writting = false;
				_retryNumber = 0;
				_timeout = iTimeout;
				_L_EPC = "";
				_L_TID = "";
				_L_USER = "";
				_L_Folio = folio;

				if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
					sendLogEvent(STATUS_ECU_40_ReadEPC , CURRENT_FUNTION ,null);
					try{
						result = _reader.readEpc6cTag();
					}
					catch (Exception ex)
					{
						sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
						setStatus(STATUS_IDLE);
					}
				}
				if (result) {
					setStatus(L_EPC);
				} else {
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC , CURRENT_FUNTION , null);
					setStatus(STATUS_IDLE);
				}
			}
		}
		else {
			sendRFIDEvent(STATUS_ECU_25_BusyReader, CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
		}
		return result;
	}



	private boolean leeTag_retry() {
		if (_timerTask_StartTask != null) {
			_timerTask_StartTask.cancel();
		}
		if (_timer_StartTask != null) {
			_timer_StartTask.purge();
			_timer_StartTask.cancel();
		}
		_timerTask_StartTask = null;
		_timer_StartTask = null;

		boolean result = false;

		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado, CURRENT_FUNTION, null);
			_tagNotFound = true;
			_writting = false;
			if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
				sendLogEvent(STATUS_ECU_40_ReadEPC , CURRENT_FUNTION ,null);
				try{
					result = _reader.readEpc6cTag();
				}
				catch (Exception ex)
				{
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
					setStatus(STATUS_IDLE);
				}
			}
			if (result) {
				setStatus(L_EPC);
			} else {
				sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}
		} else {
			sendLogEvent(STATUS_ECU_99_RFIDDisponible,CURRENT_FUNTION,null);
			setStatus(STATUS_IDLE);
		}
		return result;
	}

	private boolean escribeTag_retry() {
		if (_timerTask_StartTask != null) {
			_timerTask_StartTask.cancel();
		}
		if (_timer_StartTask != null) {
			_timer_StartTask.purge();
			_timer_StartTask.cancel();
		}
		_timerTask_StartTask = null;
		_timer_StartTask = null;

		boolean result = false;

		if (_readerOccupied == false) {
			_readerOccupied = true;
			sendLogEvent(STATUS_ECU_98_RFIDOcupado, CURRENT_FUNTION, null);
			_tagNotFound = true;
			_writting = false;

			if (_readerConfigured && _status == STATUS_IDLE && _reader != null) {
				sendLogEvent(STATUS_ECU_40_ReadEPC , CURRENT_FUNTION ,null);
				try{
					result = _reader.readEpc6cTag();
				}
				catch (Exception ex)
				{
					sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC, CURRENT_FUNTION, ex.getMessage() );
					setStatus(STATUS_IDLE);
				}
			}
			if (result) {
				setStatus(L_EPC);
			} else {
				sendRFIDEvent(STATUS_ECU_41_ErrorReadEPC , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}
		} else {

			sendLogEvent(STATUS_ECU_99_RFIDDisponible,CURRENT_FUNTION, null);
			setStatus(STATUS_IDLE);
		}
		return result;
	}





	// region M�todos privados
	////////////////////////////////////////////////
	// Reader Interface Methods //
	////////////////////////////////////////////////
	@Override
	public void onReaderDetectTag(String arg0) {
		String sPrefijo;
		String ReadFolio;
		Log.i(TAG, _status + "-onReaderDetectTag(" + arg0 + ")");
		switch (_status) {
		////////////////////////////////////////////////
		// prePersoTag
		////////////////////////////////////////////////
		case PP_READ_EPC:
			_PP_EPC = arg0;
			_mask = new SimpleSelectionMask();
			_mask.setBank(BankType.EPC);
			_mask.setOffset(32);
			_mask.setMask(_PP_EPC.substring(4, _PP_EPC.length()));
			_mask.setAction(MaskActionType.Match);
			Log.i(TAG, "PP_READ_EPC-" + _PP_EPC);
			break;

		case PP_READ_TID:
			Log.i(TAG, "PP_READ_EPC-" + _PP_EPC);
			_PP_TID = arg0;
			break;

		case PP_WRITE_EPC:
			break;

		// persoTag( states
		case P_READ_EPC:
			_P_EPC = arg0;
			sPrefijo = _P_EPC.substring(4,12);
			Log.i(TAG, _status + " Prefijo Leido = "+ sPrefijo);
			if (!sPrefijo.equals("4E454355") )
			{
				sendRFIDEvent(STATUS_ECU_20_NotPersonalizedTag, CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
				break;
			}
			ReadFolio = _P_EPC.substring(14,20);
			Log.i(TAG, _status + "Folio Leido = :"+ ReadFolio );
			String iFolio= Long.parseLong(ReadFolio, 16)+"";
			Log.i(TAG, _status + "Folio Leido = :"+ iFolio + "-" +   _P_Folio);
			if (!iFolio.equals(_P_Folio) )
			{
				sendRFIDEvent(STATUS_ECU_23_TagNotCorrespondingToFolioGiven, CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
				break;
			}
			_mask = new SimpleSelectionMask();
			_mask.setBank(BankType.EPC);
			_mask.setOffset(32);
			_mask.setMask(_P_EPC.substring(4, _P_EPC.length()));
			_mask.setAction(MaskActionType.Match);
			Log.i(TAG, "P_READ_EPC-" + _P_EPC);
			break;

		case P_READ_TID:
			_P_TID = arg0;
			Log.i(TAG, "P_READ_TID-" + _P_TID);
			break;

		case P_WRITE_USER:
			Log.i(TAG, "P_WRITE_USER- Regresa.....");
			break;

		case P_WRITE_CONSUMO:
			Log.i(TAG, "P_WRITE_CONSUMO- Regresa.....");
			break;

		// leerTag( states
		case L_EPC:
			_L_EPC = arg0;
			sPrefijo = _L_EPC.substring(4,12);
			Log.i(TAG, _status + " Prefijo Leido = "+ sPrefijo);
			if (!sPrefijo.equals("4E454355") )
			{
				sendRFIDEvent(STATUS_ECU_20_NotPersonalizedTag, CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
				break;
			}
			ReadFolio = _L_EPC.substring(14, 20);
			Log.i(TAG, _status + "Folio Leido = :"+ ReadFolio );
			iFolio= Long.parseLong(ReadFolio, 16)+"";
			Log.i(TAG, _status + "Compara Folios: "+ iFolio + "-" +   _L_Folio);
			if (!iFolio.equals(_L_Folio) )
			{
				sendRFIDEvent(STATUS_ECU_23_TagNotCorrespondingToFolioGiven, CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
				break;
			}
			_mask.setBank(BankType.EPC);
			_mask.setOffset(32);
			_mask.setMask(_L_EPC.substring(4, _L_EPC.length()));
			_mask.setAction(MaskActionType.Match);
			Log.i(TAG, "_L_EPC-" + _L_EPC);
			break;

		case L_TID:
			_L_TID = arg0;
			Log.i(TAG, "_L_TID-" + _L_TID);
			break;

		case L_USER:
			_L_USER = arg0;
			Log.i(TAG, "_L_USER-" + _L_USER);
			break;

		// grabarConsumo( states
		case G_READ_EPC:
			_G_EPC = arg0;
			_mask = new SimpleSelectionMask();
			_mask.setBank(BankType.EPC);
			_mask.setOffset(32);
			_mask.setMask(_G_EPC.substring(4, _G_EPC.length()));
			_mask.setAction(MaskActionType.Match);
			Log.i(TAG, "G_READ_EPC-" + _G_EPC);
			break;

		case G_READ_TID:
			_G_TID = arg0;
			Log.i(TAG, "G_READ_TID-" + _G_TID);
			break;

		case G_WRITE_USER:
			break;

		case STATUS_NOTINITIALIZED:
			Log.e(TAG, "onReaderDetectTag-STATUS_NOTINITIALIZED-Not supposed to be here");
			break;
		case STATUS_IDLE:
			Log.e(TAG, "onReaderDetectTag-STATUS_IDLE-Not supposed to be here");
			break;
		default:

			break;
		// endregion Not valid status
		}
	}
	int blockECU = 0;
	private boolean writeECUBlock() {
		try {
			//Log.i(TAG, "writeECUBlock: Metodo escritura");
			boolean result = false;
			resultAvailable = false;
			if (blockECU < 3) {
				String DataToWrite = _P_Dato.substring(0 + 28 * blockECU, 28 + 28 * blockECU);
				result = _reader.writeMemory(BankType.User, 0 + 7 * blockECU, DataToWrite, (SimpleSelectionMask) _mask);
				Thread.sleep(250);
				if (result) {
					setStatus(P_WRITE_USER);
					blockECU++;
				} else {
					setStatus(STATUS_IDLE);
				}
			} else {
				setStatus(STATUS_IDLE);
				result = false;
			}
			return result;
		} catch (Exception ex) {
			return false;
		}
	}

	////////////////////////////////////////////////
	// Reader State Changed //
	////////////////////////////////////////////////
	@Override
	@SuppressLint("DefaultLocale")
	public void onReaderStateChanged(ReaderState arg0) {
		Log.i(TAG, _status + "-onReaderStateChanged(" + arg0.toString() + ")");

		if (arg0 == ReaderState.Stop) {
			switch (_status) {

			//////////////////////////////////////////////
			// prePersoTag
			//////////////////////////////////////////////
			case PP_READ_EPC:
				sendLogEvent(STATUS_ECU_44_ReadTID ,PROCESS_ECU_1_PrePerso,null);
				if (_reader.readMemory6c(BankType.TID, 0, 6, (SimpleSelectionMask) _mask)) {
					setStatus(PP_READ_TID);
				} else {
					sendRFIDEvent(STATUS_ECU_45_ErrorReadTID, PROCESS_ECU_1_PrePerso, null);
					setStatus(STATUS_IDLE);
				}
				break;

			case PP_READ_TID:
				if (!_PP_TID.equals("")) {
					String sEPC = CalculateFolio(_PP_TID, _PP_Folio);
					sendLogEvent(STATUS_ECU_42_WriteEPC, PROCESS_ECU_1_PrePerso, null);
					if (_reader.writeMemory(BankType.EPC, 2, sEPC, (SimpleSelectionMask) _mask)) {
						setStatus(PP_WRITE_EPC);
					} else {
						sendRFIDEvent(STATUS_ECU_43_ErrorWriteEPC ,PROCESS_ECU_1_PrePerso, null);
						setStatus(STATUS_IDLE);
					}
				} else {
					sendRFIDEvent(STATUS_ECU_43_ErrorWriteEPC ,PROCESS_ECU_1_PrePerso, null);
					setStatus(STATUS_IDLE);
				}
				break;

			case PP_WRITE_EPC:
				if (_waitingForStop == false) {
					if (resultAvailable == false) {
						Log.d(TAG, "Waiting for result...");
					} else {
						sendRFIDEvent(STATUS_ECU_00_Success, PROCESS_ECU_1_PrePerso,  _PP_EPC.substring(4, _PP_EPC.length()) +"|" + _PP_TID);
						setStatus(STATUS_IDLE);
					}
				} else {
					sendRFIDEvent(STATUS_ECU_22_TimeoutOperation ,PROCESS_ECU_1_PrePerso, null);
					Log.w(TAG, "READ TAG stopped");
				}
				break;

			//////////////////////////////////////////////
			// persoTag
			//////////////////////////////////////////////
			case P_READ_EPC:
				sendLogEvent(STATUS_ECU_44_ReadTID , PROCESS_ECU_2_Perso, null);
				if (_reader.readMemory6c(BankType.TID, 0, 6, (SimpleSelectionMask) _mask)) {
					setStatus(P_READ_TID);
				} else {
					sendRFIDEvent(STATUS_ECU_45_ErrorReadTID, PROCESS_ECU_2_Perso, null);
					setStatus(STATUS_IDLE);
				}
				break;

			case P_READ_TID:
				try {
					Log.i(TAG, "P_READ_TID");
					if (!_P_TID.equals("")) {
						String sEPC = CalculateFolio(_P_TID, _P_Folio);
						sendLogEvent(STATUS_ECU_50_WriteUSER, PROCESS_ECU_2_Perso, null);
						if (_P_EPC.substring(4, _P_EPC.length()).equals(sEPC)) {
							writeECUBlock();
						} else {
							sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_2_Perso, null);
							setStatus(STATUS_IDLE);
						}
					} else {
						sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_2_Perso, null);
						setStatus(STATUS_IDLE);
					}
				} catch (Exception e) {
					Log.e(TAG, "_P_Dato: " + e.getMessage());
					sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_2_Perso, null);
					setStatus(STATUS_IDLE);
				}
				break;
			case P_WRITE_USER:
				try {
					Log.d(TAG, "P_WRITE_USER: onReaderStateChanged");
					if (_waitingForStop == false) {
						if (resultAvailable == false) {
							Log.d(TAG, "Waiting for result...");
						} else {
							if (blockECU < 3) {
								writeECUBlock();
							} else if (blockECU >= 3) {
								 sendRFIDEvent(STATUS_ECU_00_Success,  PROCESS_ECU_2_Perso,  _P_EPC.substring(4, _P_EPC.length()) +"|" + _P_TID);
								setStatus(STATUS_IDLE);
							}
						}
					} else {
						sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_2_Perso, null);
						setStatus(STATUS_IDLE);
						Log.w(TAG, "Reader stopped");
					}
				} catch (Exception e) {
					sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_2_Perso, null);
					setStatus(STATUS_IDLE);
					Log.i(TAG, "P_WRITE_USER: " + e.getMessage());
				}
				break;

			case P_WRITE_CONSUMO:
				Log.d(TAG, "P_WRITE_USER: onReaderStateChanged");
				if (_waitingForStop == false) {
					if (resultAvailable == false) {
						Log.d(TAG, "Waiting for result...");
					} else {
						sendRFIDEvent(STATUS_ECU_00_Success ,CURRENT_FUNTION , _P_EPC.substring(4, _P_EPC.length()) +"|" + _P_TID);
						setStatus(STATUS_IDLE);
					}
				} else {
					Log.w(TAG, "Reader stopped");
				}
				break;

			//////////////////////////////////////////////
			// leerTag
			//////////////////////////////////////////////
			case L_EPC:
				sendLogEvent(STATUS_ECU_44_ReadTID, PROCESS_ECU_3_LeerTag, null);
				if (_reader.readMemory6c(BankType.TID, 0, 6, (SimpleSelectionMask) _mask)) {
					setStatus(L_TID);
				} else {

					sendRFIDEvent(STATUS_ECU_45_ErrorReadTID, PROCESS_ECU_3_LeerTag, null);
					setStatus(STATUS_IDLE);
				}
				break;

			case L_TID:
				if (!_L_TID.equals("")) {
					String sEPC = CalculateFolio(_L_TID, _L_Folio);
					sendLogEvent(STATUS_ECU_48_ReadUSER, PROCESS_ECU_3_LeerTag, null);
					if (_L_EPC.substring(4, _L_EPC.length()).equals(sEPC)) {
						if (_reader.readMemory6c(BankType.User, 0, 20, (SimpleSelectionMask) _mask)) {
							setStatus(L_USER);
						} else {
							sendRFIDEvent(STATUS_ECU_51_ErrorWriteUSER, PROCESS_ECU_3_LeerTag, null);
							setStatus(STATUS_IDLE);
						}
					}
				} else {
					setStatus(STATUS_IDLE);
				}
				break;

			case L_USER:
				Log.i(TAG, "L_USER:" + _L_USER);
				String sTag = ReParse(_L_USER , _L_Folio);
				if (!sTag.equals("")) {
					sendRFIDEvent(STATUS_ECU_00_Success ,PROCESS_ECU_3_LeerTag ,   _L_EPC.substring(4, _L_EPC.length())+"|" + _L_TID + "|" + sTag + "|");
				} else {
					sendRFIDEvent(STATUS_ECU_14_WrongTollingFormat, PROCESS_ECU_3_LeerTag , null);
				}
				setStatus(STATUS_IDLE);
				break;

			// grabarConsumo( states
			case G_READ_EPC:
				sendLogEvent(STATUS_ECU_44_ReadTID, PROCESS_ECU_4_GrabarConsumo, null);
				if (_reader.readMemory6c(BankType.TID, 0, 6, (SimpleSelectionMask) _mask)) {
					setStatus(G_READ_TID);
				} else {
					sendRFIDEvent(STATUS_ECU_45_ErrorReadTID, PROCESS_ECU_4_GrabarConsumo, null);
					setStatus(STATUS_IDLE);
				}
				break;

			case G_READ_TID:
				Log.i(TAG, "G_READ_TID");
				if (!_G_TID.equals("")) {
					String sEPC = CalculateFolio(_G_TID, _G_Folio);
					Log.i(TAG, "EPC calculado: " + sEPC + " EPC Normal: " + _G_EPC.substring(4, _G_EPC.length()));
					if (_G_EPC.substring(4, _G_EPC.length()).equals(sEPC)) {
						Log.i(TAG, "Escribe");
						if (_reader.writeMemory(BankType.User, 16, _G_Dato, (SimpleSelectionMask) _mask)) {
							setStatus(G_WRITE_USER);
						} else {
							setStatus(STATUS_IDLE);
						}
					} else {
						setStatus(STATUS_IDLE);
					}
				} else {
					setStatus(STATUS_IDLE);
				}
				break;

			case G_WRITE_USER:
				if (_waitingForStop == false) {
					if (resultAvailable == false) {
						Log.d(TAG, "Waiting for result...");
					} else {
						sendRFIDEvent(STATUS_ECU_00_Success, CURRENT_FUNTION , null);
						setStatus(STATUS_IDLE);
					}
				} else {
					Log.w(TAG, "Reader stopped");
				}
				break;
			// region Not valid status
			case STATUS_IDLE:
				break;
			case STATUS_NOTINITIALIZED:
				Log.e(TAG, "onReaderStateChanged-STATUS_NOTINITIALIZED-Not supposed to be here.");
				setStatus(STATUS_IDLE);
				break;
			default:
				break;
			}
		}
	}

	@Override
	@SuppressLint("DefaultLocale")
	public void onReaderResult(ResultCode arg0) {
		switch (_status) {
		case PP_WRITE_EPC:
			resultAvailable = true;
			if (arg0.equals(ResultCode.Success)) {
				_tagNotFound = false;
			} else if (arg0.equals(ResultCode.MemoryOverrun)) {
				sendRFIDEvent(STATUS_ECU_12_BlockedMemory , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}

			break;

		case P_WRITE_USER:
			Log.i(TAG, "P_WRITE_USER....");
			resultAvailable = true;
			if (arg0.equals(ResultCode.Success)) {
				_tagNotFound = false;
			} else if (arg0.equals(ResultCode.MemoryOverrun)) {
				sendRFIDEvent(STATUS_ECU_12_BlockedMemory , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}
			break;

		case P_WRITE_CONSUMO:
			resultAvailable = true;
			if (arg0.equals(ResultCode.Success)) {
				 sendRFIDEvent(STATUS_ECU_00_Success, CURRENT_FUNTION , null);
				_tagNotFound = false;
			} else if (arg0.equals(ResultCode.MemoryOverrun)) {
				sendRFIDEvent(STATUS_ECU_12_BlockedMemory , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}
			break;

		case G_WRITE_USER:
			resultAvailable = true;
			if (arg0.equals(ResultCode.Success)) {
				_tagNotFound = false;
			} else if (arg0.equals(ResultCode.MemoryOverrun)) {
				sendRFIDEvent(STATUS_ECU_12_BlockedMemory , CURRENT_FUNTION , null);
				setStatus(STATUS_IDLE);
			}
			break;

		case STATUS_NOTINITIALIZED:
			Log.e(TAG, "onReaderResult-STATUS_NOTINITIALIZED-Not supposed to be here");
			break;
		case STATUS_IDLE:
			Log.e(TAG, "onReaderResult-STATUS_IDLE-Not supposed to be here");
			break;
		default:
			break;
		}
	}

	@Override
	public void onReaderRssiChanged(int arg0) {
		Log.i(TAG, "onReaderRssiChanged(" + arg0 + ")");
	}

	private String CalculateFolio(String TID, String Folio) {
		int foo = Integer.parseInt(Folio);
		String hFolio = Integer.toHexString(foo);
		hFolio = String.format("%6s", hFolio).replace(' ', '0');
		String FF = "4E45435500" + hFolio + TID.substring(TID.length() - 8, TID.length());
		return FF.toUpperCase();
	}

	@SuppressLint("DefaultLocale")
	public String convertHexToString(String hex) {
		String hexRes ="";
		int ival = 0;
		StringBuilder sb = new StringBuilder(hex.length() / 2);
		for (int i = 0; i < hex.length(); i += 2) {
			hexRes = "" + hex.charAt(i) + hex.charAt(i + 1);
			if (!hexRes.equals("00")) {
				ival = Integer.parseInt(hexRes, 16);
				sb.append((char) ival);
			}
		}
		return sb.toString();
	}

	public String StringToHex(String str) {
		char[] chars = str.toCharArray();
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}
		return hex.toString();
	}

	private String ReParse(String str, String Folio) {
		try {
			String Placa = convertHexToString(str.substring(0, 14));
			Long dTComb = (Long.parseLong(str.substring(14, 16), 16));
            String sTComb = dTComb.toString();
			Long dGalMesEnt = Long.parseLong(str.substring(16, 20), 16);

			Long dGalMesDec = (Long.parseLong(str.substring(20, 22), 16));
			String sGalMesDec = dGalMesDec.toString();
			if (sGalMesDec.length() < 2)
				sGalMesDec = "0" + sGalMesDec;

            Long dTSubs = (Long.parseLong(str.substring(22, 24), 16));
			String sSubs = dTSubs.toString();

            Long dTVeh = (Long.parseLong(str.substring(24, 26), 16));
			String sTVeh = dTVeh.toString();
			String sVin = convertHexToString(str.substring(26, 60));

			Long dDia = Long.parseLong(str.substring(64, 66), 16);
			String sDia = dDia.toString();
			if (sDia.length() < 2)
				sDia = "0" + sDia;

			Long dMes = Long.parseLong(str.substring(66, 68), 16);
			String sMes = dMes.toString();
			if (sMes.length() < 2)
				sMes = "0" + sMes;

			long dYear = Long.parseLong(str.substring(68, 72), 16);
			Long dGalConsEnt = Long.parseLong(str.substring(72, 76), 16);
			Long dGalConsDec = Long.parseLong(str.substring(76, 78), 16);
			String sGalConsDec = dGalConsDec.toString();
			if (sGalConsDec.length() < 2)
				sGalConsDec = "0" + sGalConsDec;

			String Result = Folio + "|" + Placa + "|" + sVin + "|" + sTVeh + "|" + sTComb + "|" + sSubs + "|" + dGalMesEnt + "."
					+ sGalMesDec + "|" + sDia + sMes + dYear + "|" + dGalConsEnt + "." + sGalConsDec;
			Log.e(TAG, " Result ReParse: " + Result);
			return Result;
		} catch (Exception e) {
			Log.e(TAG, " ReParse " + e.toString());
			return str;
		}
	}
}