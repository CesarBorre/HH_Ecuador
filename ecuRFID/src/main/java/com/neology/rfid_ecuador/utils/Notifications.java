package com.neology.rfid_ecuador.utils;

import android.content.Context;
import android.view.View;

import com.neology.rfid_ecuador.ui.customdialog.CustomDialog;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialogOnClickListener;

public class Notifications {

    public static CustomDialog showOkCustomDialog(String title, String description, String okTitle, Context context) {

        CustomDialog dialog = new CustomDialog(CustomDialog.CustomDialogType.DEFAULT, context);
        if(okTitle != null && okTitle.length() > 0) {
            dialog.setCustomButton(okTitle, new CustomDialogOnClickListener(dialog) {
                @Override
                public void onClick(View v) {
                    this.customDialog.dismissCustomDialog();
                }
            });
        }
        dialog.showCustomDialog(title, description);

        return dialog;
    }

    public static CustomDialog showLoaderCustomDialog(String title, String description, Context context) {

        CustomDialog dialog = new CustomDialog(CustomDialog.CustomDialogType.LOADER, context);
        dialog.showCustomDialog(title, description);

        return dialog;
    }

}
