package com.neology.rfid_ecuador.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class Utilities {

    public static final String userNameKey="kUserName";
    public static final String passwordKey="kPassword";
    public static final String urlKey="kUrlInstance";

    public static void persistString(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences("App_Preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    public static String getPersistedString(Context context, String key, String _default) {
        SharedPreferences preferences = context.getSharedPreferences("App_Preferences", Context.MODE_PRIVATE);
        return preferences.getString(key, _default);
    }

}
