package com.neology.rfid_ecuador.utils;

import android.os.Build;
import android.widget.TextView;

/**
 * Created by neology on 12/17/15.
 */
public class Compatibily {

    @SuppressWarnings("deprecation")
    public static void setTextAppearance(TextView view, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setTextAppearance(id);
        } else {
            view.setTextAppearance(view.getContext(), id);
        }
    }
}
