package com.neology.rfid_ecuador.utils;

/**
 * Created by emercado on 12/10/15.
 */
public enum EnumResultCodeApi {
        _0(0,"RESULT_ECU_00_Success"),
        _1(1,"RESULT_ECU_01_NoTagInTheField"),
        _2(2,"RESULT_ECU_02_MoreThan1Tag"),
        _3(3,"RESULT_ECU_03_CannotReadBlock"),
        _4(4,"RESULT_ECU_04_ReaderConnectionFailed"),
        _5(5,"RESULT_ECU_05_ReaderConfigurationError"),
        _10(10,"RESULT_ECU_10_WrongDataFormat"),
        _11(11,"RESULT_ECU_11_MemoryOverrun"),
        _12(12,"RESULT_ECU_12_BlockedMemory"),
        _13(13,"RESULT_ECU_13_OffsetOutOfRange"),
        _14(14,"RESULT_ECU_14_WrongTollingFormat"),
        _15(15,"RESULT_ECU_15_WriteError"),
        _16(16,"RESULT_ECU_16_LenghtOutOfRange"),
        _17(17,"RESULT_ECU_17_WrongECUFolioFormat"),
        _18(18,"RESULT_ECU_18_TollingWriteError"),
        _19(19,"RESULT_ECU_19_WrongPrefix"),
        _20(20,"RESULT_ECU_20_NotPersonalizedTag"),
        _21(21,"RESULT_ECU_21_TagNotReadPreviously"),
        _61(61,"RESULT_ECU_61_Exception_prePersoTag"),
        _62(62,"RESULT_ECU_62_Exception_persoTag"),
        _63(63,"RESULT_ECU_63_Exception_leerTag"),
        _64(64,"RESULT_ECU_64_Exception_grabarConsumo");


        private String strCodeName;
        private int intCode;
        EnumResultCodeApi( int value,String toString) {
                strCodeName = toString;
                intCode = value;
        }

        public String getStrCodeName() {
                return strCodeName;
        }

        public void setStrCodeName(String strCodeName) {
                this.strCodeName = strCodeName;
        }

        public int getIntCode() {
                return intCode;
        }

        public void setIntCode(int intCode) {
                this.intCode = intCode;
        }

        @Override
        public String toString() {
                return "EnumResultCodeApi{" +
                        "strCodeName='" + strCodeName + '\'' +
                        ", intCode=" + intCode +
                        '}';
        }
}
