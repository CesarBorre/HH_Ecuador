package com.neology.rfid_ecuador;

import android.app.ActionBar;
import android.app.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.neology.rfid_ecuador.model.VehicleByPlate;
import com.neology.rfid_ecuador.model.VehicleTag;
import com.neology.rfid_ecuador.session.Session;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialog;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialogOnKeyListener;
import com.neology.rfid_ecuador.utils.Notifications;
import com.neology.rfid_ecuador.utils.Utilities;

import java.util.Timer;
import java.util.TimerTask;

interface LogInCallback {

    enum LogInErrorType {FIELD_VALIDATION, NOT_VALIDATED_USER, UNDEFINED}

    void success();

    void failed(LogInErrorType errorType);
}


public class LoginActivity extends Activity {

    private EditText mPassword;
    private AutoCompleteTextView mUrl;
    private AutoCompleteTextView mUser;

    private long mLastClickTime = 0;
    private CustomDialog loader = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.activity_login_action_bar);
            actionBar.setIcon(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));
            actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.activity_login_action_bar));
        }

        mUser = (AutoCompleteTextView) findViewById(R.id.usuario);
        mPassword = (EditText) findViewById(R.id.contrasena);
        mUrl = (AutoCompleteTextView) findViewById(R.id.url);

        mUser.setText(Utilities.getPersistedString(LoginActivity.this, Utilities.userNameKey, mUser.getText().toString()));
        mPassword.setText(Utilities.getPersistedString(LoginActivity.this, Utilities.passwordKey, mPassword.getText().toString()));
        mUrl.setText(Utilities.getPersistedString(LoginActivity.this, Utilities.urlKey, mUrl.getText().toString()));

        (findViewById(R.id.email_sign_in_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                loader = new CustomDialog(CustomDialog.CustomDialogType.LOADER, LoginActivity.this);
                loader.setOnKeyListener(new CustomDialogOnKeyListener(loader) {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        return true;
                    }
                });
                loader.showCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kLogIn_LoggingInMessage));

                logIn(new LogInCallback() {
                    @Override
                    public void success() {
                        loader.dismissCustomDialog();

                        Intent myIntent = new Intent(LoginActivity.this, SearchActivity.class);
                        LoginActivity.this.startActivity(myIntent);
                    }

                    @Override
                    public void failed(LogInErrorType errorType) {
                        loader.dismissCustomDialog();
                        if (errorType != LogInCallback.LogInErrorType.FIELD_VALIDATION) {
                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kLogIn_UnableToAuthenticateUserMessage),
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    LoginActivity.this);
                        }
                    }
                });
            }
        });
    }

    private void logIn(final LogInCallback result) {

        mUrl.setError(null);
        mUser.setError(null);
        mPassword.setError(null);

        final String url = mUrl.getText().toString();
        final String user = mUser.getText().toString();
        final String password = mPassword.getText().toString();

        TextView view = null;

        if (TextUtils.isEmpty(user))
            view = mUser;
        else if (TextUtils.isEmpty(password))
            view = mPassword;
        else if (TextUtils.isEmpty(url)) {
            view = mUrl;
        }

        if (view != null) {
            view.setError(getString(R.string.kLogIn_RequiredField));
            view.requestFocus();

            if (result != null)
                result.failed(LogInCallback.LogInErrorType.FIELD_VALIDATION);

            return;
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Utilities.persistString(LoginActivity.this, Utilities.userNameKey, user);
                                Utilities.persistString(LoginActivity.this, Utilities.passwordKey, password);
                                Utilities.persistString(LoginActivity.this, Utilities.urlKey, url);

                                if (result != null)
                                    result.success();
                            }
                        });
                    }
                };
                thread.start();
            }
        }, 3000);
    }
}