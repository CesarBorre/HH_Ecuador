package com.neology.rfid_ecuador.session;

import com.neology.rfid_ecuador.model.VehicleByPlate;
import com.neology.rfid_ecuador.model.VehicleTag;

public class Session {

    private static VehicleByPlate vehicleToRecord=new VehicleByPlate();
    private static VehicleTag tagToRecord=new VehicleTag();

    /*
    private static String userName="";
    private static String password="";
    private static String url="";

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        Session.userName = userName;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Session.password = password;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Session.url = url;
    }*/

    public static VehicleByPlate getVehicleToRecord() {
        return vehicleToRecord;
    }

    public static void setVehicleToRecord(VehicleByPlate vehicleToRecord) {
        Session.vehicleToRecord = vehicleToRecord;
    }

    public static VehicleTag getTagToRecord() {
        return tagToRecord;
    }

    public static void setTagToRecord(VehicleTag tagToRecord) {
        Session.tagToRecord = tagToRecord;
    }

    @Override
    public String toString() {
        return "Session{VehicleToRecord: "+vehicleToRecord.toString()+", TagToRecord: "+tagToRecord.toString()+"}";
    }
}
