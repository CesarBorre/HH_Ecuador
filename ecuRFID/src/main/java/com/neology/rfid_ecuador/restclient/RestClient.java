package com.neology.rfid_ecuador.restclient;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neology.rfid_ecuador.model.Response;
import com.neology.rfid_ecuador.model.VehicleByPlate;
import com.neology.rfid_ecuador.model.VehicleTag;
import com.neology.rfid_ecuador.utils.ServerContext;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


public class RestClient {

    public static final int kVehicleNotFoundCode = 2001;
    public static final int kTagUHFAlreadyAssigned = 2020;
    public static final int kTagNFCAlreadyAssigned = 2021;

    private ServerContext context;
    private HttpHeaders requestHeaders;
    private RestTemplate client = new RestTemplate();

    public RestClient(ServerContext context) {
        this.context = context;
        requestHeaders = new HttpHeaders();
        HttpAuthentication authHeader = new HttpBasicAuthentication(context.getUsername(), context.getPassword());
        requestHeaders.setAuthorization(authHeader);
        client.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ClientHttpRequestFactory httpFactory = client.getRequestFactory();

        if (httpFactory != null) {
            if (httpFactory instanceof SimpleClientHttpRequestFactory) {
                ((SimpleClientHttpRequestFactory) httpFactory).setConnectTimeout(10 * 1000);
                ((SimpleClientHttpRequestFactory) httpFactory).setReadTimeout(30 * 1000);
            } else if (httpFactory instanceof HttpComponentsClientHttpRequestFactory) {
                ((HttpComponentsClientHttpRequestFactory) httpFactory).setConnectTimeout(10 * 1000);
                ((HttpComponentsClientHttpRequestFactory) httpFactory).setReadTimeout(30 * 1000);
            }
        }
    }

    public VehicleByPlate getVehicleByPlate(String plate) {
        VehicleByPlate result = null;
        try {
            String baseUrl = EndpointsUrls.getVehicleByPlate(plate);
            String assembledPath = assembleEndpoint(baseUrl);
            Log.i(this.getClass().toString(), assembledPath);

            HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
            ResponseEntity<VehicleByPlate> response = client.exchange(assembledPath, HttpMethod.GET, requestEntity, VehicleByPlate.class);

            Log.i(this.getClass().toString(), response.getStatusCode().toString());
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                result = response.getBody();
            } else {
                Log.i(this.getClass().toString(), "Response code: " + response.getStatusCode());
            }
            Log.i(this.getClass().toString(), "Get result:" + result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public ArrayList<VehicleByPlate> getVehicles() {
        ArrayList<VehicleByPlate> result = null;
        try {

            String baseUrl = EndpointsUrls.getVehiclesWithoutTag();
            String assembledPath = assembleEndpoint(baseUrl);
            Log.i(this.getClass().toString(), assembledPath);

            HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
            ResponseEntity<VehicleByPlate[]> response = client.exchange(assembledPath, HttpMethod.GET, requestEntity, VehicleByPlate[].class);

            Log.i(this.getClass().toString(), response.getStatusCode().toString());
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                result = new ArrayList<>(Arrays.asList(response.getBody()));
            } else {
                Log.i(this.getClass().toString(), "Response code: " + response.getStatusCode());
            }
            Log.i(this.getClass().toString(), "Get result:" + result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public Response associateVehicleTag(String plate, String tidUhf, String tidNfc) {

        HttpStatus _status;
        Response result = null;
        VehicleTag vt = new VehicleTag(plate, tidUhf, tidNfc);

        try {

            String baseUrl = EndpointsUrls.associateVehicleTag();
            String assembledPath = assembleEndpoint(baseUrl);

            HttpEntity<?> requestEntity = new HttpEntity<Object>(vt, requestHeaders);
            ResponseEntity response = client.exchange(assembledPath, HttpMethod.POST, requestEntity, null);
            _status = response.getStatusCode();

            if (_status != null && _status.equals(HttpStatus.OK)) {
                result = new Response();
                result.setStatus(_status);
                return result;
            }

        } catch (HttpClientErrorException ex) {
            try {
                result = new ObjectMapper().readValue(ex.getResponseBodyAsString(), Response.class);
                result.setStatus(ex.getStatusCode());
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private String assembleEndpoint(String path) {

        return context
                .getServerUrl()
                .concat(path);
    }

}
