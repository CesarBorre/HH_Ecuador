package com.neology.rfid_ecuador.restclient;

/**
 * Created by ecme on 09/12/2015.
 */
public class EndpointsUrls {
    private static final String SEARCH_CAR_BY_PLATE="/rest/vehicles/vehicle/plate/";
    private static final String SEARCH_CAR_BY_TAG="/rest/vehicles/vehicle/tag/";
    private static final String VEHICLE_TAG_ASSOCIATION="/rest/vehicles/vehicle/relateTag";
    private static final String VEHICLES_WITHOUT_TAG="/rest/vehicles/vehiclesWithoutTag";

    public static final String getVehicleByPlate(String plate) { return SEARCH_CAR_BY_PLATE.concat(plate); }
    public static final String associateVehicleTag(){
        return VEHICLE_TAG_ASSOCIATION;
    }
    public static final String getVehiclesWithoutTag(){
        return VEHICLES_WITHOUT_TAG;
    }

}
