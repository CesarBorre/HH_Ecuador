package com.neology.rfid_ecuador;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atid.lib.media.SoundPlayer;

import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.rfid_ecuador.model.VehicleTag;
import com.neology.rfid_ecuador.model.VehicleByPlate;
import com.neology.rfid_ecuador.restclient.RestClient;
import com.neology.rfid_ecuador.utils.Notifications;
import com.neology.rfid_ecuador.utils.ServerContext;
import com.neology.rfid_ecuador.ui.customdialog.*;
import com.neology.rfid_ecuador.utils.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

interface RecordFolioNumberCallback {

    enum RecordFolioErrorType {NO_FOLIO_FOUND, RECORDING_PROCESS}

    void success(String folio);

    void failed(RecordFolioErrorType errorType);
}

interface VehicleByPlateNumberCallback {

    enum VehicleByPlateErrorType {NO_PLATE_FOUND, NO_VEHICLE_FOUND, SEARCHING_PROCESS}

    void success(VehicleByPlate vehicleByPlate);

    void failed(VehicleByPlateErrorType errorType);
}

interface VehiclesCallback {

    enum VehiclesErrorType {NO_VEHICLES_FOUND, OBTAINING_PROCESS}

    void success(ArrayList<VehicleByPlate> vehicles);

    void failed(VehiclesErrorType errorType);
}

public class SearchActivity extends Activity implements RF_Listener {

    private RF_Control RF;
    private SoundPlayer sound_error;
    private SoundPlayer sound_success;

    private long mLastClickTime = 0;
    private CustomDialog loader = null;
    private CustomDialog uhfWriterDialog = null;

    private VehicleTag tagToRecord = null;
    private VehicleByPlate vehicleToRecord = null;
    private ArrayList<VehicleByPlate> vehicles = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_search_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_pre_customization:

                uhfWriterDialog = new CustomDialog(CustomDialog.CustomDialogType.INPUT, SearchActivity.this);

                uhfWriterDialog.setOnKeyListener(new CustomDialogOnKeyListener(uhfWriterDialog) {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent event) {

                        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
                            recordFolioNumber(this.customDialog.getInputText(), null);
                            return true;

                        } else
                            return false;
                    }
                });

                String cancel = getResources().getString(R.string.kDefAlertCancelOptTitle);
                uhfWriterDialog.setCustomButton(cancel, new CustomDialogOnClickListener(uhfWriterDialog) {
                    @Override
                    public void onClick(View v) {
                        this.customDialog.dismissCustomDialog();
                    }
                });

                String record = getResources().getString(R.string.kSearchToRecord_AlertRecordOptTitle);
                uhfWriterDialog.setCustomButton(record, new CustomDialogOnClickListener(uhfWriterDialog) {
                    @Override
                    public void onClick(View v) {
                        recordFolioNumber(this.customDialog.getInputText(), null);
                    }
                });

                String title = getResources().getString(R.string.kSearchToRecord_RecordFolioInstructionsMessage);
                uhfWriterDialog.showCustomDialog(title, null);

                return true;

            case R.id.action_read_tag:
                Intent intent = new Intent(SearchActivity.this, ReadingActivity.class);
                SearchActivity.this.startActivity(intent);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private View.OnClickListener didTapSearchListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            CustomDialog dialog = new CustomDialog(CustomDialog.CustomDialogType.INPUT, SearchActivity.this);

            String cancel = getResources().getString(R.string.kDefAlertCancelOptTitle);
            dialog.setCustomButton(cancel, new CustomDialogOnClickListener(dialog) {
                @Override
                public void onClick(View v) {
                    this.customDialog.dismissCustomDialog();
                }
            });

            String search = getResources().getString(R.string.kSearchToRecord_AlertSearchOptTitle);
            dialog.setCustomButton(search, new CustomDialogOnClickListener(dialog) {
                @Override
                public void onClick(View v) {

                    String plateNumber = this.customDialog.getInputText();

                    if (plateNumber.length() > 0) {

                        this.customDialog.dismissCustomDialog();

                        String title = getResources().getString(R.string.kDefAlertTitle);
                        String description = getResources().getString(R.string.kSearchToRecord_SearchingVehicleMessage);
                        loader = Notifications.showLoaderCustomDialog(title, description, SearchActivity.this);

                        getVehicleByPlateNumber(plateNumber, new VehicleByPlateNumberCallback() {

                            @Override
                            public void success(VehicleByPlate vehicleByPlate) {

                                loader.dismissCustomDialog();
                                ArrayList<VehicleByPlate> vehicles = new ArrayList<>(Collections.singletonList(vehicleByPlate));
                                reloadData(vehicles, View.VISIBLE);
                            }

                            @Override
                            public void failed(VehicleByPlateErrorType errorType) {

                                String message = null;
                                loader.dismissCustomDialog();

                                switch (errorType) {
                                    case NO_PLATE_FOUND:
                                        message = getResources().getString(R.string.kSearchToRecord_NoPlateNumberFoundMessage);
                                        break;
                                    case NO_VEHICLE_FOUND:
                                        message = getResources().getString(R.string.kSearchToRecord_NoVehicleFoundMessage);
                                        break;
                                    case SEARCHING_PROCESS:
                                        message = getResources().getString(R.string.kSearchToRecord_FaliedOperationMessage);
                                        break;
                                    default:
                                        break;
                                }

                                if (message.length() > 0) {
                                    Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                            message, getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                            SearchActivity.this);
                                }

                            }
                        });
                    } else {
                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchToRecord_RequiredPlateNumberMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                }
            });
            String title = getResources().getString(R.string.kSearchToRecord_SearchPlateAlertTitle);
            dialog.showCustomDialog(title, null);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_search);

            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                actionBar.setCustomView(R.layout.activity_search_action_bar);
                actionBar.setIcon(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));
                actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.activity_search_action_bar));
            }

            ImageButton ib = (ImageButton) findViewById(R.id.searchBtn);
            ib.setOnClickListener(didTapSearchListener);

            sound_success = new SoundPlayer(this, R.raw.ok);
            sound_error = new SoundPlayer(this, R.raw.error);

            String title = getResources().getString(R.string.kDefAlertTitle);
            String description = getResources().getString(R.string.kSearchActivity_ObtainingVehiclesMessage);
            loader = Notifications.showLoaderCustomDialog(title, description, SearchActivity.this);

        } catch (Exception ex) {
            Log.d(this.getComponentName().getClassName(), "onCreate -Error :" + ex.getMessage());
        }
    }

    private void getVehicles() {

        getPendingVehicles(new VehiclesCallback() {
            @Override
            public void success(ArrayList<VehicleByPlate> vehicles) {

                loader.dismissCustomDialog();
                SearchActivity.this.vehicles = vehicles;
                reloadData(vehicles, View.GONE);
            }

            @Override
            public void failed(VehiclesErrorType errorType) {

                String message = null;
                loader.dismissCustomDialog();

                switch (errorType) {
                    case NO_VEHICLES_FOUND:
                        message = getResources().getString(R.string.kSearchActivity_NoVehiclesFoundMessage);
                        break;
                    case OBTAINING_PROCESS:
                        message = getResources().getString(R.string.kSearchToRecord_FaliedOperationMessage);
                        break;
                    default:
                        break;
                }

                if (message.length() > 0) {
                    Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                            message, getResources().getString(R.string.kDefAlertAcceptOptTitle),
                            SearchActivity.this);
                }

            }
        });
    }

    private void getPendingVehicles(final VehiclesCallback result) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    String user = Utilities.getPersistedString(SearchActivity.this, Utilities.userNameKey, "");
                    String password = Utilities.getPersistedString(SearchActivity.this, Utilities.passwordKey, "");
                    String url = Utilities.getPersistedString(SearchActivity.this, Utilities.urlKey, "");

                    ServerContext context = new ServerContext(user, password, url);
                    final ArrayList<VehicleByPlate> vehicles = (new RestClient(context)).getVehicles();

                    if (vehicles != null) {
                        if (result != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    result.success(vehicles);
                                }
                            });
                        }
                    } else {
                        if (result != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    result.failed(VehiclesCallback.VehiclesErrorType.NO_VEHICLES_FOUND);
                                }
                            });
                        }
                    }
                } catch (Exception ex) {
                    if (result != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                result.failed(VehiclesCallback.VehiclesErrorType.OBTAINING_PROCESS);
                            }
                        });
                    }
                }
            }
        };
        thread.start();
    }

    private void getVehicleByPlateNumber(final String plateNumber, final VehicleByPlateNumberCallback result) {

        if (plateNumber != null && plateNumber.length() > 0) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        String user = Utilities.getPersistedString(SearchActivity.this, Utilities.userNameKey, "");
                        String password = Utilities.getPersistedString(SearchActivity.this, Utilities.passwordKey, "");
                        String url = Utilities.getPersistedString(SearchActivity.this, Utilities.urlKey, "");

                        ServerContext context = new ServerContext(user, password, url);
                        final VehicleByPlate vehicleByPlate = (new RestClient(context)).getVehicleByPlate(plateNumber);

                        if (vehicleByPlate != null) {
                            if (result != null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        result.success(vehicleByPlate);
                                    }
                                });
                            }
                        } else {
                            if (result != null) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        result.failed(VehicleByPlateNumberCallback.VehicleByPlateErrorType.NO_VEHICLE_FOUND);
                                    }
                                });
                            }
                        }
                    } catch (Exception ex) {
                        if (result != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    result.failed(VehicleByPlateNumberCallback.VehicleByPlateErrorType.SEARCHING_PROCESS);
                                }
                            });
                        }
                    }
                }
            };
            thread.start();

        } else {
            if (result != null)
                result.failed(VehicleByPlateNumberCallback.VehicleByPlateErrorType.NO_PLATE_FOUND);
        }
    }

    public void didSelectSearchErase(View searchEraseLayout) {

        reloadData(vehicles, View.GONE);
    }

    private void reloadData(ArrayList<VehicleByPlate> vehicles, int searchVisibility) {

        TableLayout tableLayout = (TableLayout) findViewById(R.id.table_layout);
        tableLayout.removeAllViews();
        //Header
        (findViewById(R.id.header_layout)).setVisibility((vehicles != null && vehicles.size() > 0) ? View.VISIBLE : View.GONE);
        //Rows
        createRows(tableLayout, vehicles);
        //Search erase
        (findViewById(R.id.search_erase_layout)).setVisibility(searchVisibility);
    }

    private void createRows(TableLayout table, ArrayList<VehicleByPlate> vehicles) {

        int index = 0;
        if (vehicles != null) {
            for (final VehicleByPlate vehicle : vehicles) {

                View row = SearchActivity.this.getLayoutInflater().inflate(R.layout.activity_search_row, table, false);
                row.setBackgroundResource((index % 2 == 0) ? R.drawable.activity_search_row_1 : R.drawable.activity_search_row_2);

                ((TextView) row.findViewById(R.id.vin_field)).setText(vehicle.getVin());
                ((TextView) row.findViewById(R.id.plateNumber_field)).setText(vehicle.getLicensePlate());

                row.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // Preventing multiple clicks, using threshold of 1 second
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        uhfWriterDialog = new CustomDialog(CustomDialog.CustomDialogType.INPUT, SearchActivity.this);

                        uhfWriterDialog.setOnKeyListener(new CustomDialogOnKeyListener(uhfWriterDialog) {
                                                             @Override
                                                             public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                                                                 if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                                                                         || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
                                                                     //this.customDialog.dismissCustomDialog();
                                                                     SearchActivity.this.vehicleToRecord = vehicle;
                                                                     performVehicleTagAssociation(vehicle, this.customDialog.getInputText());

                                                                     return true;
                                                                 } else
                                                                     return false;
                                                             }
                                                         }

                        );

                        String cancel = getResources().getString(R.string.kDefAlertCancelOptTitle);
                        uhfWriterDialog.setCustomButton(cancel, new

                                        CustomDialogOnClickListener(uhfWriterDialog) {
                                            @Override
                                            public void onClick(View v) {
                                                this.customDialog.dismissCustomDialog();
                                            }
                                        }

                        );

                        String accept = getResources().getString(R.string.kSearchToRecord_AlertRecordOptTitle);
                        uhfWriterDialog.setCustomButton(accept, new

                                        CustomDialogOnClickListener(uhfWriterDialog) {
                                            @Override
                                            public void onClick(View v) {
                                                //this.customDialog.dismissCustomDialog();
                                                SearchActivity.this.vehicleToRecord = vehicle;
                                                performVehicleTagAssociation(vehicle, this.customDialog.getInputText());
                                            }
                                        }

                        );
                        String title = getResources().getString(R.string.kSearchToRecord_PlateNumberAndFolioAssociationMessage);
                        uhfWriterDialog.showCustomDialog((title + ": " + vehicle.getLicensePlate()), null);
                    }
                });
                index++;
                table.addView(row, new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.MATCH_PARENT, 1.0f));
            }
        }
    }

    private void recordFolioNumber(String folio, final RecordFolioNumberCallback result) {

        if (folio != null && folio.length() > 0) {

            try {
                final Integer integer = Integer.valueOf(folio);

                String title = getResources().getString(R.string.kDefAlertTitle);
                String description = getResources().getString(R.string.kSearchToRecord_RecordingMessage);
                loader = Notifications.showLoaderCustomDialog(title, description, SearchActivity.this);

                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        if (RF.prePersoTagUHF(integer.toString())) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (result != null)
                                        result.success(integer.toString());
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (result != null)
                                        result.failed(RecordFolioNumberCallback.RecordFolioErrorType.RECORDING_PROCESS);
                                }
                            });
                        }
                    }
                };
                thread.start();

            } catch (Exception e) {

                if (loader != null)
                    loader.dismissCustomDialog();

                Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kSearchActivity_OnlyNumbersAllowedMessage),
                        getResources().getString(R.string.kDefAlertAcceptOptTitle),
                        SearchActivity.this);
            }

        } else {
            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                    getResources().getString(R.string.kSearchToRecord_RequiredFolioMessage),
                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                    SearchActivity.this);

            if (result != null)
                result.failed(RecordFolioNumberCallback.RecordFolioErrorType.NO_FOLIO_FOUND);
        }
    }

    private void parseReceivedData(String data) {

        try {
            String[] resultStr = data.split("\\|");
            Log.i(this.getComponentName().getClassName(), "--------- RESULT CODE FROM API: " + Arrays.toString(resultStr));
            Log.i(this.getComponentName().getClassName(), "--------- RESULT CODE FROM API: " + data);

            switch (resultStr[0]) {
                case "0":
                    sound_success.play();

                    switch (resultStr[1]) {
                        case "1":

                            if (uhfWriterDialog != null)
                                uhfWriterDialog.dismissCustomDialog();

                            if (loader != null)
                                loader.dismissCustomDialog();

                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kSearchToRecord_SuccessfulFolioRecordedMessage),
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    SearchActivity.this);
                            break;
                        case "2":
                            tagToRecord = ((tagToRecord != null) ? tagToRecord : new VehicleTag());
                            tagToRecord.setTidUhf(resultStr[3]); // Set UHF ID
                            tagToRecord.setTidNfc(resultStr[4]); // Set UHF ID
                            tagToRecord.setPlate(vehicleToRecord.getLicensePlate());

                            CustomDialog dialog = new CustomDialog(CustomDialog.CustomDialogType.DEFAULT, SearchActivity.this);

                            dialog.setOnKeyListener(new CustomDialogOnKeyListener(dialog) {
                                @Override
                                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                    return true;
                                }
                            });

                            dialog.setCustomButton(getResources().getString(R.string.kDefAlertAcceptOptTitle), new CustomDialogOnClickListener(dialog) {
                                @Override
                                public void onClick(View v) {
                                    this.customDialog.dismissCustomDialog();
                                    Intent intent = new Intent(SearchActivity.this, ValidationActivity.class);
                                    intent.putExtra("SearchToRecord_vehicleToRecord", vehicleToRecord);
                                    intent.putExtra("SearchToRecord_tagToRecord", tagToRecord);
                                    SearchActivity.this.startActivity(intent);
                                }
                            });

                            if (loader != null)
                                loader.dismissCustomDialog();

                            dialog.showCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kSearchToRecord_PreValidationMessage));
                            break;
                        case "7":

                            if (uhfWriterDialog != null)
                                uhfWriterDialog.dismissCustomDialog();

                            if (loader != null)
                                loader.dismissCustomDialog();

                            loader = new CustomDialog(CustomDialog.CustomDialogType.LOADER, SearchActivity.this);
                            loader.setOnKeyListener(new CustomDialogOnKeyListener(loader) {
                                @Override
                                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                    return true;
                                }
                            });
                            loader.showCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kSearchToRecord_PlaceNFCTagMessage));
                            break;
                        default:
                            sound_success.play();

                            if (loader != null)
                                loader.dismissCustomDialog();

                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kSearchToRecord_SuccessfulOperationMessage),
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    SearchActivity.this);
                            break;
                    }
                    break;
                case "-1":
                    sound_success.play();
                    Toast.makeText(getApplicationContext(), R.string.kSearchToRecord_SuccessfulRFMessage,
                            Toast.LENGTH_LONG).show();
                    break;
                case "1":
                    if (resultStr[1].equals("1") || resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchToRecord_NoTagMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "2":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_MoreThanOneTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "4":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_UHFReaderConnectionErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "10":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_WrongFormatDataErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "20":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_NotCustomizedTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "23":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_FolioNumberErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                case "31":
                    if (resultStr[1].equals("2")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kSearchActivity_FolioNumberOutOfRangeErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                SearchActivity.this);
                    }
                    break;
                default:
                    sound_error.play();

                    if (loader != null)
                        loader.dismissCustomDialog();

                    Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                            getResources().getString(R.string.kSearchToRecord_FaliedOperationMessage),
                            getResources().getString(R.string.kDefAlertAcceptOptTitle),
                            SearchActivity.this);
                    break;
            }
        } catch (Exception e) {
            Log.e(this.getComponentName().getClassName(), e.getMessage());
            sound_error.play();
        }
    }

    private boolean performVehicleTagAssociation(VehicleByPlate vehicle, String folio) {

        boolean result = false;

        if (folio != null && folio.length() > 0) {

            try {
                Integer integer = Integer.valueOf(folio);
                if (result = recordVehicleDataWithFolio(vehicle, integer.toString())) {
                    tagToRecord = ((tagToRecord != null) ? tagToRecord : new VehicleTag());
                    tagToRecord.setFolio(folio);
                }

            } catch (Exception e) {

                if (loader != null)
                    loader.dismissCustomDialog();

                Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kSearchActivity_OnlyNumbersAllowedMessage),
                        getResources().getString(R.string.kDefAlertAcceptOptTitle),
                        SearchActivity.this);
            }

        } else {
            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                    getResources().getString(R.string.kSearchToRecord_RequiredFolioMessage),
                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                    SearchActivity.this);
        }

        return result;
    }

    private boolean recordVehicleDataWithFolio(VehicleByPlate vehicle, String folio) {

        boolean result = false;

        String title = getResources().getString(R.string.kDefAlertTitle);
        String description = getResources().getString(R.string.kSearchToRecord_RecordingMessage);
        loader = Notifications.showLoaderCustomDialog(title, description, SearchActivity.this);

        try {
            result = RF.persoTagUHF(folio, vehicle.getLicensePlate(), vehicle.getVin(),
                    (vehicle.getVehicleTypeId() + ""), (vehicle.getFuelTypeId() + ""),
                    (vehicle.getSubsidization() ? "1" : "0" + ""), (vehicle.getLitersPerMonth() + ""),
                    DateFormat.format("ddMMyyyy", new Date()).toString(), "0.00");
        } catch (Exception ex) {
            loader.dismissCustomDialog();
        }

        return result;
    }

    //region RF_Listener methods
    @Override
    public void Event_RF_Control(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parseReceivedData(data);
            }
        });
    }

    @Override
    public void Event_RF_Log(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parseReceivedData(data);
            }
        });
    }
    //endregion

    //region Activity methods
    @Override
    protected void onPause() {
        super.onPause();
        if (RF != null)
            RF.onPauseProcessNFC();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (RF != null)
            RF = null;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (RF != null) {
            RF.Destroy();
            RF = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RF == null) {
            RF = new RF_Control();
            RF.Initialize(this);
        }
        RF.onResumeProcessNFC();
        getVehicles();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (RF != null)
            RF.onNewIntentProcessNFC(intent);
    }
    //endregion

}