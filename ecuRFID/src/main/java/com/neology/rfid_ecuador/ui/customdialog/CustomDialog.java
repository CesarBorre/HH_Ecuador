package com.neology.rfid_ecuador.ui.customdialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.neology.rfid_ecuador.R;

public class CustomDialog {

    public enum CustomDialogType {INPUT, DEFAULT, LOADER}

    private Dialog dialog = null;
    private TableRow bodyLayout = null;
    private LinearLayout actionLayout = null;
    private CustomDialogType dialogType = CustomDialogType.DEFAULT;

    EditText input_EditText = null;
    TextView title_TextView = null;
    TextView description_TextView = null;
    ProgressBar loader_ProgressBar = null;

    public List<Button> buttons = null;

    public CustomDialog(CustomDialogType dialogType, Context context) {

        this.dialogType = dialogType;

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCanceledOnTouchOutside(false);

        input_EditText = (EditText) dialog.findViewById(R.id.input);
        input_EditText.setVisibility((dialogType != CustomDialogType.INPUT) ? View.GONE : View.VISIBLE);

        loader_ProgressBar = (ProgressBar) dialog.findViewById(R.id.loader);
        loader_ProgressBar.setVisibility((dialogType != CustomDialogType.LOADER) ? View.GONE : View.VISIBLE);
        loader_ProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.custom_dialog_progressbar), PorterDuff.Mode.SRC_IN);

        bodyLayout = (TableRow) dialog.findViewById(R.id.body_layout);
        bodyLayout.setGravity((dialogType != CustomDialogType.LOADER) ? Gravity.CENTER : Gravity.CENTER_VERTICAL | Gravity.LEFT);
        actionLayout = (LinearLayout) dialog.findViewById(R.id.action_layout);
    }

    public void setCustomButton(String text, CustomDialogOnClickListener listener) {

        Button button = new Button(dialog.getContext());
        button.setTextColor(new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                new int[]{
                        ContextCompat.getColor(dialog.getContext(), R.color.custom_dialog_button_pressed),
                        ContextCompat.getColor(dialog.getContext(), R.color.custom_dialog_button)}));
        button.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
        button.setPadding(8, 8, 8, 8);
        button.setText(text);
        button.setTextSize(18);
        button.setMinHeight(70);
        button.setMinimumHeight(70);
        button.setOnClickListener(listener);
        button.setBackgroundResource(R.drawable.custom_dialog_button);

        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        layoutParams.setMargins(10, 10, 10, 10);
        button.setLayoutParams(layoutParams);

        if (buttons != null) {
            buttons.add(button);
        } else {
            buttons = new ArrayList<Button>();
            buttons.add(button);
        }

        actionLayout.addView(button);
    }

    public void showCustomDialog(String title, String description) {

        updateTitle(title);
        updateDescription(description);
        updateActionLayout();

        dialog.show();
    }

    public void dismissCustomDialog() {
        if (dialog != null)
            dialog.dismiss();
    }

    public void updateInput(String text) {

        input_EditText = (EditText) dialog.findViewById(R.id.input);

        if (text != null && text.length() > 0)
            input_EditText.setText(text);
    }

    public void updateTitle(String title) {

        title_TextView = (TextView) dialog.findViewById(R.id.title);
        title_TextView.setGravity((dialogType != CustomDialogType.LOADER) ? Gravity.CENTER : Gravity.CENTER_VERTICAL | Gravity.LEFT);

        if (title != null && title.length() > 0)
            title_TextView.setText(title);
    }

    public void updateDescription(String description) {

        description_TextView = (TextView) dialog.findViewById(R.id.description);
        description_TextView.setGravity((dialogType != CustomDialogType.LOADER) ? Gravity.CENTER : Gravity.CENTER_VERTICAL | Gravity.LEFT);

        if (description != null && description.length() > 0) {

            description_TextView.setVisibility(View.VISIBLE);
            description_TextView.setText(description);

            if (!(buttons != null && buttons.size() > 0)) {

                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) description_TextView.getLayoutParams();
                layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin * 2, layoutParams.bottomMargin * 2);
                description_TextView.setLayoutParams(layoutParams);
            }

        } else {

            description_TextView.setVisibility(View.GONE);

            if (!(buttons != null && buttons.size() > 0)) {

                title_TextView = (TextView) dialog.findViewById(R.id.title);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) title_TextView.getLayoutParams();
                layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin * 2, layoutParams.bottomMargin * 2);
                title_TextView.setLayoutParams(layoutParams);
            }
        }
    }

    public void updateActionLayout() {
        actionLayout = (LinearLayout) dialog.findViewById(R.id.action_layout);
        actionLayout.setVisibility((buttons != null && buttons.size() > 0) ? View.VISIBLE : View.GONE);
    }

    public String getInputText() {
        return input_EditText.getText().toString();
    }

    public String getTitle() {
        return title_TextView.getText().toString();
    }

    public String getDescription() {
        return description_TextView.getText().toString();
    }

    public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
        dialog.setOnKeyListener(onKeyListener);
    }

   /* public void setOnKeyUpListener(DialogInterface.On onKeyUpListener) {

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "Key-" + keyCode + " - KeyEvent-" + event);
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
            RF.leerTagUHF(tagToRecord.getFolio());
        }
        return super.onKeyDown(keyCode, event);
    }
*/
}