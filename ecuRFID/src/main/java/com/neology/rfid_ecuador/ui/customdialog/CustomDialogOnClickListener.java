package com.neology.rfid_ecuador.ui.customdialog;

import android.view.View;

public class CustomDialogOnClickListener implements View.OnClickListener {

    public CustomDialog customDialog = null;

    public CustomDialogOnClickListener(CustomDialog customDialog) {
        this.customDialog = customDialog;
    }

    @Override
    public void onClick(View v) {
        //read your lovely variable
    }
}
