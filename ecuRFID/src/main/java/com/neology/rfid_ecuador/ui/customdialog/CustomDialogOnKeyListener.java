package com.neology.rfid_ecuador.ui.customdialog;

import android.view.KeyEvent;
import android.content.DialogInterface;

public class CustomDialogOnKeyListener implements DialogInterface.OnKeyListener {

    public CustomDialog customDialog = null;

    public CustomDialogOnKeyListener(CustomDialog customDialog) {
        this.customDialog = customDialog;
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        //read your lovely variable
        return true;
    }
}

