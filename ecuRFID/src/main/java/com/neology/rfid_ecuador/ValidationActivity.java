package com.neology.rfid_ecuador;

import android.app.ActionBar;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Activity;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.atid.lib.media.SoundPlayer;
import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.rfid_ecuador.model.Response;
import com.neology.rfid_ecuador.model.ResultStrConverter;
import com.neology.rfid_ecuador.model.TagUhf;
import com.neology.rfid_ecuador.model.VehicleByPlate;
import com.neology.rfid_ecuador.model.VehicleTag;
import com.neology.rfid_ecuador.restclient.RestClient;
import com.neology.rfid_ecuador.session.Session;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialog;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialogOnClickListener;
import com.neology.rfid_ecuador.utils.DateUtils;
import com.neology.rfid_ecuador.utils.Notifications;
import com.neology.rfid_ecuador.utils.ServerContext;
import com.neology.rfid_ecuador.utils.Utilities;

import org.springframework.http.HttpStatus;

import java.util.Arrays;

interface ValidationCallback {

    void success();

    void failed(Response response);
}

public class ValidationActivity extends Activity implements RF_Listener {

    public enum SubsidizedCatalog {

        NO(0),
        YES(1);

        private final int value;

        SubsidizedCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public enum FuelTypeCatalog {

        DIESEL(1),
        GASOLINE(2);

        private final int value;

        FuelTypeCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public enum VehicleTypeCatalog {

        AUTOMOBILE(1),
        PICKUP(2),
        TRUCK(3),
        TRAILER_TRUCK(4),
        DOUBLE_TRAILER(5);

        private final int value;

        VehicleTypeCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    private RF_Control RF;
    private SoundPlayer sound_error;
    private SoundPlayer sound_success;

    private long mLastClickTime = 0;
    private CustomDialog loader = null;
    private VehicleTag tagToRecord = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_validation);

            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                actionBar.setCustomView(R.layout.activity_validation_action_bar);
                actionBar.setIcon(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));
                actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.activity_validation_action_bar));
            }

            sound_success = new SoundPlayer(this, R.raw.ok);
            sound_error = new SoundPlayer(this, R.raw.error);

            tagToRecord = (VehicleTag) getIntent().getSerializableExtra("SearchToRecord_tagToRecord");
            VehicleByPlate vehicleToRecord = (VehicleByPlate) getIntent().getSerializableExtra("SearchToRecord_vehicleToRecord");

            if (vehicleToRecord != null && tagToRecord != null) {
                Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kValidationActivity_PointToUHFTagMessage),
                        getResources().getString(R.string.kDefAlertAcceptOptTitle),
                        ValidationActivity.this);
            } else {
                Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kValidationActivity_FailedPreValidationMessage),
                        getResources().getString(R.string.kDefAlertAcceptOptTitle),
                        ValidationActivity.this);
            }

            RF = new RF_Control();
            RF.Initialize(this);

        } catch (Exception ex) {
            Log.d(this.getComponentName().getClassName(), "onCreate -Error :" + ex.getMessage());
        }
    }

    private void writeToTagTV(String data) {
        try {
            final String[] resultStr = data.split("\\|");
            Log.d(this.getComponentName().getClassName(), " -->RESULT CODE FROM API: " + Arrays.toString(resultStr));
            Log.d(this.getComponentName().getClassName(), " -->RESULT CODE FROM API: " + data);
            switch (resultStr[0]) {
                case "0":
                    if (loader != null)
                        loader.dismissCustomDialog();

                    if (resultStr[1].equals("3")) {
                        sound_success.play();
                        fillInputs(ResultStrConverter.convertStrToTagUhf(data));
                    }
                    break;
                case "1":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_NoTagMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "2":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_MoreThanOneTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "10":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_WrongFormatDataErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "20":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_NotCustomizedTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "23":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_FolioNumberErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "31":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_FolioNumberOutOfRangeErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                    break;
                case "-1":
                    sound_success.play();
                    Toast.makeText(getApplicationContext(), R.string.kValidationActivity_SuccessfulRFMessage,
                            Toast.LENGTH_LONG).show();
                    break;
                default:
                    sound_error.play();

                    if (loader != null)
                        loader.dismissCustomDialog();

                    Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                            getResources().getString(R.string.kValidationActivity_FailedOperationMessage),
                            getResources().getString(R.string.kDefAlertAcceptOptTitle),
                            ValidationActivity.this);
                    break;
            }

        } catch (Exception e) {
            Log.d(this.getComponentName().getClassName(), e.getMessage());
            sound_error.play();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(this.getComponentName().getClassName(), "Key-" + keyCode + " - KeyEvent-" + event);
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {

            String title = getResources().getString(R.string.kDefAlertTitle);
            String description = getResources().getString(R.string.kValidationActivity_ReadingMessage);
            loader = Notifications.showLoaderCustomDialog(title, description, ValidationActivity.this);

            RF.leerTagUHF(tagToRecord.getFolio());
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return false;
        } else {
            return false;
        }
    }


    public void didSelectInvalid(View button) {
        this.finish();
    }

    public void didSelectValidate(View button) {

        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        String vin = ((TextView) findViewById(R.id.vin_textView)).getText().toString();

        if (vin.length() > 0) {
            String title = getResources().getString(R.string.kDefAlertTitle);
            String description = getResources().getString(R.string.kValidationActivity_PerformingValidationMessage);
            loader = Notifications.showLoaderCustomDialog(title, description, ValidationActivity.this);

            performValidation(new ValidationCallback() {
                @Override
                public void success() {

                    if (loader != null)
                        loader.dismissCustomDialog();

                    CustomDialog dialog = new CustomDialog(CustomDialog.CustomDialogType.DEFAULT, ValidationActivity.this);

                    String accept = getResources().getString(R.string.kDefAlertAcceptOptTitle);
                    dialog.setCustomButton(accept, new CustomDialogOnClickListener(dialog) {
                        @Override
                        public void onClick(View v) {
                            ValidationActivity.this.finish();
                        }
                    });
                    String title = getResources().getString(R.string.kDefAlertTitle);
                    String description = getResources().getString(R.string.kValidationActivity_SuccessfulValidationMessage);
                    dialog.showCustomDialog(title, description);
                }

                @Override
                public void failed(Response response) {

                    if (loader != null)
                        loader.dismissCustomDialog();

                    if (response != null && response.getStatus().equals(HttpStatus.NOT_FOUND)) {
                        if (response.getCode() == RestClient.kVehicleNotFoundCode)
                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kValidationActivity_VehicleNotFoundMessage),
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    ValidationActivity.this);
                    } else if (response != null && response.getStatus().equals(HttpStatus.BAD_REQUEST)) {

                        String message = null;

                        if(response.getCode() == RestClient.kTagUHFAlreadyAssigned)
                            message = getResources().getString(R.string.kValidationActivity_TagUHFAlreadyAssignedMessage);
                        else if(response.getCode() == RestClient.kTagNFCAlreadyAssigned)
                            message = getResources().getString(R.string.kValidationActivity_TagNFCAlreadyAssignedMessage);

                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle), message,
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    ValidationActivity.this);
                    } else {
                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kValidationActivity_FailedValidationMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ValidationActivity.this);
                    }
                }
            });
        } else {
            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                    getResources().getString(R.string.kValidationActivity_PointToUHFTagMessage),
                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                    ValidationActivity.this);
        }
    }

    void performValidation(final ValidationCallback result) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {

                    String user = Utilities.getPersistedString(ValidationActivity.this, Utilities.userNameKey, "");
                    String password = Utilities.getPersistedString(ValidationActivity.this, Utilities.passwordKey, "");
                    String url = Utilities.getPersistedString(ValidationActivity.this, Utilities.urlKey, "");

                    ServerContext context = new ServerContext(user, password, url);
                    final Response response = (new RestClient(context)).associateVehicleTag(tagToRecord.getPlate(), tagToRecord.getTidUhf(), tagToRecord.getTidNfc());

                    if (response != null && response.getStatus().equals(HttpStatus.OK)) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (result != null)
                                    result.success();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (result != null)
                                    result.failed(response);
                            }
                        });
                    }
                } catch (Exception ex) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                    getResources().getString(R.string.kValidationActivity_FailedOperationMessage),
                                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                    ValidationActivity.this);
                        }
                    });
                }
            }
        };
        thread.start();
    }

    private void fillInputs(TagUhf tag) {

        ((TextView) findViewById(R.id.vin_textView)).setText(tag.getVin());
        ((TextView) findViewById(R.id.gallons_textView)).setText(tag.getGalones());
        ((TextView) findViewById(R.id.folioNumber_textView)).setText(tag.getFolio());
        ((TextView) findViewById(R.id.plateNumber_textView)).setText(tag.getPlaca());
        ((TextView) findViewById(R.id.usedGallons_textView)).setText(tag.getGalonesConsumidos());
        ((TextView) findViewById(R.id.lastRecharge_textView)).setText(DateUtils.dateToString(tag.getFecha(), "dd/MM/yyyy"));

        try {
            int isSubsidized = Integer.parseInt(tag.getSubsidio());
            String subsidized = (isSubsidized == SubsidizedCatalog.NO.getValue()) ?
                    getResources().getString(R.string.kValidationActivity_SubsidizedCatalogNo) :
                    ((isSubsidized == SubsidizedCatalog.YES.getValue()) ?
                            getResources().getString(R.string.kValidationActivity_SubsidizedCatalogYes) :
                            getResources().getString(R.string.kValidationActivity_CatalogUndefined));
            ((TextView) findViewById(R.id.subsidized_textView)).setText(subsidized);
        } catch (NumberFormatException ex) {
            Log.d(this.getComponentName().getClassName(), "On subsidized parse error :" + ex.getMessage());
        }

        try {
            int type = Integer.parseInt(tag.getTipoCombustible());
            String fuelType = (type == FuelTypeCatalog.DIESEL.getValue()) ?
                    getResources().getString(R.string.kValidationActivity_FuelTypeCatalogDiesel) :
                    ((type == FuelTypeCatalog.GASOLINE.getValue()) ?
                            getResources().getString(R.string.kValidationActivity_FuelTypeCatalogGasoline) :
                            getResources().getString(R.string.kValidationActivity_CatalogUndefined));
            ((TextView) findViewById(R.id.fuelType_textView)).setText(fuelType);
        } catch (NumberFormatException ex) {
            Log.d(this.getComponentName().getClassName(), "On fuel type parse error :" + ex.getMessage());
        }

        try {
            int type = Integer.parseInt(tag.getTipoVehiculo());
            String vehicleType = (type == VehicleTypeCatalog.AUTOMOBILE.getValue()) ?
                    getResources().getString(R.string.kValidationActivity_VehicleTypeCatalogAutomobile) :
                    ((type == VehicleTypeCatalog.PICKUP.getValue()) ?
                            getResources().getString(R.string.kValidationActivity_VehicleTypeCatalogPickUp) :
                            ((type == VehicleTypeCatalog.TRUCK.getValue()) ?
                                    getResources().getString(R.string.kValidationActivity_VehicleTypeCatalogTruck) :
                                    ((type == VehicleTypeCatalog.TRAILER_TRUCK.getValue()) ?
                                            getResources().getString(R.string.kValidationActivity_VehicleTypeCatalogTrailerTruck) :
                                            ((type == VehicleTypeCatalog.DOUBLE_TRAILER.getValue()) ?
                                                    getResources().getString(R.string.kValidationActivity_VehicleTypeCatalogDoubleTrailer) :
                                                    getResources().getString(R.string.kValidationActivity_CatalogUndefined)))));
            ((TextView) findViewById(R.id.vehicleType_textView)).setText(vehicleType);
        } catch (NumberFormatException ex) {
            Log.d(this.getComponentName().getClassName(), "On vehicle type parse error :" + ex.getMessage());
        }
    }

    //region RF_Listener methods
    @Override
    public void Event_RF_Control(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                writeToTagTV(data);
            }
        });
    }

    @Override
    public void Event_RF_Log(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                writeToTagTV(data);
            }
        });
    }
    //endregion

}