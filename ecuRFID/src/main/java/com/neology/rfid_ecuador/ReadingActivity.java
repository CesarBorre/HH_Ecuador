package com.neology.rfid_ecuador;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.atid.lib.media.SoundPlayer;
import com.neology.ecu.RF_Control;
import com.neology.ecu.RF_Listener;
import com.neology.rfid_ecuador.model.ResultStrConverter;
import com.neology.rfid_ecuador.model.TagUhf;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialog;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialogOnClickListener;
import com.neology.rfid_ecuador.ui.customdialog.CustomDialogOnKeyListener;
import com.neology.rfid_ecuador.utils.DateUtils;
import com.neology.rfid_ecuador.utils.Notifications;

import java.util.Arrays;

public class ReadingActivity extends Activity implements RF_Listener {

    public enum SubsidizedCatalog {

        NO(0),
        YES(1);

        private final int value;

        SubsidizedCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public enum FuelTypeCatalog {

        DIESEL(1),
        GASOLINE(2);

        private final int value;

        FuelTypeCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public enum VehicleTypeCatalog {

        AUTOMOBILE(1),
        PICKUP(2),
        TRUCK(3),
        TRAILER_TRUCK(4),
        DOUBLE_TRAILER(5);

        private final int value;

        VehicleTypeCatalog(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    private RF_Control RF;
    private SoundPlayer sound_error;
    private SoundPlayer sound_success;

    private CustomDialog loader = null;
    private CustomDialog uhfWriterDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_reading);

            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                actionBar.setCustomView(R.layout.activity_reading_action_bar);
                actionBar.setIcon(new ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent)));
                actionBar.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.activity_reading_action_bar));
            }

            sound_success = new SoundPlayer(this, R.raw.ok);
            sound_error = new SoundPlayer(this, R.raw.error);

            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                    getResources().getString(R.string.kReadingActivity_StartReadingMessage),
                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                    ReadingActivity.this);

        } catch (Exception ex) {
            Log.d(this.getComponentName().getClassName(), "onCreate -Error :" + ex.getMessage());
        }
    }

    private void parseReceivedData(String data) {
        try {
            final String[] resultStr = data.split("\\|");
            Log.d(this.getComponentName().getClassName(), " -->RESULT CODE FROM API: " + Arrays.toString(resultStr));
            Log.d(this.getComponentName().getClassName(), " -->RESULT CODE FROM API: " + data);

            switch (resultStr[0]) {
                case "0":
                    sound_success.play();

                    if (uhfWriterDialog != null)
                        uhfWriterDialog.dismissCustomDialog();

                    if (loader != null)
                        loader.dismissCustomDialog();

                    if (resultStr[1].equals("3"))
                        fillInputs(ResultStrConverter.convertStrToTagUhf(data));
                    else if (resultStr[1].equals("6"))
                        fillInputs(ResultStrConverter.convertStrToTagNFC(data));

                    break;
                case "1":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_NoTagMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "2":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_MoreThanOneTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "10":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_WrongFormatDataErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "20":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_NotCustomizedTagErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "23":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_FolioNumberErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "31":
                    if (resultStr[1].equals("3")) {

                        if (loader != null)
                            loader.dismissCustomDialog();

                        Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                                getResources().getString(R.string.kReadingActivity_FolioNumberOutOfRangeErrorMessage),
                                getResources().getString(R.string.kDefAlertAcceptOptTitle),
                                ReadingActivity.this);
                    }
                    break;
                case "-1":
                    sound_success.play();
                    Toast.makeText(getApplicationContext(), R.string.kReadingActivity_SuccessfulRFMessage,
                            Toast.LENGTH_LONG).show();
                    break;
                default:
                    sound_error.play();

                    if (loader != null)
                        loader.dismissCustomDialog();

                    Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                            getResources().getString(R.string.kReadingActivity_FailedOperationMessage),
                            getResources().getString(R.string.kDefAlertAcceptOptTitle),
                            ReadingActivity.this);
                    break;
            }

        } catch (Exception e) {
            Log.d(this.getComponentName().getClassName(), e.getMessage());
            sound_error.play();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(this.getComponentName().getClassName(), "Key-" + keyCode + " - KeyEvent-" + event);
        if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {


            uhfWriterDialog = new CustomDialog(CustomDialog.CustomDialogType.INPUT, ReadingActivity.this);

            uhfWriterDialog.setOnKeyListener(new CustomDialogOnKeyListener(uhfWriterDialog) {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                    if ((keyCode == KeyEvent.KEYCODE_SOFT_RIGHT || keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT
                            || keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) && event.getRepeatCount() <= 0 && event.getAction() == KeyEvent.ACTION_UP) {
                        performUHFTagReading(this.customDialog.getInputText());
                        return true;
                    } else
                        return false;
                }
            });

            String cancel = getResources().getString(R.string.kDefAlertCancelOptTitle);
            uhfWriterDialog.setCustomButton(cancel, new CustomDialogOnClickListener(uhfWriterDialog) {
                @Override
                public void onClick(View v) {
                    this.customDialog.dismissCustomDialog();
                }
            });

            String read = getResources().getString(R.string.kReadingActivity_AlertRecordOptTitle);
            uhfWriterDialog.setCustomButton(read, new CustomDialogOnClickListener(uhfWriterDialog) {
                @Override
                public void onClick(View v) {
                    performUHFTagReading(this.customDialog.getInputText());
                }
            });
            uhfWriterDialog.showCustomDialog(getResources().getString(R.string.kReadingActivity_FolioNumberRequestMessage), null);

            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return false;
        } else {
            return false;
        }
    }

    private boolean performUHFTagReading(String folio) {

        boolean result = false;

        if (folio != null && folio.length() > 0) {

            String title = getResources().getString(R.string.kDefAlertTitle);
            String description = getResources().getString(R.string.kReadingActivity_ReadingMessage);
            loader = Notifications.showLoaderCustomDialog(title, description, ReadingActivity.this);

            try {
                Integer integer = Integer.valueOf(folio);
                result = RF.leerTagUHF(integer.toString());
            } catch (Exception e) {

                if (loader != null)
                    loader.dismissCustomDialog();

                Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                        getResources().getString(R.string.kReadingActivity_OnlyNumbersAllowedMessage),
                        getResources().getString(R.string.kDefAlertAcceptOptTitle),
                        ReadingActivity.this);
            }

        } else {
            Notifications.showOkCustomDialog(getResources().getString(R.string.kDefAlertTitle),
                    getResources().getString(R.string.kReadingActivity_RequiredFolioMessage),
                    getResources().getString(R.string.kDefAlertAcceptOptTitle),
                    ReadingActivity.this);
        }
        return result;
    }

    private void fillInputs(TagUhf tag) {

        if (tag.getVin() != null && (tag.getVin().length() > 0)) {
            ((TextView) findViewById(R.id.vin_textView)).setText(tag.getVin());
            (findViewById(R.id.vin_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.vin_layout)).setVisibility(View.GONE);

        if (tag.getGalones() != null && (tag.getGalones().length() > 0)) {
            ((TextView) findViewById(R.id.gallons_textView)).setText(tag.getGalones());
            (findViewById(R.id.gallons_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.gallons_layout)).setVisibility(View.GONE);

        if (tag.getFolio() != null && (tag.getFolio().length() > 0)) {
            ((TextView) findViewById(R.id.folioNumber_textView)).setText(tag.getFolio());
            (findViewById(R.id.folioNumber_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.folioNumber_layout)).setVisibility(View.GONE);

        if (tag.getPlaca() != null && (tag.getPlaca().length() > 0)) {
            ((TextView) findViewById(R.id.plateNumber_textView)).setText(tag.getPlaca());
            (findViewById(R.id.plateNumber_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.plateNumber_layout)).setVisibility(View.GONE);

        if (tag.getGalonesConsumidos() != null && (tag.getGalonesConsumidos().length() > 0)) {
            ((TextView) findViewById(R.id.usedGallons_textView)).setText(tag.getGalonesConsumidos());
            (findViewById(R.id.usedGallons_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.usedGallons_layout)).setVisibility(View.GONE);

        if (tag.getFecha() != null) {
            ((TextView) findViewById(R.id.lastRecharge_textView)).setText(DateUtils.dateToString(tag.getFecha(), "dd/MM/yyyy"));
            (findViewById(R.id.lastRecharge_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.lastRecharge_layout)).setVisibility(View.GONE);

        if (tag.getSubsidio() != null) {
            try {
                int isSubsidized = Integer.parseInt(tag.getSubsidio());
                String subsidized = (isSubsidized == SubsidizedCatalog.NO.getValue()) ?
                        getResources().getString(R.string.kReadingActivity_SubsidizedCatalogNo) :
                        ((isSubsidized == SubsidizedCatalog.YES.getValue()) ?
                                getResources().getString(R.string.kReadingActivity_SubsidizedCatalogYes) :
                                getResources().getString(R.string.kReadingActivity_CatalogUndefined));
                ((TextView) findViewById(R.id.subsidized_textView)).setText(subsidized);
            } catch (NumberFormatException ex) {
                Log.d(this.getComponentName().getClassName(), "On subsidized parse error :" + ex.getMessage());
            }
            (findViewById(R.id.subsidized_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.subsidized_layout)).setVisibility(View.GONE);

        if (tag.getTipoCombustible() != null) {
            try {
                int type = Integer.parseInt(tag.getTipoCombustible());
                String fuelType = (type == FuelTypeCatalog.DIESEL.getValue()) ?
                        getResources().getString(R.string.kReadingActivity_FuelTypeCatalogDiesel) :
                        ((type == FuelTypeCatalog.GASOLINE.getValue()) ?
                                getResources().getString(R.string.kReadingActivity_FuelTypeCatalogGasoline) :
                                getResources().getString(R.string.kReadingActivity_CatalogUndefined));
                ((TextView) findViewById(R.id.fuelType_textView)).setText(fuelType);
            } catch (NumberFormatException ex) {
                Log.d(this.getComponentName().getClassName(), "On fuel type parse error :" + ex.getMessage());
            }
            (findViewById(R.id.fuelType_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.fuelType_layout)).setVisibility(View.GONE);

        if (tag.getTipoVehiculo() != null) {
            try {
                int type = Integer.parseInt(tag.getTipoVehiculo());
                String vehicleType = (type == VehicleTypeCatalog.AUTOMOBILE.getValue()) ?
                        getResources().getString(R.string.kReadingActivity_VehicleTypeCatalogAutomobile) :
                        ((type == VehicleTypeCatalog.PICKUP.getValue()) ?
                                getResources().getString(R.string.kReadingActivity_VehicleTypeCatalogPickUp) :
                                ((type == VehicleTypeCatalog.TRUCK.getValue()) ?
                                        getResources().getString(R.string.kReadingActivity_VehicleTypeCatalogTruck) :
                                        ((type == VehicleTypeCatalog.TRAILER_TRUCK.getValue()) ?
                                                getResources().getString(R.string.kReadingActivity_VehicleTypeCatalogTrailerTruck) :
                                                ((type == VehicleTypeCatalog.DOUBLE_TRAILER.getValue()) ?
                                                        getResources().getString(R.string.kReadingActivity_VehicleTypeCatalogDoubleTrailer) :
                                                        getResources().getString(R.string.kReadingActivity_CatalogUndefined)))));
                ((TextView) findViewById(R.id.vehicleType_textView)).setText(vehicleType);
            } catch (NumberFormatException ex) {
                Log.d(this.getComponentName().getClassName(), "On vehicle type parse error :" + ex.getMessage());
            }
            (findViewById(R.id.vehicleType_layout)).setVisibility(View.VISIBLE);
        } else
            (findViewById(R.id.vehicleType_layout)).setVisibility(View.GONE);
    }

    //region RF_Listener methods
    @Override
    public void Event_RF_Control(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parseReceivedData(data);
            }
        });
    }

    @Override
    public void Event_RF_Log(final String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                parseReceivedData(data);
            }
        });
    }
    //endregion

    //region Activity methods
    @Override
    protected void onPause() {
        super.onPause();
        if (RF != null)
            RF.onPauseProcessNFC();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (RF != null)
            RF = null;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (RF != null) {
            RF.Destroy();
            RF = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RF == null) {
            RF = new RF_Control();
            RF.Initialize(this);
            RF.readTagNFC();
        }
        RF.onResumeProcessNFC();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (RF != null)
            RF.onNewIntentProcessNFC(intent);
    }
    //endregion
}
