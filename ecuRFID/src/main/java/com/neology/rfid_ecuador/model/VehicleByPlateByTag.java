package com.neology.rfid_ecuador.model;

import java.util.Date;

/**
 * Created by emercado on 12/8/15.
 */
public class VehicleByPlateByTag  {
    private String ruc;
    private String vin;
    private Integer vehicleModel;
    private String vehicleBrand;
    private String vechileSubbrand;
    private String plate;
    private String usageType;
    private String vehicleType;
    private String fuelType;
    private boolean subsidy;
    private Integer status;
    private float litersByMonth;
    private float litersConsumed;
    private Date dateLatestCharge;

    public VehicleByPlateByTag() {
    }

    public VehicleByPlateByTag(String ruc, String vin, Integer vehicleModel, String vehicleBrand, String vechileSubbrand, String plate, String usageType, String vehicleType, String fuelType, boolean subsidy, Integer status, float litersByMonth, float litersConsumed, Date dateLatestCharge) {
        this.ruc = ruc;
        this.vin = vin;
        this.vehicleModel = vehicleModel;
        this.vehicleBrand = vehicleBrand;
        this.vechileSubbrand = vechileSubbrand;
        this.plate = plate;
        this.usageType = usageType;
        this.vehicleType = vehicleType;
        this.fuelType = fuelType;
        this.subsidy = subsidy;
        this.status = status;
        this.litersByMonth = litersByMonth;
        this.litersConsumed = litersConsumed;
        this.dateLatestCharge = dateLatestCharge;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(Integer vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getVechileSubbrand() {
        return vechileSubbrand;
    }

    public void setVechileSubbrand(String vechileSubbrand) {
        this.vechileSubbrand = vechileSubbrand;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public boolean isSubsidy() {
        return subsidy;
    }

    public void setSubsidy(boolean subsidy) {
        this.subsidy = subsidy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public float getLitersByMonth() {
        return litersByMonth;
    }

    public void setLitersByMonth(float litersByMonth) {
        this.litersByMonth = litersByMonth;
    }

    public float getLitersConsumed() {
        return litersConsumed;
    }

    public void setLitersConsumed(float litersConsumed) {
        this.litersConsumed = litersConsumed;
    }

    public Date getDateLatestCharge() {
        return dateLatestCharge;
    }

    public void setDateLatestCharge(Date dateLatestCharge) {
        this.dateLatestCharge = dateLatestCharge;
    }
}
