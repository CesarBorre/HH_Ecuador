package com.neology.rfid_ecuador.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class VehicleTag implements Serializable{
    private String plate;
    private String tidUhf;
    private String tidNfc;
    @JsonIgnore
    private String folio;

    public VehicleTag() {
    }

    public VehicleTag(String plate, String tidUhf, String tidNfc) {
        this.plate = plate;
        this.tidUhf = tidUhf;
        this.tidNfc = tidNfc;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getTidUhf() {
        return tidUhf;
    }

    public void setTidUhf(String tidUhf) {
        this.tidUhf = tidUhf;
    }

    public String getTidNfc() {
        return tidNfc;
    }

    public void setTidNfc(String tidNfc) {
        this.tidNfc = tidNfc;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public String toString() {
        return "VehicleTag{" +
                "folio='" + folio + '\'' +
                ", plate='" + plate + '\'' +
                ", tidUhf='" + tidUhf + '\'' +
                ", tidNfc='" + tidNfc + '\'' +
                '}';
    }
}
