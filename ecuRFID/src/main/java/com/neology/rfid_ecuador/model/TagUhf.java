package com.neology.rfid_ecuador.model;

import java.util.Date;

public class TagUhf {

    private String epc;
    private String tid;
    private String folio;
    private String placa;
    private String vin;
    private String tipoVehiculo;
    private String tipoCombustible;
    private String subsidio;
    private String galones;
    private Date fecha;
    private String galonesConsumidos;


    public TagUhf() {
    }

    public String getEpc() {
        return epc;
    }
    public void setEpc(String epc) {
        this.epc = epc;
    }

    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPlaca() {
        return placa;
    }
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFolio() {
        return folio;
    }
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getGalones() {
        return galones;
    }
    public void setGalones(String galones) {
        this.galones = galones;
    }

    public String getGalonesConsumidos() {
        return galonesConsumidos;
    }
    public void setGalonesConsumidos(String galonesConsumidos) {
        this.galonesConsumidos = galonesConsumidos;
    }

    public String getSubsidio() {
        return subsidio;
    }
    public void setSubsidio(String subsidio) {
        this.subsidio = subsidio;
    }

    public String getTid() {
        return tid;
    }
    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }
    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }
    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getVin() {
        return vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }

}
