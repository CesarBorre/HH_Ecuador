package com.neology.rfid_ecuador.model;

import com.neology.rfid_ecuador.utils.DateUtils;

/**
 * Created by emercado on 12/13/15.
 */
public class ResultStrConverter {
    public static TagUhf convertStrToTagUhf (String input){
        TagUhf result=new TagUhf();
        if (input!=null && !input.equals(""))
            try{
                String[] inputSplit = input.split("\\|");
                result.setEpc(inputSplit[2]);
                result.setTid(inputSplit[3]);
                result.setFolio(inputSplit[4]);
                result.setPlaca(inputSplit[5]);
                result.setVin(inputSplit[6]);
                result.setTipoVehiculo(inputSplit[7]);
                result.setTipoCombustible(inputSplit[8]);
                result.setSubsidio(inputSplit[9]);
                result.setGalones(inputSplit[10]);
                result.setFecha(DateUtils.stringToDate(inputSplit[11],"ddMMyyyy"));
                result.setGalonesConsumidos(inputSplit[12]);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        return result;
    }
    public static TagUhf convertStrToTagNFC (String input){
        TagUhf result=new TagUhf();
        if (input!=null && !input.equals(""))
            try{
                String[] inputSplit = input.split("\\|");
                result.setFolio(inputSplit[3]);
                result.setSubsidio(inputSplit[4]);
                result.setGalones(inputSplit[5]);
                result.setPlaca(inputSplit[6]);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        return result;
    }
}
