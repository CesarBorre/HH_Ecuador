package com.neology.rfid_ecuador.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.neology.rfid_ecuador.ValidationActivity;

import static com.neology.rfid_ecuador.ValidationActivity.FuelTypeCatalog.*;

public class VehicleByPlate implements Serializable {
    private String licensePlate;
    private String vin;
    private Integer model;
    private String vehicleType;
    private String fuelType;
    private boolean subsidization;
    private float litersPerMonth;
    private String status;
    @JsonIgnore
    private int vehicleTypeId = 1;
    @JsonIgnore
    private int fuelTypeId = 1;


    public VehicleByPlate() {
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getModel() {
        return model;
    }

    public void setModel(Integer model) {
        this.model = model;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public boolean getSubsidization() {
        return subsidization;
    }

    public void setSubsidization(boolean subsidization) {
        this.subsidization = subsidization;
    }

    public float getLitersPerMonth() {
        return litersPerMonth;
    }

    public void setLitersPerMonth(float litersPerMonth) {
        this.litersPerMonth = litersPerMonth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public int getFuelTypeId() {

        if (this.getFuelType().equals("Gasolina"))
            return ValidationActivity.FuelTypeCatalog.GASOLINE.getValue();
        else if (this.getFuelType().equals("Diesel"))
            return ValidationActivity.FuelTypeCatalog.DIESEL.getValue();
         else {
            return 0;
        }
    }

    public void setFuelTypeId(int fuelTypeId) { this.fuelTypeId = fuelTypeId; }

    @Override
    public String toString() {
        return "VehicleByPlate{" +
                "fuelType='" + fuelType + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", vin='" + vin + '\'' +
                ", model=" + model +
                ", vehicleType='" + vehicleType + '\'' +
                ", subsidization=" + subsidization +
                ", litersPerMonth=" + litersPerMonth +
                ", status='" + status + '\'' +
                ", vehicleTypeId=" + vehicleTypeId +
                ", fuelTypeId=" + fuelTypeId +
                '}';
    }
}
